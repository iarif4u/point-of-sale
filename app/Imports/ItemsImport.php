<?php

namespace App\Imports;

use App\Brand;
use App\Category;
use App\Product;
use App\Stock;
use App\Warehouse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ItemsImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        DB::beginTransaction();

        try {
            foreach ($collection->all() as $column => $item) {
                /*$category = Category::UpdateOrCreate([
                    'name' => Auth::id()
                ],[
                    ,
                    'parent_id',
                    'status',
                    'is_delete'
                ]);*/

                //$product = Product::where(['serial_no' => $item[0]])->first();
                $whereHouse = Warehouse::where(['id'=>1])->first();
             //   $has = Product::where(['serial_no' => $item[0]])->first();
                    $category = Category::updateOrCreate(['name' => $item[2]]);
                    $sub_category = Category::updateOrCreate(['name' => $item[3]], ['parent_id' => $category->id]);

                    $brand = Brand::updateOrCreate(['name' => $item[4]]);

                    $product= Product::create([
                        'serial_no' => $item[0],
                        'name'=> $item[1],
                        'brand_id'=> $brand->id,
                        'category_id'=> $category->id,
                        'sub_category_id'=> $sub_category->id,
                        'product_details'=> $item[9],
                        'item_tk_rate' => $item[6],
                        'item_dollar_rate' => $item[7],
                        'stock' => (intval($item[8])>0)?$item[8]:0,
                    ]);
                    Stock::updateOrCreate([
                        'product_id' => $product->id,
                        'warehouse_id' => $whereHouse->id
                    ],[
                        'product_qty' => (intval($item[8])>0)?$item[8]:0
                    ]);
                    //['serial_no','name','category_id','sub_category_id','brand_id','item_tk_rate','item_dollar_rate','product_details','status','is_delete','stock']
                }
            
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($column,$item,$exception->getMessage());
            throw new \Exception($exception->getMessage(),$exception->getCode(),$exception->getPrevious());
        }
    }
    public function chunkSize(): int
    {
        return 1000;
    }
}
