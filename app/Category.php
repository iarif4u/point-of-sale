<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $fillable = ['name','parent_id','status','is_delete'];

    //subcate has one category
    public function category() {
        return $this->hasOne('App\Category','id','parent_id')->where(['is_delete'=>0]);
    }

    //subcate has one category
    public function sub_category() {
        return $this->hasMany('App\Category','parent_id','id')->where(['is_delete'=>0]);
    }
}
