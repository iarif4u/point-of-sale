<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table= 'customers';

    protected $fillable = ['name','phone','company','address','status','is_delete'];
}
