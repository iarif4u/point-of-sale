<?php

namespace App\DataTables;

use App\Sell;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SellsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('invoice_no',function ($sell){
                return ($sell->invoice_no)? $sell->invoice_no :"Not Found";
            })
            ->addColumn('due',function ($sell){
                return $sell->total_tk-$sell->paid_tk;
            })->editColumn('created_at', function ($sell) {
                return $sell->created_at ? with(new Carbon($sell->created_at))->format('d-m-Y') : '';;
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%$keyword%"]);
            })->addColumn('action',function($sells){
              return view('admin.pos.datatable.sold_actions',['sells'=>$sells]);
            })->setRowClass(function ($sell){
                return 'category_'.$sell->id;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Sell $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Sell $model)
    {
        return $model->newQuery()->with(['customer','return']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('sells-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(5);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('invoice_no'),
            Column::make('customer.name')->title('Customer Name'),
            Column::make('total_tk')->title('Total Amount'),
            Column::make('paid_tk')->title('Total Paid'),
            Column::make('due')->title('due'),
            Column::make('created_at')->title('Date'),
            Column::make('action')->title('Action'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Sells_' . date('YmdHis');
    }
}
