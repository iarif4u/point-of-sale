<?php

namespace App\DataTables;

use App\Stock;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StocksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($stock){
                return '<div class="action">
                                            <a onclick="get_item_view(' . $stock->product->id . ')" href="javascript:void(0);" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            <a onclick="get_item_transaction(' . $stock->product->id . ','
                . $stock->warehouse->id . ')"
                                                   href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                        </div>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Stock $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Stock $model)
    {
        return $model->newQuery()->with(['product','warehouse']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stocks-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('product.serial_no')->title('Serials'),
            Column::make('product.name')->title('Name'),
            Column::make('product.sub_category.name')->title('Sub Category')->orderable(false)->searchable(false),
            Column::make('product.product_details')->title('Details'),
            Column::make('warehouse.name')->title('Warehouse'),
            Column::make('product.item_tk_rate')->title('BDTk'),
            Column::make('product.item_dollar_rate')->title('Dollar'),
            Column::make('product_qty')->title('Quantity'),
            Column::make('action'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Stocks_' . date('YmdHis');
    }
}
