<?php

namespace App\DataTables;

use App\Stock;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PosProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('warehouse.name',function ($stock){
                return view('admin.pos.datatable.warehouse_ac',['stock'=>$stock]);

            })
            ->addColumn('action', function ($stock){
                return '<a href="javascript:void(0);" onclick="add_product_to_chart('.$stock->product->id.',this,'.$stock->id.')" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-cart-plus" aria-hidden="true"></i> </a>';
            })->setRowClass(function ($stock){
                return "product_".$stock->id;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Stock $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Stock $model)
    {
        return $model->newQuery()->with(['product','warehouse']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('product-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(1)
                    ->addTableClass('table table-bordered table-striped');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('product.serial_no')->title('Serial')->addClass('product_serial'),
            Column::make('product.name')->title('Product')->addClass('product_name'),
            Column::make('warehouse.name')->title('Warehouse')->addClass('product_wh ware_house'),
            Column::make('product_qty')->title('QTY')->addClass('product_qty'),
            Column::make('product.item_tk_rate')->title('Price')->addClass('product_price'),
            Column::make('action')->title('Add')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PosProducts_' . date('YmdHis');
    }
}
