<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseLog extends Model
{
    protected $table = 'warehouse_logs';

    protected $fillable = ['from_warehouse','to_warehouse','product','quantity','note'];

    //A log id  has one from warehouse
    public function warehouse_from() {
        return $this->hasOne('App\Warehouse','id','from_warehouse');
    }

    //A log id  has one to warehouse
    public function warehouse_to() {
        return $this->hasOne('App\Warehouse','id','to_warehouse');
    }

    //A log id  has one product
    public function product_data() {
        return $this->hasOne('App\Product','id','product');
    }
}
