<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationTransaction extends Model
{
    protected $table = "quotation_transactions";

    protected $fillable = ['product_id','product_qty','product_rate','customer_id','status','quotation_no'];

    //every transaction has one product
    public function product() {
        return $this->hasOne('App\Product','id','product_id')->with(['brand']);
    }
    //transaction has one customer or not
    public function customer() {
        return $this->hasOne('App\Customer','id','customer_id');
    }
}
