<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = "quotations";
    protected $fillable = ['customer_id','ref_note','total_tk'];

    //sell has one customer
    public function customer() {
        return $this->hasOne('App\Customer','id','customer_id');
    }

    //transaction has many transaction
    public function transaction() {
        return $this->hasMany('App\QuotationTransaction','quotation_no','id')->with(['product']);
    }
}
