<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = "vendors";

    protected $fillable = ['name','phone','company','address','status','is_delete'];
}
