<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPaid extends Model
{
    protected $table = 'customer_paids';

    protected $fillable = ['amount','customer_id','payment_method','reference','type','receiver_id','sell_id'];
}
