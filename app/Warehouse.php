<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = "warehouses";

    protected $fillable = ['name','phone','address','status','is_delete'];

    //A stock id  has one product
    public function stock() {
        return $this->hasMany('App\Stock','warehouse_id','id')->with('product');
    }
}
