<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $table = "sells";

    protected $fillable = ['customer_id','payment_method',"invoice_no",'ref_note','paid_tk','total_tk'];

    //sell has one customer
    public function customer() {
        return $this->hasOne('App\Customer','id','customer_id')->select(['id','name','phone','company','address','status','is_delete']);
    }

    //transaction has many transaction
    public function transaction() {
        return $this->hasMany('App\Transaction','sells_id','id')->where(['type'=>'out'])->with(['product','warehouse']);
    }

    //transaction has many return
    public function return() {
        return $this->hasMany('App\ReturnModel','sell_id','id')->select(['id','sell_id','customer_id','return_product_price','customer_get_return'])->with('transaction');
    }
    //get function of return amount
    public function return_amout() {
        return $this->hasMany('App\ReturnModel','sell_id','id')->sum('customer_get_return');
    }
}
