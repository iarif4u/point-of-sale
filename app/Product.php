<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table = 'products';

    protected $fillable = ['serial_no','name','category_id','sub_category_id','brand_id','item_tk_rate','item_dollar_rate','product_details','status','is_delete','stock'];

    //subcate has one category
    public function category() {
        return $this->hasOne('App\Category','id','category_id')->with('sub_category');
    }

    //subcate has many category
    public function transaction() {
        return $this->hasMany('App\Transaction','product_id','id');
    }

    //subcate has one category
    public function sub_category() {
        return $this->hasOne('App\Category','id','sub_category_id');
    }

    //subcate has one brand
    public function brand() {
        return $this->hasOne('App\Brand','id','brand_id');
    }
    //subcate has one brand
    public function stock_warehouse() {
        return $this->hasMany('App\Stock','product_id','id')->with('warehouse');
    }
}
