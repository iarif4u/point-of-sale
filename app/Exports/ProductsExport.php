<?php

namespace App\Exports;

use App\Stock;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductsExport implements FromView,ShouldAutoSize,ShouldQueue
{
    use Exportable,Queueable;

    public function view(): View
    {
        $stocks = Stock::with(['product', 'warehouse'])->offset(999)->limit(1000)->get();
        return view('admin.exports.productList', [
            'stocks' => $stocks
        ]);
    }
    public function array(): array
    {
        $stocks = Stock::with(['product', 'warehouse'])->limit(500)->get();
        $items =[];
        if($stocks->count()>0):
            foreach($stocks as $stock):
                $product = [
                    $stock->product->serial_no,
                    $stock->product->name,
                    $stock->product->category->name,
                    $stock->product->sub_category->name,
                    $stock->product->brand->name,
                    $stock->warehouse->name,
                    $stock->product->item_tk_rate,
                    $stock->product->item_dollar_rate,
                    $stock->product_qty,
                    $stock->product->product_details
                ];
            $items[]=$product;
            endforeach;
        endif;
        dd($items);
        /*return [
            [1, 2, 3],
            [4, 5, 6]
        ];*/
    }
}
