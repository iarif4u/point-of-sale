<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = "transactions";

    protected $fillable = ['product_id','product_qty','product_rate','product_dollar_rate','vendor_id','customer_id','warehouse_id','special_note','type','status','sells_id','product_status','currency'];

    //every transaction has one product
    public function product() {
        return $this->hasOne('App\Product','id','product_id')->with(['brand']);
    }
    //get transaction sells data
    public function sell() {
        return $this->hasOne('App\Sell','id','sells_id');
    }
    //transaction has one customer or not
    public function customer() {
        return $this->hasOne('App\Customer','id','customer_id');
    }

    //transaction has one vendor or not
    public function vendor() {
        return $this->hasOne('App\Vendor','id','vendor_id');
    }

    //transaction has one vendor or not
    public function warehouse() {
        return $this->hasOne('App\Warehouse','id','warehouse_id');
    }

    //get transaction product with other data
    public function product_data() {
        return $this->hasOne('App\Product','id','product_id')->with(['brand','category','sub_category']);
    }

}
