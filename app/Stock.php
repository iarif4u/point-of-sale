<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = "stocks";

    protected $fillable = ['product_id','warehouse_id','product_qty'];

    //A stock id  has one product
    public function product() {
        return $this->hasOne('App\Product','id','product_id')->where(['is_delete'=>0])->with('sub_category');
    }


    //A stock id  has one product
    public function warehouse() {
        return $this->hasOne('App\Warehouse','id','warehouse_id')->where(['is_delete'=>0]);
    }
}
