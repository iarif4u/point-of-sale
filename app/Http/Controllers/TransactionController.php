<?php

namespace App\Http\Controllers;

use App\Product;
use App\Sell;
use App\Stock;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
class TransactionController extends Controller
{
    #import item for vendor
    public function import_item(Request $request){

        $has_error = $this->validate_trans_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error);
        }else{

            $product_id = $request->input('item_id');
            $product_qty = $request->input('itemQty');
            $tk_rate = $request->input('itemRate');
            $dollar_rate = $request->input('itemDollarRate');
            $warehouse_id = $request->input('itemWirehouse');
            $currency = $request->input('currency');
            $product = Product::where(['id'=>$product_id])->select('stock')->first();
            $product_status = $product->stock+$product_qty;
            $transaction = Transaction::create([
                'product_id'=>$product_id,
                'product_qty'=>$product_qty,
                'product_rate'=>$tk_rate,
                'product_dollar_rate'=>$dollar_rate,
                'vendor_id'=>$request->input('itemVendor'),
                'warehouse_id'=>$warehouse_id,
                'special_note'=>$request->input('specialNote'),
                'type'=>'in',
                'status'=>1,
                'product_status'=>$product_status,
                'currency'=>$currency
                ]);
            if($transaction){
                Product::where(['id'=>$product_id])->update(['stock'=>DB::raw("stock+".$product_qty),'item_tk_rate'=>$tk_rate,'item_dollar_rate'=>$dollar_rate]);
                Stock::updateOrCreate([
                    'product_id' =>$product_id ,
                    'warehouse_id'=>$warehouse_id,
                ]);
                $stock = Stock::firstOrNew(['product_id' =>$product_id ,'warehouse_id'=>$warehouse_id]);
                $stock->product_qty = $stock->product_qty+$product_qty;
                $stock->save();
                return redirect()->back()->with('message','Item import successfully done');
            }else{
                return redirect()->back()->withErrors("Item import fail");
            }
        }
    }

    #validate transaction request
    private function validate_trans_req($request){
        $validator=  Validator::make($request, [
            'item_id' => 'required|numeric|exists:products,id',
            'itemVendor' => 'required|numeric|exists:vendors,id',
            'itemWirehouse' => 'required|numeric|exists:warehouses,id',
            'itemRate' => 'required|numeric',
            'itemQty' => 'required|numeric',
            'itemDollarRate' => 'required|numeric',
            'specialNote' => 'max:191',
            'currency' => 'required|max:191',

        ],[
            'item_id.required' => "Item name is required",
            'item_id.exists' => "Item doesn't match of our record",
            'item_id.numeric' => "Item name is invalid",
            'itemVendor.required' => "Vendor name is required",
            'itemVendor.exists' => "Vendor doesn't match of our record",
            'itemVendor.numeric' => "Vendor name is invalid",
            'itemWirehouse.required' => "Warehouse name is required",
            'itemWirehouse.exists' => "Warehouse doesn't match of our record",
            'itemWirehouse.numeric' => "Warehouse name is invalid",
            'specialNote.max' => 'Special note has invalid length',
            'itemRate.required' => 'Item rate is required',
            'itemRate.numeric' => 'Item rate is must be numeric',
            'itemQty.required' => 'Item quantity is required',
            'itemQty.numeric' => 'Item quantity is must be numeric',
            'itemDollarRate.required' => 'Item dollar rate is required',
            'itemDollarRate.numeric' => 'Item dollar rate is must be numeric',
            'currency.required' => 'Item currency is required',
            'currency.max' => 'Item currency must be below 191 charecter',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    private function get_transaction_type($transaction){
        if ($transaction->type=="in"){
            return "Buy";
        }
        elseif ($transaction->type=="out"){
            return "Sell";
        }else{
            return "Return";
        }
    }
    #get warehouse details transaction
    public function get_warehouse_transaction(Request $request){
        $warehouse_id = $request->input('warehouse_id');
        $transaction_list = Transaction::with(['product'])->where(['warehouse_id'=>$warehouse_id])->get();
        $data = array();
        foreach ($transaction_list as $transaction):
            $sub_array = array();
            $sub_array[] = date('d-m-Y',strtotime($transaction->created_at));
            $sub_array[] = $transaction->product->name;
            $sub_array[] = $this->get_transaction_type($transaction);
            $sub_array[] = ($transaction->type=="in")? null : $transaction->product_rate;
            $sub_array[] = ($transaction->type=="in")? $transaction->product_dollar_rate: null;

            $sub_array[] = $transaction->product_qty;
            $data[] = $sub_array;
        endforeach;

        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }
    #get vendor details transaction
    public function get_vendor_transaction(Request $request){
        $vendor_id = $request->input('vendor_id');
        $transaction_list = Transaction::with(['product'])->where(['vendor_id'=>$vendor_id])->get();
        $data = array();
        foreach ($transaction_list as $transaction):
            $sub_array = array();
            $sub_array[] = date('d-m-Y',strtotime($transaction->created_at));
            $sub_array[] = $transaction->product->name;
            $sub_array[] = $transaction->product_dollar_rate;
            $sub_array[] = $transaction->product_qty;
            $data[] = $sub_array;
        endforeach;

        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }
    #get brand transaction details
    public function get_brand_transaction(Request $request){
        $brand_id = $request->input('brand_id');
        $product_list = Product::with(['transaction'])->where(['brand_id'=>$brand_id])->get();
        $data = array();
        foreach ($product_list as $product){
            if($product->transaction):
            foreach ($product->transaction as $transaction){
                $sub_array = array();
                if($transaction->type == "return"){
                    $type = "Return";
                }
                if($transaction->type == "out"){
                    $type = "Sell";
                }
                if($transaction->type == "in"){
                    $type = "Buy";
                }
                $sub_array[] = date('d-m-Y',strtotime($transaction->created_at));
                $sub_array[] = $product->name;
                $sub_array[] = $type;
                $sub_array[] = ($transaction->type=="in")? null : $transaction->product_rate;
                $sub_array[] = ($transaction->type=="in")? $transaction->product_dollar_rate: null;

                $sub_array[] = $transaction->product_qty;
                $data[] = $sub_array;
            }
            endif;
        }

        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }
    //get customer transaction data
    public function get_customer_transaction(Request $request){
        $customer_id = $request->input('customer_id');
        $transaction_list = Transaction::with(['product'])->where(['customer_id'=>$customer_id])->orderBy('id','DESC')->get();
        $data = array();
        foreach ($transaction_list as $transaction){
            $sub_array = array();
            $sub_array[] = $transaction->sells_id;
            $sub_array[] = date('d-m-Y',strtotime($transaction->created_at));
            $sub_array[] = date('h:i A',strtotime($transaction->created_at));
            $sub_array[] = $transaction->product->name;
            $sub_array[] = $transaction->product_rate;
            $sub_array[] = ($transaction->type=="return")?"Return" :"Sell";
            $sub_array[] = $transaction->product_qty;
            $data[] = $sub_array;
        }
        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }
    #get customer transaction and sells details
    public function get_customer_history(Request $request){
        $customer_id = $request->input('customer_id');
        $sellsList = Sell::with(['customer','transaction'])->where(['customer_id'=>$customer_id])->get();
        $data = array();
        $total_row = 0;
        foreach ($sellsList as $sell){
            $sell_id = $sell->id;
            $sell_date = date('d-m-Y',strtotime($sell->created_at));
            $total_row+=$sell->transaction->count();
            foreach ($sell->transaction as $transaction){
                $sub_array = array();
                $product_name = $transaction->product->name;
                $product_price = $transaction->product_rate;
                $product_qty = $transaction->product_qty;
                $sub_array[] = $sell_id;
                $sub_array[] = $sell_date;
                $sub_array[] = $product_name;
                $sub_array[] = $product_price;
                $sub_array[] = $product_qty;
                $data[] = $sub_array;
            }

        }
        $output = array(
            'draw' => intval($request->input('draw')),
            'recordsTotal' => $total_row,
            'data' => $data,
        );
        echo json_encode($output);
    }
    #get transactions details by product id
    public function get_product_transaction(Request $request){
        $product_id = $request->input('product_id');
        #check request by form validation
        $has_error = $this->validate_product_trans_req($request->all());
        if ($has_error){
            echo json_encode(['error'=>'true','message'=>$has_error]);
        }else{

            $transaction_list = Transaction::with(['product','customer','vendor','warehouse'])
                ->where(['product_id'=>$product_id])
                ->orderBy('id','asc')
                ->get();
            $row = 0;
            $current_qty = 0;
            $data = array();
            foreach ($transaction_list as $transaction){
                $row++;
                $sub_array = array();
                $customer_name = null;
                $vendor_name = null;
                $customer_quantity = null;
                $vendor_quantity = null;
                if ($transaction->customer){
                    $customer_name = $transaction->customer->name;
                    if($transaction->type=="return"){

                        $vendor_quantity= $transaction->product_qty." (Return)";
                    }else{

                        $customer_quantity= $transaction->product_qty;
                    }

                }
                if ($transaction->vendor){
                    $vendor_name = $transaction->vendor->name;
                    $vendor_quantity= $transaction->product_qty;

                }
                $sub_array[]= date('d-m-Y',strtotime($transaction->created_at));
                $sub_array[]=  $transaction->special_note;
                $sub_array[]=  ($transaction->product_dollar_rate > 0) ? $transaction->product_dollar_rate : null;
                $sub_array[]=  $customer_name;
                $sub_array[]=  $vendor_name;
                $sub_array[]=  $customer_quantity;
                $sub_array[]=  $vendor_quantity;
                $sub_array[]=  $transaction->currency;
                $sub_array[]=  $transaction->product_status;
                $data[] = $sub_array;
            }
            $output = array(
                'draw' => intval($request->input('draw')),
                'recordsTotal' => $transaction_list->count(),
                'recordsFiltered' => $row,
                'data' => $data,
            );
            echo json_encode($output);
        }
    }

    #validate transaction request
    private function validate_product_trans_req($request){
        $validator=  Validator::make($request, [
            'product_id' => 'required|numeric|exists:products,id',
        ],[
            'item_id.required' => "Item name is required",
            'item_id.exists' => "Item doesn't match of our record",
            'item_id.numeric' => "Item name is invalid",
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
}
