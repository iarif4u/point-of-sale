<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Stock;
use App\Transaction;
use Illuminate\Validation\Rule;
use App\Brand;
use App\Category;
use App\Product;
use App\Vendor;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ItemController extends Controller
{
    //get product streaming by datatable request
    public function get_product_streaming(Request $request){
        $itemList = Product::with(['category','sub_category','brand'])->where(['is_delete'=>0])->get();
        $data = array();
        if (isset($itemList)){
            foreach($itemList as $item){
                $sub_array = array();
                $sub_array[] = '<div class="item_serial">'.$item->serial_no.'</div>';
                $sub_array[] = '<div class="item_name">'.$item->name.'</div>';
                $sub_array[] = '<div class="category">'.$item->category->name.'</div>';
                $sub_array[] = '<div class="sub-category">'.$item->sub_category->name.'</div>';
                $sub_array[] = '<div class="brand">'.$item->brand->name.'</div>';
                $sub_array[] = ($item->item_tk_rate>0)? '<div class="bd-rate">৳ '.$item->item_tk_rate.'</div>' :'<div class="bd-rate">৳ 0</div>';
                $sub_array[] = ($item->item_dollar_rate>0)? '<div class="dollar_rate">$ '.$item->item_dollar_rate.'</div>' :'<div class="bd-rate">$ 0</div>';

                $sub_array[] = '<a onclick="get_item_view('.$item->id.')" href="javascript:void(0);"
                class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                 <a onclick="get_item_import('.$item->id.')" href="javascript:void(0);" class="btn-view" title="Import
        Information" data-toggle="modal" data-target="#modal-import"><i class="fa fa-plus" aria-hidden="true"></i></a>
                <a href="javascript:void(0);" onclick="get_item_view('.$item->id.')" class="btn-edit" title="Edit
        Information" data-toggle="modal" data-target="#modal-edititemarea"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
         <a onclick="get_item_delete('.$item->id.')" href="javascript:void(0);" class="btn-detect" title="Detect
        Information" data-toggle="modal" data-target="#modal-delectitemarea"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        <a onclick="get_item_transaction('.$item->id.')" href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>';
                $data[] = $sub_array;
            }
        }
        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }
    #get view of item list
    public function get_item_list(){
        $category_list = Category::where(['is_delete'=>0,'parent_id'=>0])->get();
        $subcateList = Category::where(['is_delete'=>'0'])->where('parent_id','>','0')->get();
        $brand_list = Brand::where(['is_delete'=>'0'])->get();

        $vendor_list = Vendor::where(['is_delete'=>'0'])->get();
        $warehouse_list = Warehouse::where(['is_delete'=>'0'])->get();

        $vendor_list = $this->make_collection_select($vendor_list,'company');
        $category_list = $this->make_collection_select($category_list);

        $warehouse_list = $this->make_collection_select($warehouse_list);
        $brand_list = $this->make_collection_select($brand_list);
        $subcateList = $this->make_collection_select($subcateList);

        return view('admin.item.item',['warehouse_list'=>$warehouse_list,'vendor_list'=>$vendor_list,'brand_list'=>$brand_list,'subcateList'=>$subcateList,'category_list'=>$category_list]);
    }
    #download excel file
    public function download_excel_file(){
        return Excel::download(new ProductsExport(), 'Products'.date('d-m-Y_h:i:s').'.xlsx');
    }
    #make collection list to array for select tag
    private function make_collection_select($data_list,$name='name'){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->id] = $data->$name;
        }
        return $select;
    }
    #make new item
    public function make_new_item(Request $request){
        $has_error = $this->validate_new_item_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            try {
                Product::create([
                    'serial_no' => $request->input('serial_no'),
                    'name' => $request->input('itemName'),
                    'category_id' => $request->input('itemCategory'),
                    'sub_category_id' => $request->input('itemSubCategory'),
                    'brand_id' => $request->input('brandName'),
                    'item_tk_rate' => $request->input('itemRate'),
                    'item_dollar_rate' => $request->input('itemDollarRate'),
                    'product_details' => $request->input('productDetails'),
                ]);
            }catch (\Exception $exception){
                return redirect()->back()->withErrors("Item insert fail")->withInput();
            }
            return redirect()->back()->with('message','Item insert successfully done');
        }
    }

    #validate new item request
    private function validate_new_item_req($request,$product_id=null){

        $request['name'] = $request['itemName'];
        unset($request['itemName']);
        $validator=  Validator::make($request, [
            'item_id' => 'exists:products,id',
            'name' => 'required',
            'serial_no' => [
                'required',
                Rule::unique('products')->ignore($product_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'brandName' => 'required|exists:brands,id',
            'specialNote' => 'max:191',
            'productDetails' => 'max:191',
            'itemCategory' => 'required|exists:categories,id',
            'itemSubCategory' => 'required|exists:categories,id',
            'itemRate' => 'required|numeric',
            'itemDollarRate' => 'required|numeric',
        ],[
            'item_id.exists' => "Item doesn't match of our record",
            'specialNote.max' => 'Special note has invalid length',
            'productDetails.max' => 'Product details has invalid length',
            'name.required' => 'Item name is required',
            'serial_no.required' => 'Item serial no is required',
            'name.unique' => 'The item name has already been taken',
            'serial_no.unique' => 'The item serial no has already been taken',
            'brandName.required' => 'Brand name is required',
            'brandName.exists' => 'The brand name not found',
            'itemRate.required' => 'Item rate is required',
            'itemRate.numeric' => 'Item rate is must be numeric',
            'itemDollarRate.required' => 'Item dollar rate is required',
            'itemDollarRate.numeric' => 'Item dollar rate is must be numeric',
            'itemCategory.required' => 'Category name is required',
            'itemCategory.exists' => 'The category name not found',
            'itemSubCategory.required' => 'Sub-category name is required',
            'itemSubCategory.exists' => 'The sub-category name not found',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }


    #get item category list
    public function get_category_list(){
        $category_list = Category::where(['is_delete'=>0,'parent_id'=>0])->get();
        return view('admin.item.category',['category_list'=>$category_list]);
    }
    #update category
    public function update_category(Request $request){
        $category_id = $request->input('category_id');
        $has_error = $this->validate_cate_req($request->all(),$category_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $category = Category::where('id', $category_id)->update( [
                'name' => $request->input('categoryName')
            ]);
            if($category){
                return redirect()->back()->with('message','Category update success');
            }else{
                return redirect()->back()->withErrors("Category update fail")->withInput();
            }
        }
    }
    #get subcategory by category id
    public function get_sub_category(Request $request){
        $category_id = $request->input('category_id');
        $validator=  Validator::make($request->all(), [
            'category_id' => 'exists:categories,id',
        ],[
            'category_id.exists' => 'Category not found',
        ]);
        $subcateList = Category::where(['is_delete'=>'0','parent_id'=>$category_id])->get();
        $subcateList = $this->make_subcate_ajax($subcateList);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            if ($subcateList){

                //$returnHTML = view('admin.ajax_view.sub_category')->with('subcateList', $subcateList)->render();
                $returnHTML[0] = [
                    'id' => 1,
                    'text' => "SubcatGory",
                ];
                $returnHTML[1] = [
                    'id' => 1,
                    'text' => "NonSubcate",
                ];
                return response()->json(array('error' => false, 'html'=>$subcateList));
               /* echo \Form::select('itemSubCategory',$subcateList, null,
                    ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'itemSubCategory']);*/

            }else{
                echo json_encode(['error'=>'true','message'=>"Category Not Found"]);
            }

        }
    }

    #make subcategory for ajax
    public function make_subcate_ajax($data_list){
        $select = array();
        $i = 0;
        foreach ($data_list as $data){
            $select[$i++] =
            [
                'id' => $data->id,
                'text' => $data->name,
            ];
        }
        return $select;
    }
    #get item subcategory list
    public function get_sub_category_list(){
        $category_list = Category::where(['is_delete'=>0,'parent_id'=>0])->select('id','name')->get();
        $cateList = array();
        foreach ($category_list as $category){
            $cateList[$category->id] = $category->name;
        }

        $subcateList = Category::with(['category'])->where(['is_delete'=>'0'])->where('parent_id','>','0')->get();
        return view('admin.item.subcategory',['cateList'=>$cateList,'subcateList'=>$subcateList]);
    }
    #make new category
    public function make_category(Request $request){
        $has_error = $this->validate_cate_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $category = Category::create( [
                'name' => $request->input('categoryName')
            ]);
            if($category){
                return redirect()->back()->with('message','Category insert success');
            }else{
                return redirect()->back()->withErrors("Category insert fail")->withInput();
            }
        }
    }

    #validate category item
    private function validate_cate_req($request,$category_id=null){
        $request['name'] = $request['categoryName'];
        unset($request['categoryName']);
        $validator=  Validator::make($request, [
            'name' => [
                'required',
                Rule::unique('categories')->ignore($category_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
        ],[
            'name.required' => 'Category name is required',
            'name.unique' => 'The category name has already been taken',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }

    #delete category
    public function delete_category(Request $request){
        $validator=  Validator::make($request->all(), [
            'category_id' => 'exists:categories,id',
        ],[
            'category_id.exists' => 'Category not found',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
             $cate_id = $request->input('category_id');
            Category::where(['parent_id'=>$cate_id])->orWhere('id','=',$cate_id)->update(['is_delete'=>1]);
            echo json_encode(['error'=>'false','message'=>"Category Delete Success"]);
        }
    }

    #insert new sub-category
    public function insert_sub_category(Request $request){
        $has_error = $this->varified_sub_cate_req($request);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $subcate = Category::create( [
                'name' => $request->input('name'),
                'parent_id'=> $request->input('category_id'),
            ]);
            if($subcate){
                return redirect()->back()->with('message','Subcategory insert success');
            }else{
                return redirect()->back()->withErrors("Subcategory insert fail")->withInput();
            }
        }
    }

    #verified subcategory request
    private function varified_sub_cate_req($request,$sub_cate_id=null){

        $request['name'] = $request['subCategoryName'];
        unset($request['subCategoryName']);
        $validator=  Validator::make($request->all(), [
            'category_id' => 'required|exists:categories,id',
            'name' => [
                'required',
                Rule::unique('categories')->ignore($sub_cate_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],

        ],[
            'category_id.required' => 'Category name is required',
            'category_id.exists' => 'Category name not found',
            'name.required' => 'Sub-Category name is required',
            'name.unique' => 'Sub-Category name is already been taken',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }

    #update subcategory data info
    public function update_sub_category(Request $request){
        $sub_cate_id = $request->input('subCategory_id');
        $has_error = $this->varified_sub_cate_req($request,$sub_cate_id);

        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $subcate = Category::where(['id'=>$sub_cate_id])->update( [
                'name' => $request->input('subCategoryName'),
                'parent_id'=> $request->input('category_id'),
            ]);
            if($subcate){
                return redirect()->back()->with('message','Subcategory update success');
            }else{
                return redirect()->back()->withErrors("Subcategory update fail")->withInput();
            }
        }
    }
    #update item
    public function update_item(Request $request){
        $product_id = $request->input('item_id');
        $has_error = $this->validate_new_item_req($request->all(),$product_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            //start transaction
            DB::beginTransaction();
            try {
                $last_import_update = $request->input('productLastImport');
                $last_transaction  = Transaction::where(['product_id'=>$product_id,'type'=>'in'])->orderBy('id','desc')->select('product_qty','warehouse_id')->first();
                $last_import  = $last_transaction->product_qty;
                $get_diff = $last_import_update-$last_import;
                $warehouse_id = $last_transaction->warehouse_id;

                Product::where(['id' => $product_id])->update([
                    'serial_no' => $request->input('serial_no'),
                    'name' => $request->input('itemName'),
                    'category_id' => $request->input('itemCategory'),
                    'sub_category_id' => $request->input('itemSubCategory'),
                    'brand_id' => $request->input('brandName'),
                    'item_tk_rate' => $request->input('itemRate'),
                    'item_dollar_rate' => $request->input('itemDollarRate'),
                    'product_details' => $request->input('productDetails'),
                    'stock'=>DB::raw("stock+".$get_diff)
                ]);
                $transaction =Transaction::where(['product_id'=>$product_id,'type'=>'in'])->orderBy('id','desc')->first();

                Transaction::where(['product_id'=>$product_id,'type'=>'in'])->where('id','>=',
                    $transaction->id)->orderBy('id','desc')->limit(1)
                    ->update([
                    'product_qty'=> DB::raw('product_qty + '.$get_diff),
                    'product_dollar_rate'=>$request->input('itemDollarRate'),
                    'currency'=>$request->input('itemCurrency'),
                ]);
                Transaction::where(['product_id'=>$product_id])->where('id','>=',$transaction->id)->orderBy('id','desc')
                    ->update([
                        'product_status'=> DB::raw('product_status + '.$get_diff)
                    ]);

                Stock::where([
                    'product_id' =>$product_id ,
                    'warehouse_id'=>$warehouse_id,
                ])->update([
                    'product_qty'=>DB::raw('product_qty + '.$get_diff),
                ]);
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors("Item update fail")->withInput();
            }
            DB::commit();
            return redirect()->back()->with('message','Item update successfully done');
        }
    }

    #delete an item by ajax request
    public function delete_item(Request $request){
        $product_id = $request->input('item_id');
        $validator=  Validator::make($request->all(), [
            'item_id' => 'required|exists:products,id',
        ],[
            'item_id.required' => 'Product details not found',
            'item_id.exists' => "Product id doesn't match",
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            Product::where(['id'=>$product_id])->update(['is_delete'=>1]);
            echo json_encode(['error'=>false,'message'=>"Item delete success"]);
        }
    }

    #get item details by ajax
    public function get_item_details(Request $request){
        $validator=  Validator::make($request->all(), [
            'item_id' => 'required|exists:products,id',
        ],[
            'item_id.required' => 'Product details not found',
            'item_id.exists' => "Product id doesn't match",
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $product_id = $request->input('item_id');
            $product_details = $itemList = Product::with(['category','sub_category','brand'])->where(['id'=>$product_id])->first();
            $transaction = Transaction::with(['vendor','warehouse'])->where(['product_id'=>$product_id,'type'=>'in'])->orderBy('id','desc')->first();
            $last_import = ($transaction)?$transaction->product_qty:0;
            $warehouse = ($transaction)?$transaction->warehouse->name:"N/A";
            $vendor = ($transaction)?$transaction->vendor->name:"N/A";
            $dollar = ($transaction)?$transaction->product_dollar_rate:"N/A";
            $currency = ($transaction)?$transaction->currency:"N/A";
            $trans_data = array(
                'last_import'=>$last_import,
                'warehouse'=>$warehouse,
                'vendor'=>$vendor,
                'dollar'=>$dollar,
                'currency'=>$currency,
            );
            echo json_encode(['error'=>false,'message'=>$product_details,'transaction'=>$trans_data]);
        }
    }

    #get item details for transaction data
    public function get_item_info(Request $request){
        $validator=  Validator::make($request->all(), [
            'item_id' => 'required|exists:products,id',
        ],[
            'item_id.required' => 'Product details not found',
            'item_id.exists' => "Product id doesn't match",
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $product_id = $request->input('item_id');
            $product_details  = Product::where(['id'=>$product_id])->first();
            echo json_encode(['error'=>false,'message'=>$product_details]);
        }
    }
}
