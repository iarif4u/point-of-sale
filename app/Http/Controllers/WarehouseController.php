<?php

namespace App\Http\Controllers;

use App\Product;
use App\Stock;
use App\Warehouse;
use App\WarehouseLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
class WarehouseController extends Controller
{
    #get view of item list
    public function get_warehouse_list(){
        $warehouse_list = Warehouse::where(['is_delete'=>0])->get();
        return view('admin.warehouse.warehouse',['warehouse_list'=>$warehouse_list]);
    }

    #make new warehouse
    public function make_warehouse(Request $request){
        $has_error = $this->validate_warehouse_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer = Warehouse::create( [
                'name' => $request->input('warehouseName'),
                'phone'=> $request->input('warehouseMobile'),
                'address'=> $request->input('warehouseAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Warehouse insert success');
            }else{
                return redirect()->back()->withErrors("Warehouse insert fail");
            }
        }
    }

    #validate warehouse request
    private function validate_warehouse_req($request,$warehouse_id=null){
        $request['phone'] = $request['warehouseMobile'];
        $request['name'] = $request['warehouseName'];
        unset($request['warehouseName']);
        unset($request['warehouseMobile']);
        $validator=  Validator::make($request, [
            'warehouse_id' => 'exists:warehouses,id',
            'name' => [
                'required',
                Rule::unique('warehouses')->ignore($warehouse_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'phone' => [
                'required',
                'regex:/^01\d{9}$/',
                Rule::unique('warehouses')->ignore($warehouse_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'warehouseAddress' => 'required',
        ],[
            'warehouse_id.exists' => 'Warehouse not found',
            'warehouseName.required' => 'Warehouse name is required',
            'warehouseName.unique' => 'Warehouse name has already been taken',
            'phone.required' => 'Warehouse mobile number is required',
            'phone.unique' => 'Warehouse mobile number has already been taken',
            'phone.regex' => 'Warehouse mobile number is invalid',
            'warehouseAddress.required' => 'Warehouse address is required',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    #update/edit warehouse info
    public function update_warehouse(Request $request){
        $warehouse_id = $request->input('warehouse_id');
        $has_error = $this->validate_warehouse_req($request->all(),$warehouse_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer =Warehouse::where('id', $warehouse_id)->update([
                'name' => $request->input('warehouseName'),
                'phone'=> $request->input('warehouseMobile'),
                'address'=> $request->input('warehouseAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Warehouse update success');
            }else{
                return redirect()->back()->withErrors("Warehouse update fail");
            }
        }
    }

    #delete warehouse
    public function delete_warehouse(Request $request){
        $validator=  Validator::make($request->all(), [
            'warehouse_id' => 'exists:warehouses,id',
        ],[
            'warehouse_id.exists' => 'Warehouse not found',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $user = Warehouse::find($request->input('warehouse_id'));
            $user->is_delete = 1;
            $user->save();
            echo json_encode(['error'=>'false','message'=>"Warehouse Delete Success"]);
        }
    }

    //transfer warehouse product
    public function warehouse_transfer(){
        $warehouse_info = Warehouse::where(['is_delete'=>0])->get();
        $product_info = Product::where(['is_delete'=>0])->get();
        $product_serial = $this->make_serial_name_select($product_info,'serial_no','name');
        $warehouse = $this->make_collection_select($warehouse_info);
        $stock_info =Stock::with(['product','warehouse'])->get();
        return view('admin.warehouse.transfer',['stock_info'=>$stock_info,'warehouse'=>$warehouse,'product_serial'=>$product_serial]);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list,$name='name'){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->id] = $data->$name;
        }
        return $select;
    }

    #make collection list to array for select tag
    private function make_serial_name_select($data_list,$serial,$name='name'){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->id] = $data->$serial." - ". $data->$name;
        }
        return $select;
    }

    //get the product quantity by individual warehouse
    public function warehouse_product_qty(Request $request){
        $product_id = $request->input('product_id');
        $warehouse_id = $request->input('warehouse_id');
        $stock = Stock::where(['product_id'=>$product_id,'warehouse_id'=>$warehouse_id])->select('product_qty')
            ->first();
        if ($stock){
            return $stock->product_qty;
        }
        return 0;
    }
    //transfer warehouse quantity
    public function transfer_warehouse_stock(Request $request){
        $validator=  Validator::make($request->all(), [
            'from_warehouse' => 'required|numeric|exists:warehouses,id|different:to_warehouse',
            'to_warehouse' => 'required|numeric|exists:warehouses,id|different:from_warehouse',
            'product_id' => 'required|numeric|exists:products,id',
            'product_quantity' => 'required|numeric',
        ],[
            'from_warehouse.required' => 'Form warehouse not found',
            'from_warehouse.numeric' => 'FormWarehouse not found',
            'from_warehouse.exists' => 'Form Warehouse not found',
            'from_warehouse.different' => 'Form Warehouse can not be same as to warehouse',
            'to_warehouse.required' => 'To warehouse not found',
            'to_warehouse.numeric' => 'To warehouse not found',
            'to_warehouse.exists' => 'To warehouse not found',
            'to_warehouse.different' => 'To warehouse can not be same as from warehouse',
            'product_id.required' => 'Product id required',
            'product_id.numeric' => 'Product id is invalid',
            'product_id.exists' => 'Product id not found',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else {
            $from_warehouse = $request->input('from_warehouse');
            $to_warehouse = $request->input('to_warehouse');
            $product_id = $request->input('product_id');
            $product_quantity = $request->input('product_quantity');

            $from_stock = Stock::where(['product_id' => $product_id, 'warehouse_id' => $from_warehouse])->first();
            if ($from_stock) {
                $in_from_stock = $from_stock->product_qty;
                if ($product_quantity>$in_from_stock){
                    return redirect()->back()->withErrors(['Transfer quantity must be less than in stock quantity'])
                        ->withInput();
                }else{
                    //start transaction
                    DB::beginTransaction();
                    try{
                        $new_stock = Stock::firstOrCreate([
                            'product_id' =>$product_id ,
                            'warehouse_id'=>$to_warehouse,
                        ])->increment('product_qty',$product_quantity);

                        Stock::where([
                            'product_id' =>$product_id ,
                            'warehouse_id'=>$from_warehouse,
                        ])->update([
                            'product_qty'=>DB::raw('product_qty - '.$product_quantity),
                        ]);
                        WarehouseLog::create([
                            'from_warehouse'=>$from_warehouse,
                            'to_warehouse'=>$to_warehouse,
                            'product'=>$product_id,
                            'quantity'=>$product_quantity,
                            'note'=>'Transfer Success'
                        ]);
                    }catch (\Exception $exception){
                        DB::rollback();
                        return redirect()->back()->withErrors("Warehouse transfer fail")->withInput();
                    }
                    DB::commit();
                    return redirect()->back()->with('message','Warehouse transfer successfully done');
                }
            }else{
                return redirect()->back()->withErrors("Warehouse has not the product stock")->withInput();
            }
        }
    }
    //get the warehouse transfer log
    public function warehouse_transfer_log(){
        $logList = WarehouseLog::with(['warehouse_from','warehouse_to','product_data'])->orderBy('id', 'DESC')->get();
        return view('admin.warehouse.transfer_log',['logList'=>$logList]);
    }
}
