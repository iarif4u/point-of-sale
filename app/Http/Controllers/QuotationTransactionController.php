<?php

namespace App\Http\Controllers;

use App\QuotationTransaction;
use Illuminate\Http\Request;

class QuotationTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuotationTransaction  $quotationTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationTransaction $quotationTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuotationTransaction  $quotationTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(QuotationTransaction $quotationTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuotationTransaction  $quotationTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationTransaction $quotationTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuotationTransaction  $quotationTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuotationTransaction $quotationTransaction)
    {
        //
    }
}
