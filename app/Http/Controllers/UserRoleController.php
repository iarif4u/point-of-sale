<?php

namespace App\Http\Controllers;

use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use DB;
class UserRoleController extends Controller
{
    //get new user create view
    public function get_user_view(){

        return view('admin.user_role.new_user');
    }

    //make new user with different role
    public function set_new_user(Request $request){
       $has_error=  $this->valid_user($request->all());
       if($has_error){
           return redirect()->back()->withErrors($has_error)->withInput();
       }else{

           $roleList = $request->input('role');
           //start transaction
           DB::beginTransaction();
           try{
                $user =User::create([
                    'name'=>$request->input('name'),
                    'email'=>$request->input('email'),
                    'phone'=>$request->input('phone'),
                    'role'=>'seller',
                    'is_delete'=>'0',
                    'password'=>$this->make_password($request->input('password'))
                ]);
                foreach ($roleList as $role){
                    UserRole::create([
                        'user_id'=>$user->id,
                        'role'=>$role,
                        'added_by'=>auth()->user()->getAuthIdentifier()
                    ]);
                }

           }catch (\Exception $exception){
               DB::rollback();
               return redirect()->back()->withErrors("New user creation failed".$exception->getMessage())->withInput();
           }
           DB::commit();
           return redirect()->back()->with('message','New user create successfully done');
       }
    }

    //check new user request validation
    private function valid_user($request,$user_id=null){
        $validator= Validator::make($request, [
            'name' => [
                'required',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'phone' => [
                'required',
                'numeric',
                'digits:11',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
                ],
            'password' => 'required|min:6',
            'role' => 'required|array',
            'role.*' => 'required',
        ],[
            'name.required'=>"User name is required",
            'phone.required'=>"Phone number is required",
            'phone.numeric'=>"Phone number is invalid",
            'phone.unique'=>"Phone number has already been taken",
            'email.required'=>"Email address is required",
            'email.email'=>"Email address is invalid",
            'email.unique'=>"Email address has already been taken",
            'password.required'=>"Password is required",
            'role.required'=>"User role not found",
            'role.array'=>"User role is invalid",
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }

    //make password as hash
    private function make_password($password){
        return Hash::make($password);
    }

    //get register user list with role for update and delete
    public function get_user_list(){
        $user_list = User::with('role')
            ->where('id','!=',auth()->id())
            ->where(['is_delete'=>0])
            ->select(['id','name','email','phone'])
            ->get();
        return view('admin.user_role.user_list',['user_list'=>$user_list]);
    }

    //delete an user by post request
    public function delete_user(Request $request){
        $validator= Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:users,id'
        ],[
            'user_id.required'=>"User name is required",
            'user_id.numeric'=>"User id invalid",
            'user_id.exists'=>"User not found",
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $user = User::find($request->input('user_id'));
            $user->is_delete = 1;
            $user->save();
            echo json_encode(['error'=>'false','message'=>"Delete Success"]);
        }
    }

    //update user by post request
    public function update_user_view(Request $request){
        $validator= Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:users,id'
        ],[
            'user_id.required'=>"User name is required",
            'user_id.numeric'=>"User id invalid",
            'user_id.exists'=>"User not found",
        ]);
        if ($validator->fails())
        {
            return redirect(back())->withErrors($validator->errors()->all());
        }else{
            $user = User::with('role')->where(['id'=>$request->input('user_id')])->first();
            return view('admin.user_role.update',['user'=>$user]);
        }
    }
    //update user data
    public function update_user_info(Request $request)
    {
        $user_id = $request->input('user_id');
        $has_error = $this->validate_update_request($request->all(),$user_id);
        if ($has_error) {
            return redirect()->back()->withErrors($has_error)->withInput();
        } else {
            $roleList = $request->input('role');
            $name = $request->input('name');
            $phone = $request->input('phone');
            $email = $request->input('email');
            $password = $request->input('password');

            //start transaction
            DB::beginTransaction();
            try{
                $user = User::findorfail($user_id);
                $user->name = $name;
                $user->phone = $phone;
                $user->email = $email;
                if ($password!=null){
                    $user->password = $this->make_password($password);
                }
                $user->save();
                UserRole::where(['user_id'=>$user_id])->delete();
                foreach ($roleList as $role){
                    UserRole::create([
                        'user_id'=>$user->id,
                        'role'=>$role,
                        'added_by'=>auth()->user()->getAuthIdentifier()
                    ]);
                }

            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors("User update failed")->withInput();
            }
            DB::commit();
            return redirect()->back()->with('message','User update successfully done');
        }
    }

    //validate update request
    private function validate_update_request($request,$user_id){
        if($request['password']==null){
            unset($request['password']);
        }
        $validator= Validator::make($request, [
            'name' => [
                'required',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'phone' => [
                'required',
                'numeric',
                'digits:11',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'password' => 'min:6',
            'role' => 'required|array',
            'role.*' => 'required',
        ],[
            'name.required'=>"User name is required",
            'phone.required'=>"Phone number is required",
            'phone.numeric'=>"Phone number is invalid",
            'phone.unique'=>"Phone number has already been taken",
            'email.required'=>"Email address is required",
            'email.email'=>"Email address is invalid",
            'email.unique'=>"Email address has already been taken",
            'role.required'=>"User role not found",
            'role.array'=>"User role is invalid",
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
}
