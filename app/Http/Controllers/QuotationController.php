<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Product;
use App\Quotation;
use App\QuotationTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use PDF;
class QuotationController extends Controller
{
    #get the view of quotation list
    public function get_quotation_view(){
        $quotationList = Quotation::with('customer')->get();
        return view('admin.quotation.quotation',['quotationList'=>$quotationList]);
    }

    #get the register new one quotation
    public function make_quotation(Request $request){
        $has_error = $this->validate_sell_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error);
        }else{
            $product_id =  $request->input('product_id');
            $product_qty =  $request->input('product_qty');
            $product_price = $request->input('product_price');
            $customer_id = $request->input('customer_id');
            $ref_note = ($request->input('ref_note'))?$request->input('ref_note'):null;
            DB::beginTransaction();
            try {
                $quotation = Quotation::create([
                                'customer_id'=>$customer_id,
                                'ref_note'=>$ref_note,
                                'total_tk'=>0
                            ]);
                try{
                    for ($i =0; $i<sizeof($product_id);$i++){
                        QuotationTransaction::create([
                            'product_id'=>$product_id[$i],
                            'product_qty'=>$product_qty[$i],
                            'product_rate'=>0,
                            'customer_id'=>$customer_id,
                            'status'=>1,
                            'quotation_no'=>$quotation->id
                        ]);
                    }
                    DB::commit();
                    return redirect()->route('admin.quotation.get_quotation_details',['id'=>$quotation->id])->with('message',"Quotation insert successfully done");
                }catch (\Exception $e){
                    DB::rollback();
                    return redirect()->back()->withErrors('Quotation insert fail'.$e->getMessage());
                }
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors('Quotation insert fail'.$e->getMessage());
            }

        }
    }


    #validation form request
    private function validate_sell_req($request){
        $validator=  Validator::make($request, [
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'product_qty' => 'required|array',
            'product_qty.*' => 'required|numeric',
            'product_id.*' => 'required|exists:products,id',
        ],[
            'customer_id.exists' => 'Customer not found',
            'product_id.array' => 'Product list invalid',
            'warehouse_id.array' => 'Warehouse list invalid',
            'product_qty.array' => 'Product quantity list invalid',
            'product_id.*.exists' => 'Item not found',
            'product_price.array' => 'Product price list invalid',
            'product_price.*.numeric' => 'Product price is invalid',
            'product_qty.*.numeric' => 'Invalid product quantity data',
            'total_product_price.numeric' => 'Total amount price is invalid',
            'total_product_price.required' => 'Total amount price is required',
            'ref_note.required' => 'Reference note is required',
            'ref_note.max' => 'Reference note has invalid length it\'s must be less 191 characters',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    public  function get_new_quotation_view(){
        $customerList = $this->make_collection_select(Customer::where(['is_delete'=>0])->get());
        $productList = Product::where(['is_delete'=>0])->get();
        return view('admin.quotation.make_new_quotation',['productList'=>$productList,'customerList'=>$customerList]);
    }
    #make collection list to array for select tag
    private function make_collection_select($data_list){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->id] = $data->name;
        }
        return $select;
    }

    #get details quotation by it's id
    public function get_quotation_details($quotation_id){
        $quotation = Quotation::with(['customer','transaction'])->where(['id'=>$quotation_id])->first();
        return view('admin.quotation.quotation_details',['quotation'=>$quotation]);
    }

    #get quotation pdf download
    public function get_quotation_pdf_download($quotation_id){
        $quotation = Quotation::with(['customer','transaction'])->where(['id'=>$quotation_id])->first();
        if ($quotation){
            $pdf = PDF::loadView('pdf.pages.quotation', ['quotation'=>$quotation]);
            return $pdf->stream('Quotation No '.$quotation_id.' '.date('d-m-Y').'.pdf');
        }else{
            return redirect()->route('admin.quotation.get_quotation_view')->withErrors('Sorry, Quotation not found');
        }
    }
    #print the quotation
    public function get_quotation_print($quotation_id){
        $quotation = Quotation::with(['customer','transaction'])->where(['id'=>$quotation_id])->first();
        if ($quotation){
            return view('print.pages.quotation',['quotation'=>$quotation]);
        }else{
            return redirect()->route('admin.quotation.get_quotation_view')->withErrors('Sorry, Quotation not found');
        }
    }

}

