<?php

namespace App\Http\Controllers;
use Illuminate\Validation\Rule;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class BrandController extends Controller
{
    #get view of brand list
    public function get_brand_view(){
        $brandList = Brand::where(['is_delete'=>0])->get();
        return view('admin.brand.brand',['brandList'=>$brandList]);
    }
    #make new brand
    public function make_brand(Request $request){
        $has_error = $this->validate_brand_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $brand = Brand::create( [
                'name' => $request->input('name'),
                'phone'=> $request->input('phone'),
                'contact_person'=> $request->input('brandContactPerson'),
                'address'=> $request->input('brandAddress'),
            ]);
            if($brand){
                return redirect()->back()->with('message','Brand insert success');
            }else{
                return redirect()->back()->withErrors("Brand insert fail")->withInput();
            }
        }
    }

    #validatation brand insert request
    private function validate_brand_req($request,$brand_id=null){
        $validator=  Validator::make($request, [
            'brand_id' => 'exists:brands,id',
            'name' => [
                'required',
                Rule::unique('brands')->ignore($brand_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'phone' => [
                'required',
                'regex:/^01\d{9}$/'
            ],
            'brandContactPerson' => 'required',
            'brandAddress' => 'required',
        ],[
            'brand_id.exists' => 'Brand not found',
            'name.required' => 'Brand name is required',
            'name.unique' => 'Brand name has already been taken',
            'phone.required' => 'Brand mobile number is required',
            'phone.unique' => 'Brand mobile number has already been taken',
            'phone.regex' => 'Brand mobile number is invalid',
            'brandContactPerson.required' => 'Brand contact person name is required',
            'brandAddress.required' => 'Brand address is required',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    //update brand data
    public function update_brand(Request $request){
        $brand_id = $request->input('brand_id');
        $has_error = $this->validate_brand_req($request->all(),$brand_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer =Brand::where('id', $brand_id)->update([
                'name' => $request->input('name'),
                'phone'=> $request->input('phone'),
                'contact_person'=> $request->input('brandContactPerson'),
                'address'=> $request->input('brandAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Brand update success');
            }else{
                return redirect()->back()->withErrors("Brand update fail")->withInput();
            }
        }
    }
    #delete a brand by ajax request
    public function delete_brand(Request $request){
        $validator=  Validator::make($request->all(), [
            'brand_id' => 'exists:brands,id'
        ],[
            'brand_id.exists' => 'Brand not found',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $user = Brand::find($request->input('brand_id'));
            $user->is_delete = 1;
            $user->save();
            echo json_encode(['error'=>'false','message'=>"Delete Success"]);
        }
    }
}
