<?php

namespace App\Http\Controllers;

use App\Product;
use App\ReturnModel;
use App\Stock;
use App\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use DB;
class ReturnController extends Controller
{
    #get return product view
    public function get_return_details($return_id){
        $returnDetails = ReturnModel::with(['sell','customer','transaction'])->where(['id'=>$return_id])->first();

        return view('admin.return_product.return_details',['returnDetails'=>$returnDetails]);
    }

    #get return history
    public function get_return_history(){
        $returnList = ReturnModel::with(['sell','customer'])->get();
        return view('admin.return_product.return_history',['returnList'=>$returnList]);
    }

    #get product price
    private function get_product_price($trans_id){
        return Transaction::where(['id'=>$trans_id])->select('product_rate')->first();
    }

    #get product price
    private function get_product_stock($product_id){
        return Product::where(['id'=>$product_id])->select('stock')->first();
    }
    #get return operation
    public function get_return_product(Request $request){
        $sell_id = $request->input('sell_id');
        $customer_id = $request->input('customer_id');
        $product_id = $request->input('product_id');
        $warehouse_id = $request->input('warehouse_id');
        $sell_qty = $request->input('sell_qty');
        $return_qty = $request->input('return_qty');
        $customer_get_return_tk = $request->input('customer_get_return_tk');
        $transaction_id = $request->input('transaction_id');
        $has_error = $this->validate_return_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error);
        }else{
            //start transaction
            DB::beginTransaction();
            try{
                $return = ReturnModel::create([
                    'sell_id' =>$sell_id,
                    'customer_id'=>$customer_id,
                    'return_product_price'=>0,
                    'customer_get_return'=>$customer_get_return_tk
                ]);
                try{
                    $return_id = $return->id;
                    for ($i=0;$i<sizeof($product_id);$i++){
                        $product = $product_id[$i];
                        $trans_id = $transaction_id[$i];
                        $transaction = $this->get_product_price($trans_id);
                        $warehouse = $warehouse_id[$i];
                        $product_stock = $this->get_product_stock($product);
                        Stock::where(['product_id' =>$product ,'warehouse_id'=>$warehouse])->increment('product_qty',$return_qty[$i]);
                        Product::where(['id'=>$product])->increment('stock',$return_qty[$i]);
                        $insert_row[]  = [
                            'product_id'=>$product,
                            'product_qty'=>$return_qty[$i],
                            'product_rate'=>$transaction->product_rate,
                            'product_dollar_rate'=>0,
                            'customer_id'=>$customer_id,
                            'warehouse_id'=>$warehouse,
                            'special_note'=>"",
                            'type'=>"return",
                            'status'=>1,
                            'sells_id'=>$return_id,
                            'product_status'=>$product_stock->stock+$return_qty[$i],
                            'created_at'        => Carbon::now(),
                            'updated_at'        => Carbon::now(),
                        ];
                    }
                    try{
                        Transaction::insert($insert_row);
                        DB::commit();
                        return redirect()->route('home')->with("message",'Product return complete');
                    }catch (\Exception $exception){
                        DB::rollback();
                        return redirect()->back()->withErrors('Product return failed. Due to '.$exception->getMessage());
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()->withErrors('Product return failed. Due to '.$e->getMessage());
                }
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->withErrors('Product return failed. Due to '.$e->getMessage());
            }
        }
    }

    #validation form request
    private function validate_return_req($request){
        $validator=  Validator::make($request, [
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'product_id.*' => 'required|exists:products,id',
            'ref_note' => 'max:191',
            'warehouse_id' => 'required|array',
            'sell_qty' => 'required|array',
            'return_qty' => 'required|array',
            'warehouse_id.*' => 'required|exists:warehouses,id',
            'sell_qty.*' => 'required|numeric',
            'return_qty.*' => 'required|numeric',
            'customer_get_return_tk' => 'numeric',

        ],[
            'customer_id.exists' => 'Customer not found',
            'product_id.array' => 'Product list invalid',
            'warehouse_id.array' => 'Warehouse list invalid',
            'product_qty.array' => 'Product quantity list invalid',
            'product_id.*.exists' => 'Item not found',
            'warehouse_id.*.exists' => 'Warehouse not found',
            'customer_get_return_tk.numeric' => 'Customer get amount price is invalid',
            'ref_note.required' => 'Reference note is required',
            'ref_note.max' => 'Reference note has invalid length it\'s must be less 191 characters',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }


}
