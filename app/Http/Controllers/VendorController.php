<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
class VendorController extends Controller
{
    #get view of item list
    public function get_vendor_list(){
        $vendor_list = Vendor::where(['is_delete'=>0])->get();
        return view('admin.vendor.vendor',['vendor_list'=>$vendor_list]);
    }

    #make new vendor
    public function make_vendor(Request $request){
        $has_error = $this->validate_vendor_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer = Vendor::create( [
                'name' => $request->input('vendorName'),
                'phone'=> $request->input('phone'),
                'company'=> $request->input('vendorCompany'),
                'address'=> $request->input('vendorAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Vendor insert success');
            }else{
                return redirect()->back()->withErrors("Vendor insert fail")->withInput();
            }
        }
    }


    #validatation vendor insert request
    private function validate_vendor_req($request,$vendor_id=null){
        $validator=  Validator::make($request, [
            'vendor_id' => 'exists:vendors,id',
            'vendorName' => 'required',
            'phone' => [
                'required',
                'regex:/^01\d{9}$/'
            ],
            'vendorCompany' => 'required',
            'vendorAddress' => 'required',
        ],[
            'vendor_id.exists' => 'Vendor not found',
            'vendorName.required' => 'Vendor name is required',
            'phone.required' => 'Vendor mobile number is required',
            'phone.regex' => 'Vendor mobile number is invalid',
            'vendorCompany.required' => 'Vendor company name is required',
            'vendorAddress.required' => 'Vendor address is required',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }

    #update vendor info
    public function update_vendor(Request $request){
        $vendor_id = $request->input('vendor_id');
        $has_error = $this->validate_vendor_req($request->all(),$vendor_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer =Vendor::where('id', $vendor_id)->update([
                'name' => $request->input('vendorName'),
                'phone'=> $request->input('phone'),
                'company'=> $request->input('vendorCompany'),
                'address'=> $request->input('vendorAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Vendor update success');
            }else{
                return redirect()->back()->withErrors("Vendor update fail")->withInput();
            }
        }
    }

    #delete vendor by ajax request
    public function delete_vendor(Request $request){
        $validator=  Validator::make($request->all(), [
            'vendor_id' => 'exists:vendors,id',
        ],[
            'vendor_id.exists' => 'Vendor not found',
        ]);
        if ($validator->fails())
        {
            echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $user = Vendor::find($request->input('vendor_id'));
            $user->is_delete = 1;
            $user->save();
            echo json_encode(['error'=>'false','message'=>"Vendor Delete Success"]);
        }
    }
}
