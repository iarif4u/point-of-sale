<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Imports\ItemsImport;
use App\Product;
use App\Vendor;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    private $type = "xlsx";

    //get excel category list
    public function get_category_sheet()
    {

        $data = Category::where(['parent_id' => 1, 'is_delete' => 0])->select('name as Category')->get()->toArray();

        \Excel::create('Category_list_' . date('d-m-Y'), function ($excel) use ($data) {
            $excel->setTitle('Category List');
            $excel->sheet('category', function ($sheet) use ($data) {
                $sheet->setAutoSize(true);
                $sheet->fromArray($data);
            });
        })->download($this->type);
    }

    //download subcategory list as excel file
    public function get_sub_category_sheet()
    {
        $dataList = Category::with('sub_category')->get()->toArray();
        $subcateList = array();
        foreach ($dataList as $data) {
            foreach ($data['sub_category'] as $subCate) {
                $subcate['Category'] = $data['name'];
                $subcate['Subcategory'] = $subCate['name'];
                $subcateList[] = $subcate;
            }
        }
        \Excel::create('SubCategory_list_' . date('d-m-Y'), function ($excel) use ($subcateList) {
            $excel->setTitle('Subcategory List');
            $excel->sheet('sub-category', function ($sheet) use ($subcateList) {
                $sheet->setAutoSize(true);
                $sheet->fromArray($subcateList);
            });
        })->download($this->type);
    }

    //import file by excel
    public function import_category_sheet(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();

            $data = \Excel::load($path, function ($reader) {

            })->get();
            if (!empty($data) && $data->count()) {
                $insert = array();
                foreach ($data as $key => $value) {
                    $category = Category::where(['name' => $value->category, 'is_delete' => 0])->first();
                    if (!$category) {
                        $insert[] = [
                            'name' => $value->category,
                            'parent_id' => 0,
                            'status' => 1,
                            'is_delete' => 0,
                            'created_at' => now(),
                            'updated_at' => now(),
                        ];
                    }
                }
                if (sizeof($insert) > 0) {
                    DB::table('categories')->insert($insert);
                    return redirect()->back()->with('message', sizeof($insert) . " category insert successfully done");
                } else {
                    return redirect()->back()->withErrors('Category list item\'s already inserted');
                }
            } else {
                return redirect()->back()->withErrors('Category list is empty');
            }
        } else {
            return redirect()->back()->withErrors('Category file not found');
        }
    }

    //import subcategory by excel file
    public function import_sub_category_sheet(Request $request)
    {
        //check the request has file or not
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();
            //get excel data row to data variable
            $data = \Excel::load($path, function ($reader) {
            })->get();

            //check data has any data or not
            if (!empty($data) && $data->count()) {
                $insert = array();
                $cateList = $this->get_category_list();

                foreach ($data as $key => $value) {

                    if ($value->category && $value->subcategory) {
                        if (!array_key_exists($value->subcategory, $cateList)) {
                            $cate_id = $this->get_category_id($value->category, $cateList);
                            $insert[] = [
                                'name' => $value->subcategory,
                                'parent_id' => $cate_id,
                                'status' => 1,
                                'is_delete' => 0,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }

                    }
                }
                DB::table('categories')->insert($insert);
                return redirect()->back()->with('message', sizeof($insert) . " Sub-category insert successfully done");

            } else {
                return redirect()->back()->withErrors('Sub-category list is empty');
            }
        } else {
            return redirect()->back()->withErrors('Sub-category list file not found');
        }
    }

    //get the category list
    private function get_category_list()
    {
        $cateList = array();
        $category = Category::where(['is_delete' => 0])->get();
        foreach ($category as $cate) {
            $cateList[$cate->name] = $cate->id;
        }
        return $cateList;
    }

    //get the brand list
    private function get_brand_list()
    {
        $dataList = array();
        $dataCollection = Brand::where(['is_delete' => 0])->get();
        foreach ($dataCollection as $data) {
            if (strlen($data->name) != strlen(utf8_decode($data->name))) {
                $dataList[$data->name] = $data->id;
            } else {
                $dataList[strtolower($data->name)] = $data->id;
            }

        }
        return $dataList;
    }

    //get the warehouse list
    private function get_warehouse_list()
    {
        $dataList = array();
        $dataCollection = Warehouse::where(['is_delete' => 0])->get();

        foreach ($dataCollection as $data) {
            if (strlen($data->name) != strlen(utf8_decode($data->name))) {
                $dataList[$data->name] = $data->id;
            } else {
                $dataList[strtolower($data->name)] = $data->id;
            }

        }
        return $dataList;
    }

    //get the vendor list
    private function get_vendor_list()
    {
        $dataList = array();
        $dataCollection = Vendor::where(['is_delete' => 0])->get();
        foreach ($dataCollection as $data) {
            if (strlen($data->name) != strlen(utf8_decode($data->name))) {
                $dataList[$data->name] = $data->id;
            } else {
                $dataList[strtolower($data->name)] = $data->id;
            }

        }
        return $dataList;
    }

    //get the category id by it's name, if not has than insert
    private function get_category_id($cate_name, &$cate_list)
    {
        if (array_key_exists($cate_name, $cate_list)) {
            return $cate_list[$cate_name];
        }
        $category = Category::create([
            'name' => $cate_name,
            'parent_id' => 0,
            'status' => 1,
            'is_delete' => 0
        ]);
        $cate_list[$cate_name] = $category->id;
        return $category->id;
    }

    //get sub category id create new one if not found
    private function get_sub_cate_id($cate_name, $cate_id, &$cate_list)
    {
        if (array_key_exists($cate_name, $cate_list)) {
            return $cate_list[$cate_name];
        }
        $category = Category::create([
            'name' => $cate_name,
            'parent_id' => $cate_id,
            'status' => 1,
            'is_delete' => 0
        ]);
        $cate_list[$cate_name] = $category->id;
        return $category->id;
    }

    //get product sheet list
    public function get_product_sheet()
    {
        $product = Product::with(['category', 'sub_category',])->get();
        dd($product);
    }

    //make value to integer
    private function make_val_int($value)
    {

        $number = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        return (is_numeric($number)) ? $number : 0;
    }

    //check the item already has or not
    private function check_item_name($itemName)
    {
        return Product::where(['name' => $itemName])->first();
    }

    //check the item serial no already has or not
    private function check_item_serial($itemSerial)
    {
        return Product::where(['serial_no' => $itemSerial])->first();
    }

    private function get_currency($value)
    {
        return ($value) ? $value : 'Dollar';
    }

    #get product existences
    private function product_existences($serial, $name, $category, $subcategory, $brand)
    {
        $product = Product::where([
            'serial_no' => $serial,
            'name' => $name,
            'category_id' => $category,
            'sub_category_id' => $subcategory,
            'brand_id' => $brand,
        ])->first();
        return ($product) ? $product : false;
    }

    //import product from excel file
    public function import_product_sheet(Request $request)
    {
        //check the request has file or not
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            try {
                Excel::import(new ItemsImport(), $file);
                return redirect()->back()->with('message', "Item update success");
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors([$exception->getMessage()]);
            }
        }
    }
}
