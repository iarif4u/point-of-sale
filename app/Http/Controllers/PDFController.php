<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use  DB;
use File;
use Illuminate\Support\Facades\Validator;
class PDFController extends Controller
{
    //get the header footer setting in pdf
    public function get_pdf_setting_view(){
        $pdf = Setting::first();
        return view('admin.pdf.setting',['pdf'=>$pdf]);
    }

    //set the pdf header and footer image
    public function pdf_setting(Request $request){
        $validator=  Validator::make($request->all(), [
            'header' =>'nullable|image|mimes:jpeg,png,jpg',
            'footer' =>'nullable|image|mimes:jpeg,png,jpg',
        ],[
            'header.image' => 'Header image is invalid',
            'header.mimes' => 'Header image has invalid format',
            'footer.image' => 'Footer image is invalid',
            'footer.mimes' => 'Footer image has invalid format',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all())->withInput();
        }else{
            DB::beginTransaction();
            try{
                $pdf = Setting::first();
                $destinationPath = public_path('/pdf_img');
                if ($request->hasFile('header')) {
                    $file = $request->file('header');
                    $header = $this->upload_file($destinationPath,$file);
                    if ($pdf){
                        $header_file = $pdf->pdf_header;
                        File::delete($destinationPath.'/'.$header_file);
                        $pdf->pdf_header = $header;
                    }
                }
                if ($request->hasFile('footer')) {
                    $file = $request->file('footer');
                    $footer = $this->upload_file($destinationPath,$file);
                    if($pdf){
                        $footer_file = $pdf->pdf_footer;
                        File::delete($destinationPath.'/'.$footer_file);
                        $pdf->pdf_footer = $footer;
                    }
                }
                if(!$pdf){
                    Setting::create([
                        'pdf_header' =>$header,
                        'pdf_footer' =>$footer,
                    ]);
                }
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors("PDF update setting fail ".$exception->getMessage())->withInput();
            }
            if ($pdf){
                $pdf->save();
            }

            DB::commit();
            return redirect()->back()->with('message','PDF update setting successfully done');
        }

    }

    //upload file
    private function upload_file($destinationPath,$file){
        $file_name =  $number = mt_rand(100000, 999999).".".$file->getClientOriginalExtension();
        $file->move($destinationPath,$file_name);
        return $file_name;
    }
}
