<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerPaid;
use App\DataTables\PosProductsDataTable;
use App\DataTables\SellsDataTable;
use App\Product;
use App\ReturnModel;
use App\Sell;
use App\Setting;
use App\Stock;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
use PDF;
use Yajra\DataTables\Facades\DataTables;

class SellController extends Controller
{

    #view of pos
    public function get_pos_view(PosProductsDataTable $dataTable){
        $stockList = Stock::with(['product','warehouse'])->get();
        $customerList = Customer::where(['is_delete'=>0])->get();
        $customerList = $this->make_collection_select($customerList);
        return $dataTable->render('admin.pos.get_pos_view',['stockList'=>$stockList,'customerList'=>$customerList]);
    }

    public function get_stock_products(){
        $stockList = Stock::with(['product','warehouse'])->get();

        return Datatables::of($stockList)->make();
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list){
        $select[0]= 'Select One';
        foreach ($data_list as $data){
            $select[$data->id] = $data->name;
        }
        return $select;
    }
    //get pos details by id
    public function get_pos_details($sell_id){
        $sells_item =Sell::with(['customer','transaction','return'])->where('id',$sell_id)->first();
        if ($sells_item){
            return view('admin.pos.sold_details',['sells_item'=>$sells_item]);
        }else{
            return redirect()->route('admin.pos.get_sold_view')->withErrors('Sorry, Invoice not found');
        }

    }
    //get sold item view
    public function get_sold_view(SellsDataTable $dataTable){
        $sells_item =Sell::with(['customer','return'])->get();
     //   dd($sells_item);
        return $dataTable->render('admin.pos.sold_item',['sells_item'=>$sells_item]);
    }
    //make new sells for customer
    public function make_new_sell(Request $request){
        $has_error = $this->validate_sell_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error);
        }else{
            $customer_id = $request->input('customer_id');
            $ref_note = $request->input('ref_note');
            $product_list = $request->input('product_id');
            $warehouse_list = $request->input('warehouse_id');
            $product_qty_list = $request->input('product_qty');
            $total_product_price = $request->input('total_product_price');
            $payment_method = $request->input('payment_method');
            $customer_pay = $request->input('customer_pay');
            $customer_due = $request->input('customer_due');
            $invoice_no = $request->input('invoice_no');
            $product_price = $request->input('product_price');
            $counter = 0;
            $total = 0;
            foreach ($product_list as $item_id){
                $price = $this->get_product_price($item_id);
                $qty = $product_qty_list[$counter];
                $total = $total+($qty*$product_price[$counter]);
                $counter++;
            }
            if($total!=$total_product_price){
                return redirect()->back()->withErrors("Input total price doesn't match");
            }else{
                //start transaction
                DB::beginTransaction();
                try {
                    $sell = Sell::create([
                        'customer_id' => $customer_id,
                        'payment_method' => $payment_method,
                        'ref_note' => $ref_note,
                        'invoice_no' => $invoice_no,
                        'paid_tk' => $customer_pay,
                        'total_tk' => $total_product_price
                    ]);
                    $sell_id = $sell->id;
                    $counter = 0;
                    $insert_row = array();

                    try{
                        foreach ($product_list as $item_id){
                            $product_qty = $this->get_product_price($item_id);
                            $qty = $product_qty_list[$counter];
                            Product::where(['id'=>$item_id])->update(['stock'=>DB::raw("stock-".$qty)]);
                            $stock = Stock::firstOrNew(['product_id' =>$item_id ,'warehouse_id'=>$warehouse_list[$counter]]);
                            $stock->product_qty = $stock->product_qty-$qty;
                            $stock->save();
                            $insert_row[]  = [
                                'product_id'=>$item_id,
                                'product_qty'=>$qty,
                                'product_rate'=>$product_price[$counter],
                                'product_dollar_rate'=>0,
                                'customer_id'=>$customer_id,
                                'warehouse_id'=>$warehouse_list[$counter],
                                'special_note'=>$ref_note,
                                'type'=>"out",
                                'status'=>1,
                                'sells_id'=>$sell_id,
                                'product_status'=>$product_qty->stock-$qty,
                                'created_at'        => Carbon::now(),
                                'updated_at'        => Carbon::now(),
                            ];
                            $counter++;
                        }

                        try{
                            Transaction::insert($insert_row);
                            try{
                                if ($customer_pay>0){

                                    CustomerPaid::create([
                                        'amount'=>$customer_pay,
                                        'customer_id'=>$customer_id,
                                        'payment_method'=>$payment_method,
                                        'reference'=>$ref_note,
                                        'type'=>"Paid",
                                        'receiver_id'=>auth()->user()->getAuthIdentifier(),
                                        'sell_id'=>$sell_id,
                                    ]);

                                }
                                DB::commit();
                                return redirect()->route('admin.pos.get_pos_details',['id'=>$sell_id])->with('message','Transaction has successfully done');
                            }catch (\Exception $e){
                                DB::rollback();
                                return redirect()->back()->withErrors("Sell doesn't complete ".$e->getMessage());
                            }
                        }catch (\Exception $e) {
                            DB::rollback();
                            return redirect()->back()->withErrors("Sell doesn't complete ".$e->getMessage());
                        }
                    }catch (\Exception $e) {
                        DB::rollback();
                        return redirect()->back()->withErrors("Sell doesn't complete ".$e->getMessage());
                    }

                }catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()->withErrors("Sell doesn't complete ".$e->getMessage());
                }

            }

        }
    }

    #validation form request
    private function validate_sell_req($request){
        $validator=  Validator::make($request, [
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'ref_note' => 'max:191',
            'warehouse_id' => 'required|array',
            'product_qty' => 'required|array',
            'product_qty.*' => 'required|numeric',
            'total_product_price' => 'required|numeric',
            'customer_pay' => 'required|numeric',
            'customer_due' => 'required|numeric',
            'product_id.*' => 'required|exists:products,id',
            'warehouse_id.*' => 'required|exists:warehouses,id',
        ],[
            'customer_id.exists' => 'Customer not found',
            'product_id.array' => 'Product list invalid',
            'warehouse_id.array' => 'Warehouse list invalid',
            'product_qty.array' => 'Product quantity list invalid',
            'product_id.*.exists' => 'Item not found',
            'warehouse_id.*.exists' => 'Warehouse not found',
            'product_qty.*.numeric' => 'Invalid product quantity data',
            'total_product_price.numeric' => 'Total amount price is invalid',
            'total_product_price.required' => 'Total amount price is required',
            'customer_pay.numeric' => 'Customer paid amount price is invalid',
            'customer_pay.required' => 'Customer paid amount price is required',
            'customer_due.numeric' => 'Customer due amount price is invalid',
            'customer_due.required' => 'Customer due amount price is required',
            'ref_note.required' => 'Reference note is required',
            'ref_note.max' => 'Reference note has invalid length it\'s must be less 191 characters',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    #get product price
    private function get_product_price($product_id){
        return Product::where(['id'=>$product_id])->select('item_tk_rate','stock')->first();
    }

    #invoice pdf download
    public function get_invoice_download($invoice_id){
        $sells_item =Sell::with(['customer','transaction','return'])->where('id',$invoice_id)->first();
        $pdf = Setting::first();
        //return view('pdf.pages.invoice',['quotation'=>$sells_item]);
        if ($sells_item){
            $invoice = date('Ymdh',strtotime($sells_item->created_at)).$invoice_id;
            $pdf = PDF::loadView('pdf.pdf', ['sells_item'=>$sells_item,'pdf'=>$pdf]);
            return $pdf->stream('Invoice No '.$invoice.' '.date('d-m-Y').'.pdf');
        }else{
            return redirect()->route('admin.pos.get_sold_view')->withErrors('Sorry, Invoice not found');
        }
    }
    #print the invoice
    public function get_invoice_print($invoice_id){
        $sells_item =Sell::with(['customer','transaction'])->where('id',$invoice_id)->first();
        if ($sells_item){
            return view('print.adminlte.invoice',['sells_item'=>$sells_item]);
            //return view('print.pages.invoice',['quotation'=>$sells_item]);
        }else{
            return redirect()->route('admin.pos.get_sold_view')->withErrors('Sorry, Invoice not found');
        }
    }
    //make new return amount
    public function make_new_return(Request $request){
        $validator=  Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'sell_id' => 'required|exists:sells,id',
            'payment_amount' =>  'required|numeric',
            'payment_method' =>  'required',
        ],[
            'customer_id.required' => 'Customer not found',
            'customer_id.exists' => 'Customer not found',
            'sell_id.exists' => 'Sell id not found',
            'sell_id.required' => 'Sell id is required',
            'payment_amount.required' => 'Return amount is required',
            'payment_amount.numeric' => 'Return amount is must be numeric',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all());
        }else{
            $sell_id = $request->input('sell_id');
            $customer_id = $request->input('customer_id');
            $amount = $request->input('payment_amount');
            DB::beginTransaction();
            try{
                ReturnModel::where(['sell_id' => $sell_id])->increment('customer_get_return', $amount);
                CustomerPaid::create([
                    'amount'=>$amount,
                    'customer_id'=>$customer_id,
                    'payment_method'=>$request->input('payment_method'),
                    'reference'=>$request->input('reference'),
                    'type'=>'Return',
                    'receiver_id'=>auth()->user()->getAuthIdentifier(),
                    'sell_id'=>$sell_id
                ]);

            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors("Return payment doesn't complete ".$exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message',"Return payment complete successfully done");
        }
    }
    //make new payment for due payment
    public function make_new_payment(Request $request){
        $validator=  Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'sell_id' => 'required|exists:sells,id',
            'payment_amount' =>  'required|numeric',
            'payment_method' =>  'required',
        ],[
            'customer_id.required' => 'Customer not found',
            'customer_id.exists' => 'Customer not found',
            'sell_id.exists' => 'Sell id not found',
            'sell_id.required' => 'Sell id is required',
            'payment_amount.required' => 'Payment amount is required',
            'payment_amount.numeric' => 'Payment amount is must be numeric',

        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors()->all());
        }else{
            $sell_id = $request->input('sell_id');
            $customer_id = $request->input('customer_id');
            $amount = $request->input('payment_amount');
            DB::beginTransaction();
            try {
                Sell::where(['id' => $sell_id])->increment('paid_tk', $amount);
                try{
                    CustomerPaid::create([
                        'amount'=>$amount,
                        'customer_id'=>$customer_id,
                        'payment_method'=>$request->input('payment_method'),
                        'reference'=>$request->input('reference'),
                        'type'=>'Paid',
                        'receiver_id'=>auth()->user()->getAuthIdentifier(),
                        'sell_id'=>$sell_id
                    ]);
                }catch (\Exception $exception){
                    DB::rollback();
                    return redirect()->back()->withErrors("Payment doesn't complete ".$exception->getMessage());
                }
            }catch (\Exception $exception){
                DB::rollback();
                return redirect()->back()->withErrors("Payment doesn't complete ".$exception->getMessage());
            }
            DB::commit();
            return redirect()->back()->with('message',"Payment complete successfully done");
        }
    }

}
