<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\DataTables\StocksDataTable;
use App\Product;
use App\Vendor;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Transaction;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param StocksDataTable $dataTable
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(StocksDataTable $dataTable)
    {

        $category_list = Category::where(['is_delete' => 0, 'parent_id' => 0])->get();
        $subcateList = Category::where(['is_delete' => '0'])->where('parent_id', '>', '0')->get();
        $brand_list = Brand::where(['is_delete' => '0'])->get();

        $vendor_list = Vendor::where(['is_delete' => '0'])->get();
        $warehouse_list = Warehouse::where(['is_delete' => '0'])->get();

        $vendor_list = $this->make_collection_select($vendor_list, 'company');
        $category_list = $this->make_collection_select($category_list);

        $warehouse_list = $this->make_collection_select($warehouse_list);
        $brand_list = $this->make_collection_select($brand_list);
        $subcateList = $this->make_collection_select($subcateList);
        return $dataTable->render('admin.pages.home', ['warehouse_list' => $warehouse_list, 'vendor_list' => $vendor_list, 'brand_list' => $brand_list, 'subcateList' => $subcateList, 'category_list' => $category_list]);

    }

    //get product data by data-table ajax request
    public function get_home_products(Request $request)
    {
        $itemList = Product::with(['category', 'sub_category', 'brand', 'stock_warehouse'])->where(['is_delete' => 0])->get();
        $data = array();
        if (isset($itemList)) {
            foreach ($itemList as $item) {
                if ($item->stock_warehouse->count() > 0) {
                    foreach ($item->stock_warehouse as $item_stock) {
                        $sub_array = array();
                        $sub_array[] = '<div class="item_serial">' . $item->serial_no . '</div>';
                        $sub_array[] = '<div class="item_name">' . $item->name . '</div>';
                        $sub_array[] = '<div class="sub-category">' . $item->sub_category->name . '</div>';
                        $sub_array[] = '<div class="brand">' . $item->product_details . '</div>';
                        $sub_array[] = '<div class="warehouse">' . $item_stock->warehouse->name . '</div>';
                        $sub_array[] = '<div class="tk">' . $item->item_tk_rate . '</div>';
                        $sub_array[] = '<div class="dollar">' . $item->item_dollar_rate . '</div>';
                        $sub_array[] = '<div class="qty">' . $item_stock->product_qty . '</div>';
                        $sub_array[] = '<div class="action">
                                            <a onclick="get_item_view(' . $item->id . ')" href="javascript:void(0);" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            <a onclick="get_item_transaction(' . $item->id . ','
                            . $item_stock->warehouse->id . ')"
                                                   href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                        </div>';
                        $data[] = $sub_array;
                    }
                }
            }
        }
        $output = array(
            'draw' => intval($request->input('draw')),
            'data' => $data,
        );
        echo json_encode($output);
    }

    #make collection list to array for select tag
    private function make_collection_select($data_list, $name = 'name')
    {
        $select[0] = 'Select One';
        foreach ($data_list as $data) {
            $select[$data->id] = $data->$name;
        }
        return $select;
    }


    #get transactions details by product id
    public function get_product_transaction(Request $request)
    {
        $product_id = $request->input('product_id');
        $warehouse_id = $request->input('warehouse_id');
        #check request by form validation
        $has_error = $this->validate_product_trans_req($request->all());
        if ($has_error) {
            echo json_encode(['error' => 'true', 'message' => $has_error]);
        } else {

            $transaction_list = Transaction::with(['product', 'customer', 'vendor', 'warehouse'])
                ->where(['product_id' => $product_id])
                ->where(['warehouse_id' => $warehouse_id])
                ->orderBy('id', 'asc')
                ->get();
            $row = 0;
            $current_qty = 0;
            $data = array();
            foreach ($transaction_list as $transaction) {
                $row++;
                $sub_array = array();
                $customer_name = null;
                $vendor_name = null;
                $customer_quantity = null;
                $vendor_quantity = null;
                if ($transaction->customer) {
                    $customer_name = $transaction->customer->name;
                    if ($transaction->type == "return") {

                        $vendor_quantity = $transaction->product_qty . " (Return)";
                    } else {

                        $customer_quantity = $transaction->product_qty;
                    }

                }
                if ($transaction->vendor) {
                    $vendor_name = $transaction->vendor->name;
                    $vendor_quantity = $transaction->product_qty;

                }
                $sub_array[] = date('d-m-Y', strtotime($transaction->created_at));
                $sub_array[] = $transaction->special_note;
                $sub_array[] = ($transaction->product_dollar_rate > 0) ? $transaction->product_dollar_rate : null;
                $sub_array[] = $customer_name;
                $sub_array[] = $vendor_name;
                $sub_array[] = $customer_quantity;
                $sub_array[] = $vendor_quantity;
                $sub_array[] = $transaction->currency;
                $sub_array[] = $transaction->product_status;
                $data[] = $sub_array;
            }
            $output = array(
                'draw' => intval($request->input('draw')),
                'recordsTotal' => $transaction_list->count(),
                'recordsFiltered' => $row,
                'data' => $data,
            );
            echo json_encode($output);
        }
    }

    #validate transaction request
    private function validate_product_trans_req($request)
    {
        $validator = Validator::make($request, [
            'product_id' => 'required|numeric|exists:products,id',
            'warehouse_id' => 'required|numeric|exists:warehouses,id',
        ], [
            'item_id.required' => "Item name is required",
            'item_id.exists' => "Item doesn't match of our record",
            'item_id.numeric' => "Item name is invalid",
            'warehouse_id.required' => "Warehouse name is required",
            'warehouse_id.numeric' => "Warehouse name is invalid",
            'warehouse_id.exists' => "Warehouse doesn't match of our record",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        } else {
            return false;
        }
    }
}
