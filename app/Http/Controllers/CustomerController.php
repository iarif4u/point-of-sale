<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Session;
class CustomerController extends Controller
{
    #get view of customer list
    public function get_customer_list(){
        $customerList = Customer::where(['is_delete'=>0])->get();

        return view('admin.customer.customer',['customerList'=>$customerList]);
    }

    #make/insert new customer
    public function make_customer(Request $request){
        $has_error = $this->validate_customer_req($request->all());
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer = Customer::create( [
                'name' => $request->input('customerName'),
                'phone'=> $request->input('phone'),
                'company'=> $request->input('customerCompany'),
                'address'=> $request->input('customerAddress'),
            ]);
            if($customer){
                Session::flash('customer_id', $customer->id);
                return redirect()->back()->with('message','Customer insert success');
            }else{
                return redirect()->back()->withErrors("Customer insert fail")->withInput();
            }
        }
    }

    #update customer info details
    public function update_customer(Request $request){
        $customer_id = $request->input('customer_id');
        $has_error = $this->validate_customer_req($request->all(),$customer_id);
        if ($has_error){
            return redirect()->back()->withErrors($has_error)->withInput();
        }else{
            $customer =Customer::where('id', $customer_id)->update([
                'name' => $request->input('customerName'),
                'phone'=> $request->input('phone'),
                'company'=> $request->input('customerCompany'),
                'address'=> $request->input('customerAddress'),
            ]);
            if($customer){
                return redirect()->back()->with('message','Customer update success');
            }else{
                return redirect()->back()->withErrors("Customer update fail")->withInput();
            }
        }
    }

    #validatation customer insert request
    private function validate_customer_req($request,$customer_id=null){
        $validator=  Validator::make($request, [
            'customer_id' => 'exists:customers,id',
            'customerName' => 'required',
            'phone' => [
                'required',
                'regex:/^01\d{9}$/',
                Rule::unique('customers')->ignore($customer_id)->where(function ($query) {
                    $query->where('is_delete', 0);
                })
            ],
            'customerCompany' => 'required',
            'customerAddress' => 'required',
        ],[
            'customer_id.exists' => 'Customer not found',
            'customerName.required' => 'Customer name is required',
            'phone.required' => 'Customer mobile number is required',
            'phone.unique' => 'Customer mobile number has already been taken',
            'phone.regex' => 'Customer mobile number is invalid',
            'customerCompany.required' => 'Customer company name is required',
            'customerAddress.required' => 'Customer address is required',
        ]);
        if ($validator->fails())
        {
            return $validator->errors()->all();
        }else{
            return false;
        }
    }
    #delete customer
    public function delete_customer(Request $request){
        $validator=  Validator::make($request->all(), [
            'customer_id' => 'exists:customers,id'
        ],[
            'customer_id.exists' => 'Customer not found',
        ]);
        if ($validator->fails())
        {
             echo json_encode(['error'=>'true','message'=>$validator->errors()->all()]);
        }else{
            $user = Customer::find($request->input('customer_id'));
            $user->is_delete = 1;
            $user->save();
            echo json_encode(['error'=>'false','message'=>"Delete Success"]);
        }
    }
}
