<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnModel extends Model
{
    protected $table = "returns";

    protected $fillable = ['sell_id','customer_id','return_product_price','customer_get_return'];

    //transaction has many transaction
    public function transaction() {
        return $this->hasMany('App\Transaction','sells_id','id')->where(['type'=>'return'])->with(['product','warehouse']);
    }

    //transaction has many return
    public function sell() {
        return $this->hasOne('App\Sell','id','sell_id')->with('transaction');
    }

    //sell has one customer
    public function customer() {
        return $this->hasOne('App\Customer','id','customer_id');
    }

}
