<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('pdf.layout.master')

@section('title',"DifferentCoder || Invoice ")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/print.css')}}">
    <style>
        .calculation,td{
            padding: 5px !important;
        }
        .title{
            font-size: 26px;
            padding: 5px !important;
        }
        .challan-title{
            font-size: 16px !important;
        }
        .font-10{
            font-size: 10px;
        }
        .margin-top-20{
            margin-top: 100px !important;
        }
        .margin-top-inverse-20{
            margin-top: -20px !important;
        }
        .padding-top-inverse-50{
            padding-top: -50px !important;
        }
        .padding-top-inverse-30{
            padding-top: -20px !important;
        }
        .padding-left-150{
            padding-left: 150px !important;
        }
        .padding-left-400{
            padding-left: 80px !important;
        }
        .padding-left-80{
            padding-left: 70px !important;
        }
        .margin-bottom-20{
            margin-top: 20px !important;
        }
        .margin-left-20{
            margin-left: 20px !important;
        }
        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <section class="invoice">
        <htmlpageheader name="page-header">
            @php
                $header = $pdf->pdf_header;
                $footer = $pdf->pdf_footer;
            @endphp
            <img width="100%" height="150px" src="{{asset('pdf_img/'.$header)}}" alt="">
        </htmlpageheader>
        <!-- title row -->

        <div class="row" style="margin-top: 130px">
            <table>
                <tr>
                    <td style="margin-top: -30px !important;">
                        From
                        <address>
                            <strong>Admin, Inc.</strong>
                            Phone: (804) 123-5432<br>
                            Email: info@almasaeedstudio.com
                        </address>
                    </td>
                    <td class="padding-top-inverse-30 padding-left-80">
                        To
                        <address>
                            <strong>@if($quotation->customer){{$quotation->customer->name}}@endif</strong><br>
                            @if($quotation->customer) {{$quotation->customer->address}} @endif
                        </address>
                    </td>
                    <td class="padding-top-inverse-50 padding-left-150">
                        Invoice No :
                        @if(isset($quotation)){{date('Ymdh',strtotime($quotation->created_at)).$quotation->id}}@endif<br>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row margin-bottom-20 ">
            <div class="row col-xs-12 table-responsive margin-bottom-20">
                <table class="table table-striped challan-table margin-bottom-20">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Serial</th>
                        <th>Product Name</th>
                        <th>Brand</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count =1;
                        $product_qty = 0;
                        $total_price =0;
                    @endphp
                    @if(isset($quotation))

                        @foreach($quotation->transaction as $transaction)
                            @php
                                $total_price = $total_price+$transaction->product_rate*$transaction->product_qty;
                                $product_qty = $product_qty+$transaction->product_qty;
                            @endphp
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$transaction->product->serial_no}}</td>
                                <td>{{$transaction->product->name}}</td>
                                <td>{{$transaction->product->brand->name}}</td>
                                <td>{{$transaction->product_qty}}</td>
                                <td>{{$transaction->product_rate}}</td>
                                <td>{{$transaction->product_rate*$transaction->product_qty}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        @if(isset($quotation->return))
            @if($quotation->return->count()>0)
                <div class="row">

                        <h4 class="text-left text-light-blue">Return Item</h4>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Product Serial</th>
                                <th>Product Name</th>
                                <th>Brand</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $customer_get_return = 0;
                                $return_total_price =0;
                                $return_product_qty =0;
                            @endphp
                            @foreach($quotation->return as $return)
                                @php
                                    $customer_get_return+=$return->customer_get_return;
                                @endphp
                                @foreach($return->transaction as $return_transaction)
                                    @php
                                        $return_product_qty = $return_product_qty+$return_transaction->product_qty;
                                        $return_subtotal = $return_transaction->product_rate*$return_transaction->product_qty;
                                        $return_total_price +=$return_subtotal;
                                    @endphp
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$return_transaction->product->serial_no}}</td>
                                        <td>{{$return_transaction->product->name}}</td>
                                        <td>{{$return_transaction->product->brand->name}}</td>
                                        <td>{{$return_transaction->product_qty}}</td>
                                        <td>{{$return_transaction->product_rate}}</td>
                                        <td>{{$return_subtotal}}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>

                </div>
            @endif
        @endif

        <div class="row challan-sum-area margin-bottom-20">
            <div style="margin-left: 50%" class="col-md-4">
                @if(isset($return_total_price))
                    <table class="table">

                        <tr>
                            <th>Grand Total:</th>
                            <td>@if($total_price){{$total_price}}@endif</td>
                        </tr>
                        @if(isset($return_total_price))
                            {{--<tr>
                                <th>Return Product Amount:</th>
                                <td>@if($return_total_price){{$return_total_price}}@endif</td>
                            </tr>

                            <tr>
                                <th>Get Return Amount</th>
                                <td>{{$customer_get_return}}</td>
                            </tr>--}}
                            <tr>
                                <th>Totay Payable:</th>
                                <td>
                                    @php
                                        $customer_get_from_return = $return_total_price-$customer_get_return;
                                        $payable =$total_price-$customer_get_from_return;
                                    @endphp
                                    {{$payable}}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <th>Total paid:</th>
                            <td>@if($quotation){{$quotation->paid_tk}}@endif</td>
                        </tr>
                        @if($payable>$quotation->paid_tk)
                            <tr>
                                <th>Total due:</th>
                                <td>{{$payable-$quotation->paid_tk}}</td>
                            </tr>
                        @else
                            <tr>
                                <th>Customer Return:</th>
                                <td><span class="threshold">{{$quotation->paid_tk-$payable}}</span></td>
                            </tr>
                        @endif
                    </table>
                @else
                    <table class="table">
                        <tr>
                            <th>Total Item:</th>
                            <td>@if($product_qty){{$product_qty}}@endif</td>
                        </tr>
                        <tr>
                            <th>Grand Total:</th>
                            <td>@if($product_qty){{$total_price}}@endif</td>
                        </tr>

                        <tr>

                            <th>Total paid:</th>

                            <td>@if($quotation){{$quotation->paid_tk}}@endif</td>

                        </tr>

                        <tr>

                            <th>Total due:</th>

                            <td><span class="threshold">{{$total_price-$quotation->paid_tk}}</span></td>

                        </tr>

                    </table>

                @endif
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <htmlpagefooter name="page-footer">
            <img src="{{asset('pdf_img/'.$footer)}}" width="100%" alt="">

        </htmlpagefooter>
    </section>
    <!-- /.content -->
@endsection
