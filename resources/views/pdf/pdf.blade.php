<?php
/**
 * Created by PhpStorm.
 * User: Md. Arifur Rahman
 * Date: 11/10/18
 * Time: 9:38 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @php
        if(isset($sells_item)):
            $invoice = date('Ymdh',strtotime($sells_item->created_at)).$sells_item->id;
        else:
            $invoice =null;
        endif;
    @endphp
    <title>Invoice : {{$invoice}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/css/AdminLTE.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>

        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <img width="50" src="{{asset('assets/img/logo.png')}}" alt=""/></i>
                    NABI & BROTHERS
                    Nabico international

                    <small style="text-align: right;margin: 300px" class="pull-right">Date: {{date('d/m/Y')}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <table>
                <tr>
                    <td class="col-md-4">
                        From
                        <address>
                            <strong>Admin, Inc.</strong><br>
                            Nabi & Brothers 390, Dhaka Trunk Road Kadamtali,<br>
                            Chittagong-4100,bangladesh<br>
                            Phone : 031-2521487,712160<br>
                            Mobile: 01711-721065, 01815-505056<br>
                            Email: nabico@gmail.com
                        </address>
                    </td>
                    <td class="col-md-4">
                        To
                        <address>
                            <strong>@if($sells_item->customer){{$sells_item->customer->name}}@endif</strong><br>
                            @if($sells_item->customer) {{$sells_item->customer->address}} @endif
                        </address>
                    </td>
                    <td class="col-md-4">
                        <b>Invoice No : </b> @if(isset($sells_item))@if($sells_item->invoice_no) {{$sells_item->invoice_no}} @else{{date('Ymdh',strtotime($sells_item->created_at)).$sells_item->id}}@endif @endif<br>
                        <br>
                        {{--<b>Payment Due:</b> 2/22/2014<br>--}}
                        <b>Payment Date:</b> @if(isset($sells_item)){{date('d-m-Y',strtotime($sells_item->created_at))}}@endif
                    </td>
                </tr>
            </table>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <br>
        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th style="padding: 2px">SL</th>
                        <th style="padding: 2px">Product Serial</th>
                        <th style="padding: 2px">Product Name</th>
                        <th style="padding: 2px">Brand</th>
                        <th style="padding: 2px">Quantity</th>
                        <th style="padding: 2px">Price</th>
                        <th style="padding: 2px">Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count =1;
                        $product_qty = 0;
                        $total_price =0;
                    @endphp
                    @if(isset($sells_item))

                        @foreach($sells_item->transaction as $transaction)
                            @php
                                $total_price = $total_price+$transaction->product_rate*$transaction->product_qty;
                                $product_qty = $product_qty+$transaction->product_qty;
                            @endphp
                            <tr>
                                <td style="padding: 2px">{{$count++}}</td>
                                <td style="padding: 2px">{{$transaction->product->serial_no}}</td>
                                <td style="padding: 2px">{{$transaction->product->name}}</td>
                                <td style="padding: 2px">{{$transaction->product->brand->name}}</td>
                                <td style="padding: 2px">{{$transaction->product_qty}}</td>
                                <td style="padding: 2px">{{$transaction->product_rate}}</td>
                                <td style="padding: 2px">{{$transaction->product_rate*$transaction->product_qty}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="row col-md-12">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="lead">Payment Methods: @if(isset($sells_item)){{$sells_item->payment_method}}@endif </p>
                    @if(isset($sells_item->ref_note))
                        <div class="row col-md-12">
                            <span class="lead">Reference note:</span><span class="text-muted well well-sm no-shadow">{{$sells_item->ref_note}}</span>
                        </div>
                    @endif
                </div>
                <!-- /.col -->
                <div class="col-xs-4 col-xs-offset-2">
                    @php
                        $calculation_flag = true;
                    @endphp
                    <div class="table-responsive">
                        @if(isset($return_total_price))
                            <table class="table">

                                <tr>
                                    <th>Grand Total:</th>
                                    <td>@if($total_price){{$total_price}}@endif</td>
                                </tr>
                                @if(isset($return_total_price))
                                    <tr>
                                        <th>Return Product Amount:</th>
                                        <td>@if($return_total_price){{$return_total_price}}@endif</td>
                                    </tr>

                                    <tr>
                                        <th>Get Return Amount</th>
                                        <td>{{$customer_get_return}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Payable:</th>
                                        <td>
                                            @php
                                                $customer_get_from_return = $return_total_price-$customer_get_return;
                                                $payable =$total_price-$customer_get_from_return;
                                            @endphp
                                            {{$payable}}
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Total paid:</th>
                                    <td>@if($sells_item){{$sells_item->paid_tk}}@endif</td>
                                </tr>
                                @if($payable>$sells_item->paid_tk)
                                    <tr>
                                        <th>Total due:</th>
                                        <td>{{$payable-$sells_item->paid_tk}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <th>Customer Return:</th>
                                        @php
                                            if ($sells_item->paid_tk-$payable==0){
                                                $calculation_flag = false;
                                            }
                                        @endphp
                                        <td><span class="threshold">{{$sells_item->paid_tk-$payable}}</span></td>
                                    </tr>
                                @endif
                            </table>
                        @else
                            <table class="table">
                                <tr>
                                    <th>Total Item:</th>
                                    <td>@if($product_qty){{$product_qty}}@endif</td>
                                </tr>
                                <tr>
                                    <th>Grand Total:</th>
                                    <td>@if($product_qty){{$total_price}}@endif</td>
                                </tr>

                                <tr>

                                    <th>Total paid:</th>

                                    <td>@if($sells_item){{$sells_item->paid_tk}}@endif</td>

                                </tr>

                                <tr>

                                    <th>Total due:</th>
                                    @php
                                        if ($total_price-$sells_item->paid_tk==0){
                                            $calculation_flag = false;
                                        }
                                    @endphp
                                    <td><span class="threshold">{{$total_price-$sells_item->paid_tk}}</span></td>

                                </tr>

                            </table>


                        @endif
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.col -->
            </div>
        </div>
        <!-- /.row -->

        <div class="col-md-12">
            <!-- accepted payments column -->
            <div class="text-left">Buyer's signature</div>
        </div>
        <htmlpagefooter name="page-footer">
        <div class="col-md-12 footer-print">
            <table class="table">
                <tr>
                    <th class="font-10">CTG Head Office</th>
                    <td>:</td>
                    <td class="font-10">390, Dhaka Trunk Road (Dastagir Super Marker), Kadamtali, Chittagong,
                        Bangladesh
                    </td>
                </tr>

                <tr>
                    <th class="font-10">CTG Branch</th>
                    <td>:</td>
                    <td class="font-10">699, SK. Mujib Road (1st Floor), Chittagong. Phone : 031-718863</td>
                </tr>

                <tr>
                    <th class="font-10">Dhaka Head Office</th>
                    <td>:</td>
                    <td class="font-10">City Machinery Market 96 / 97 Nobabpur Road, Dhaka. Phone : 025716473,
                        025716474
                    </td>
                </tr>
                <tr>
                    <th class="font-10">Mobile no
                    </th>
                    <td>:</td>
                    <td class="font-10"> 01817204538, 01674550978</td>
                </tr>
                <tr>
                    <th class="font-10">WEBSITE</th>
                    <td>:</td>
                    <td class="font-10">www.nabicointernational.com</td>
                </tr>
            </table>
        </div>
        </htmlpagefooter>
    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>


