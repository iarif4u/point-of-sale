<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('print.layout.master')

@section('title',"DifferentCoder || Quotation ")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/print.css')}}">
    <style>
        @media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
        .calculation,td{
            padding: 5px !important;
        }
        .title{
            font-size: 26px;
            padding: 5px !important;
        }
        .challan-title{
            font-size: 16px !important;
        }
        .font-10{
            font-size: 10px;
        }
        .margin-top-20{
            margin-top: 20px !important;
        }
        .margin-top-inverse-20{
            margin-top: -20px !important;
        }
        .padding-top-inverse-50{
            padding-top: -50px !important;
        }
        .padding-top-inverse-90{
            padding-top: -90px !important;
        }
        .padding-left-150{
            padding-left: 50px !important;
        }
        .padding-left-400{
            padding-left: 105px !important;
        }
        .padding-left-80{
            padding-left: 70px !important;
        }
        .margin-bottom-20{
            margin-top: 20px !important;
        }
        .margin-left-20{
            margin-left: 20px !important;
        }
        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <section class="invoice">
            <div class="row margin-bottom-20">
                <div class="col-xs-12 text-center challan-button">
                    <div style="font-size: 18px" class="title">Quotation</div>
                </div>
            </div>
        <!-- title row -->
        <div class="row">
            <table class="table">
                <tr>
                    <td class="col-xs-1">
                        <div >
                            <img width="50" style="margin-top: -50px" src="{{asset('assets/img/logo.png')}}" alt="" />
                        </div>
                    </td>
                    <td class="col-xs-6 challan-title">
                        <div>
                            <h3>NABI & BROTHERS</h3>
                            <h3>Nabico international</h3>
                        </div>
                    </td>
                    <td class="col-xs-7 challan-eminfo margin-left-20">

                        <table class="table margin-left-20">
                            <tr>
                                <th class="margin-left-20">Phone</th>
                                <td>:</td>
                                <td style="font-size: 10px">031-2521487, 031-712160, 031-723543</td>
                            </tr>
                            <tr>
                                <th class="margin-left-20">fax</th>
                                <td>:</td>
                                <td style="font-size: 10px">88-031-723921</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>:</td>
                                <td style="font-size: 10px">01711-721065, 01711-748508, 01815-505056, 01817-204538</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <td>:</td>
                                <td style="font-size: 10px">nabico@gmail.com, upal4444@gmail.com</td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </div>
        <div class="row margin-bottom-20">
            <div style="font-size:10px; background: #ddd; text-align: center; padding: 15px;">
                {!! strtoupper('specially deals : spare parts of marine,machineres,auto,heavy vehicle,gas & diesel generator and other accessories.
                <b>importer & stockist</b>') !!}
            </div>
        </div>
        <div class="row margin-top-20">

            <table>
                <tr>
                    <td>
                        From
                        <address>
                            <strong>Admin, Inc.</strong><br>
                            795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>
                            Phone: (804) 123-5432<br>
                            Email: info@almasaeedstudio.com
                        </address>
                    </td>
                    <td class="padding-top-inverse-50 padding-left-80">
                        To
                        <address>
                            <strong>@if($quotation->customer){{$quotation->customer->name}}@endif</strong><br>
                            @if($quotation->customer) {{$quotation->customer->address}} @endif
                        </address>
                    </td>
                    <td class="padding-top-inverse-90 padding-left-150">
                        Quotation No :
                        @if(isset($quotation)){{$quotation->id}}@endif<br>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row margin-bottom-20 margin-top-20">
            <div class="row col-xs-12 table-responsive margin-bottom-20 margin-top-20">
                <table class="table table-striped challan-table margin-bottom-20 margin-top-20">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Serial</th>
                        <th>Product Name</th>
                        <th>Brand</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count =1;
                        $product_qty = 0;
                        $total_price =0;
                    @endphp
                    @if(isset($quotation))

                        @foreach($quotation->transaction as $transaction)
                            @php
                                $total_price = $total_price+$transaction->product_rate*$transaction->product_qty;
                                $product_qty = $product_qty+$transaction->product_qty;
                            @endphp
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$transaction->product->serial_no}}</td>
                                <td>{{$transaction->product->name}}</td>
                                <td>{{$transaction->product->brand->name}}</td>
                                <td>{{$transaction->product_qty}}</td>
                                <td>{{$transaction->product_rate}}</td>
                                <td>{{$transaction->product_rate*$transaction->product_qty}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row challan-sum-area margin-bottom-20 margin-top-20">
            <table class="table">
                <tr>
                    <td style="width: 200px" class="lead"> Reference note:</td>
                    <td style="padding-left:-20px; width: 300px" style="padding-left:-20px;" class="text-muted">
                        {{$quotation->ref_note}}
                    </td>
                    <td>
                        <table class="table ">
                            <tr>
                                <th class="padding-left-150">Total Item:</th>
                                <td>{{$product_qty}}</td>
                            </tr>
                            <tr>
                                <th class="padding-left-150">Grand Total:</th>
                                <td>{{$total_price}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row challan-signture-area">
            <!-- accepted payments column -->

            <table>
                <tr class="col-md-12">
                    <td style="width:500px" class="text-left col-md-3">Buyer's signature</td>
                    <td style="padding-left: 160px" class="text-right col-md-offset-6 col-md-3">For: Nabi & brothers nabico international</td>
                </tr>
            </table>
        </div>
            <table class="table">
                <tr>
                    <th  class="font-10">CTG  Head Office</th>
                    <td>:</td>
                    <td class="font-10">390, Dhaka Trunk Road (Dastagir Super Marker), Kadamtali, Chittagong, Bangladesh                             </td>
                </tr>

                <tr>
                    <th class="font-10">CTG  Branch           </th>
                    <td>:</td>
                    <td class="font-10">699, SK.  Mujib Road (1st Floor), Chittagong. Phone : 031-718863</td>
                </tr>

                <tr>
                    <th  class="font-10">Dhaka Head Office </th>
                    <td>:</td>
                    <td class="font-10">City Machinery Market  96 / 97  Nobabpur  Road, Dhaka. Phone : 025716473, 025716474                                        </td>
                </tr>
                <tr>
                    <th  class="font-10">Mobile no
                    </th>
                    <td>:</td>
                    <td class="font-10"> 01817204538, 01674550978</td>
                </tr>
                <tr>
                    <th  class="font-10">WEBSITE</th>
                    <td>:</td>
                    <td class="font-10">www.nabicointernational.com</td>
                </tr>
            </table>

    </section>
    <!-- /.content -->
@endsection
