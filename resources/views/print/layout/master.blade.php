<!DOCTYPE html>
<html>
<head>
    @include('admin.include.header')
    <title> @yield('title') </title>
    @yield('style')
</head>
<body onload="window.print();">
<!-- Main content -->
<section class="content container-fluid">
    <!-- Small boxes (Stat box) -->
    @yield('content')
</section>
<!-- /.content -->
<!-- ./wrapper -->
{{--link the js plugin--}}
@yield('script')
</body>
</html>