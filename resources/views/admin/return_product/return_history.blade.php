<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Item Return History")

@section('header_left')
    <h1>
        Item Sale History
        <small>Control panel</small>
    </h1>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>

    <li class="active">   All Return Item History Information List Here</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a href="{{route('admin.pos.get_sold_view')}}" class="btn btn-success"> <i class="fa fa-th-list" aria-hidden="true"></i>Sale Item List </a>  </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->

                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Sale ID</th>
                            <th>Sale Date</th>
                            <th>Return Date</th>
                            <th>Customer Name </th>
                            <th>Customer Return Amount</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($returnList)
                            @foreach($returnList as $return)
                            <tr>
                                <td>{{$return->sell->id}}</td>
                                <td>{{date('d-m-Y',strtotime($return->sell->created_at))}}</td>
                                <td>{{date('d-m-Y',strtotime($return->created_at))}}</td>
                                <td>{{$return->customer->name}}</td>
                                <td>{{$return->customer_get_return}}</td>

                                <td>
                                    <a href="{{route('admin.return.get_return_details',['return_id'=>$return->id])}}" class="btn-view" title="Product Retand"> <i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Sale ID</th>
                            <th>Sale Date</th>
                            <th>Return Date</th>
                            <th>Customer Name</th>
                            <th>Customer Return Amount</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>
    </div>


@endsection

