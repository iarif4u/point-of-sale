<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Item Sale History")

@section('header_left')
    <h1>
        Item Sale History
        <small>Control panel</small>
    </h1>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">  Item Sale History</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="retandhistory.php" class="btn btn-success">
                    <i class="fa fa-th-list" aria-hidden="true"></i>
                    Return History
                </a>
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->

                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Sale ID</th>
                            <th>Sale Date</th>
                            <th>Customer Name </th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>NBL-1223</td>
                            <td>12-02-2018</td>
                            <td> Minhaj</td>
                            <td>Data Input and output Soft </td>
                            <td>$ 1200 </td>
                            <td>1</td>
                            <td>
                                <a href="#" class="btn-edit" title="Product Retand"> <i class="fa  fa-exchange" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Sale ID</th>
                            <th>Sale Date</th>
                            <th>Customer Name </th>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
            <div class="box-footer">
                Visit <a href="#"> documentation</a> for more examples and information about the plugin.
            </div>
        </div>

        <div class="modal fade" id="modal-salesorder">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Sales Oder </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- form start -->
                            <form role="form">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ProductCategory">Staff Name</label>
                                            <select class="form-control select2" style="width: 100%;">
                                                <option selected="selected">Marketing</option>
                                                <option>Designer</option>
                                                <option>Developer</option>
                                                <option>Delaware</option>
                                                <option>Tennessee</option>
                                                <option>Texas</option>
                                                <option>Washington</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="ProductCategory">Client Name</label>
                                            <select class="form-control select2" style="width: 100%;">
                                                <option selected="selected">Marketing</option>
                                                <option>Designer</option>
                                                <option>Developer</option>
                                                <option>Delaware</option>
                                                <option>Tennessee</option>
                                                <option>Texas</option>
                                                <option>Washington</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="OderStartDate">Oder Start Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ExpireDate">Expire Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker1">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="CPemailAddress">Contact Person Email Address</label>
                                            <input type="text" class="form-control" id="CPemailAddress" placeholder="Enter The Email Address">
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ProductCategory">Payment Term</label>
                                            <select class="form-control select2" style="width: 100%;">
                                                <option selected="selected">Marketing</option>
                                                <option>Designer</option>
                                                <option>Developer</option>
                                                <option>Delaware</option>
                                                <option>Tennessee</option>
                                                <option>Texas</option>
                                                <option>Washington</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="sProductName">Product Name</label>
                                            <input type="text" class="form-control" id="sProductName" placeholder="Enter The Product Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="sProductPrice">Product Price</label>
                                            <input type="text" class="form-control" id="sProductPrice" placeholder="Enter The Price">
                                        </div>
                                        <div class="form-group">
                                            <label for="sProductVat">Product Vat </label>
                                            <input type="text" class="form-control" id="sProductVat" placeholder="Enter The Vat">
                                        </div>
                                        <div class="form-group">
                                            <label for="sProductDiscount">Product Discount</label>
                                            <input type="text" class="form-control" id="sProductDiscount" placeholder="Enter The Discount">
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="TermsandCondition">Terms and Condition</label>
                                            <textarea class="form-control" rows="1" placeholder="Enter The Full Terms and Condition"></textarea>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                            </form>
                            <!-- / form -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


    </div>

@endsection

