<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Item Details")

@section('header_left')
    <h1>Item List<small>Control panel</small></h1>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Item List</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-additemarea"> <i class="fa fa-th-list" aria-hidden="true"></i> New Item </a>  </h3>
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-newcategorycsv"> <i class="fa fa-cloud-upload" aria-hidden="true"></i> New Item CSV File unloader </a>  </h3>
            <h3 class="box-title"><a href="{{route('admin.inventory.excel')}}" class="btn btn-success"> <i class="fa fa-cloud-download"></i> Download CSV File </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="index_product" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width:100px">Serial</th>
                            <th style="width:100px">Name</th>
                            <th>Category</th>
                            <th style="width:90px">Sub-category</th>
                            <th>Brand</th>
                            <th>Tk</th>
                            <th>Dollar</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Serial</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Brand</th>
                            <th>Tk</th>
                            <th>Dollar</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->

        </div>
        <!-- start Add Item Area-->
        <div class="modal fade" id="modal-additemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Item </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.inventory.get_item_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('serial_no', 'Item Serial No') !!}
                                        {!! Form::text('serial_no', null,['placeholder'=>"Enter The Item Serial No",'id'=>'serial_no','class'=>'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('itemCategory', 'Item Category') !!}
                                        {!! Form::select('itemCategory',$category_list, null,
                                            ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'itemCategory']) !!}

                                    </div>


                                    <div class="form-group">
                                        {!! Form::label('itemRate', 'Item Rate') !!}
                                        {!! Form::text('itemRate', null,['placeholder'=>"Enter The Item Rate",'class'=>'form-control importItemRate']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('brandName', 'Item Brand') !!}
                                        {!! Form::select('brandName',$brand_list, null,
                                            ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'brandName']) !!}

                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('itemName', 'Item Name') !!}
                                        {!! Form::text('itemName', null,['placeholder'=>"Enter The Item Name",'id'=>'itemName','class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemSubCategory', 'Item Sub-Category') !!}
                                        <div class="change_sub_cate">
                                            {!! Form::select('itemSubCategory',['0'=>"Select One"], null,
                                             ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'itemSubCategory']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemDollarRate', 'Item Dollar Rate') !!}
                                        {!! Form::text('itemDollarRate', null,['placeholder'=>"Enter The Item Dollar Rate",'class'=>'form-control editItemDollarRate']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('productDetails', 'Product Details') !!}
                                        {!! Form::textarea('productDetails', null,['placeholder'=>"Enter The Full Product Details",'id'=>'productDetails','class'=>'form-control',"rows"=>"1"]) !!}

                                    </div>

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
                <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Add Item Area-->

        <!-- Start CSV File Section -->
        <div class="modal fade" id="modal-newcategorycsv">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Product CSV File </h4>
                    </div>
                    {!! Form::open(['route' => 'admin.excel.product_sheet','autocomplete'=>'off','files' => true]) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            {!! Form::label('file', 'Upload CSV File For Product List') !!}
                            {!! Form::file('file', null,['id'=>'categoryName','class'=>'form-control']) !!}
                            <p class="help-block">Browsers your Product List .CSV File </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    <!-- /.form-group -->
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End CSV File Section -->

        <!-- start View Item Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Item </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- box boyd start -->
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="SerialNumber">Serial Number</label>
                                        <h5 class="viewSerialNumber item_display"></h5>

                                    </div>
                                    <div class="form-group">
                                        <label for="ItemCategory">Item Category</label>
                                        <h5 class="viewItemCategory item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="ItemRate">Item Rate</label>
                                        <h5 class="viewItemRate item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="Brand Name">Brand Name</label>
                                        <h5 class="viewItemBrand  item_display"></h5>
                                    </div>


                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="ItemName">Item Name</label>
                                        <h5 class="viewItemName item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="Item Sub-Category">Item Sub-Category</label>
                                        <h5 class="viewItemSubCate item_display"></h5>
                                    </div>

                                    <div class="form-group">
                                        <label for="ItemRate">Item Dollar Rate</label>
                                        <h5 class="viewItemDollar item_display"></h5>
                                    </div>

                                    <div class="form-group">
                                        <label for="ProductDetails">Product Details </label>
                                        <h5 class="viewItemDetails item_display"></h5>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Item Area-->

        <!-- start Edit Item Area-->
        <div class="modal fade" id="modal-edititemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Item </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.inventory.update_item','autocomplete'=>'off']) !!}
                    {{ Form::hidden('item_id', 0, array('id' => 'edit_item_id')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('serial_no', 'Item Serial No') !!}
                                        {!! Form::text('serial_no', null,['placeholder'=>"Enter The Item Serial No",'id'=>'edit_serial_no','class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemCategory', 'Item Category') !!}
                                        {!! Form::select('itemCategory',$category_list, null,
                                            ['style'=>"width: 100%;",'class' => 'viewItemCategory form-control select2','id' => 'viewItemCategory']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemRate', 'Item Rate') !!}
                                        {!! Form::text('itemRate', null,['placeholder'=>"Enter The Item Rate",'id'=>'viewItemRate','class'=>'form-control importItemRate']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemCurrency', 'Item Currency') !!}
                                        {!! Form::text('itemCurrency', null,['placeholder'=>"Enter The Item Rate",'id'=>'viewItemCurrency','class'=>'form-control importItemRate']) !!}
                                    </div>


                                    <div class="form-group">
                                        {!! Form::label('brandName', 'Item Brand') !!}
                                        {!! Form::select('brandName',$brand_list, null,
                                            ['style'=>"width: 100%;",'class' => 'viewItemBrand form-control select2','id' => 'viewItemBrand']) !!}

                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        {!! Form::label('itemName', 'Item Name') !!}
                                        {!! Form::text('itemName', null,['placeholder'=>"Enter The Item Name",'id'=>'viewItemName','class'=>'form-control viewItemName']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemSubCategory', 'Item Sub-Category') !!}
                                        <div class="change_sub_cate">
                                            {!! Form::select('itemSubCategory',['0'=>"Select One"], null,
                                             ['style'=>"width: 100%;",'class' => 'viewItemSubCate form-control select2','id' => 'viewItemSubCate']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemDollarRate', 'Item Dollar Rate') !!}
                                        {!! Form::text('itemDollarRate', null,['placeholder'=>"Enter The Item Dollar Rate",'id'=>'viewItemDollar','class'=>'form-control editItemDollarRate']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('productLastImport', 'Last Import') !!}
                                        {!! Form::text('productLastImport', null,['id'=>'productLastImport','class'=>'form-control productLastImport']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('productDetails', 'Product Details') !!}
                                        {!! Form::textarea('productDetails', null,['placeholder'=>"Enter The Full Product Details",'id'=>'viewItemDetails','class'=>'form-control viewItemDetails',"rows"=>"1"]) !!}
                                    </div>


                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
                <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Edit Item Area-->

        <!-- Start import Item Area-->
        <div class="modal fade" id="modal-import">
            <div style="margin-top: 5px;" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Item Import Now</h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.transaction.import_item','autocomplete'=>'off']) !!}
                    {{ Form::hidden('item_id', 0, array('id' => 'import_item_id')) }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('itemSerial', 'Item Serial') !!}
                                        {!! Form::text('itemSerial', null,['placeholder'=>"Enter The Item Serial",'id'=>'importItemSerial','disabled'=>'disabled','class'=>'form-control importItemSerial']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemCategory', 'Item Category') !!}
                                        {!! Form::select('itemCategory',$category_list, null,
                                            ['style'=>"width: 100%;",'disabled'=>'disabled','class' => 'viewItemCategory form-control select2','id' => 'importItemCategory']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemVendor', 'Item Vendor Company') !!}
                                        {!! Form::select('itemVendor',$vendor_list, null,
                                            ['style'=>"width: 100%;",'class' => 'viewItemVendor form-control select2','id' => 'importItemVendor']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemRate', 'Item Rate') !!}
                                        {!! Form::text('itemRate', null,['placeholder'=>"Enter The Item Rate",'id'=>'viewItemRate','class'=>'form-control importItemRate']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemQty', 'Item Quantity') !!}
                                        {!! Form::text('itemQty', null,['placeholder'=>"Enter The Item Quantity",'id'=>'importItemQuantity','class'=>'form-control importItemQuantity']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('specialNote', 'Special Note') !!}
                                        {!! Form::textarea('specialNote', null,['placeholder'=>"Enter The Special Note",'id'=>'importSpecialNote','class'=>'form-control viewSpecialNote',"rows"=>"1"]) !!}
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        {!! Form::label('itemName', 'Item Name') !!}
                                        {!! Form::text('itemName', null,['placeholder'=>"Enter The Item Name",'disabled'=>'disabled','id'=>'importItemName','class'=>'form-control viewItemName']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('itemSubCategory', 'Item Sub-Category') !!}

                                        {!! Form::select('itemSubCategory',$subcateList, null,
                                         ['disabled'=>'disabled','style'=>"width: 100%;",'class' => 'viewItemSubCate form-control select2','id' => 'importItemSubCate']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemWirehouse', 'Item Warehouse') !!}
                                        {!! Form::select('itemWirehouse',$warehouse_list, null,
                                            ['style'=>"width: 100%;",'class' => 'viewItemWarehouse form-control select2','id' => 'importItemWarehouse']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('itemDollarRate', 'Item Dollar Rate') !!}
                                        {!! Form::text('itemDollarRate', null,['placeholder'=>"Enter Item Dollar Rate",'id'=>'importItemDollar','class'=>'form-control viewItemDollar']) !!}
                                    </div>

                                    <div class="form-group">
                                       {{-- {!! Form::label('brandName', 'Item Brand') !!}
                                        {!! Form::select('brandName',$brand_list, null,
                                            ['disabled'=>'disabled','style'=>"width: 100%;",'class' => 'viewItemBrand form-control select2','id' => 'importItemBrand']) !!}--}}
                                        {!! Form::label('currency', 'Currency')  !!}
                                        {!! Form::text('currency', null,['placeholder'=>"Enter The Currency",'id'=>'currency','class'=>'form-control viewItemcurrency']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('productDetails', 'Product Details') !!}
                                        {!! Form::textarea('productDetails', null,['placeholder'=>"Enter The Full Product Details",'id'=>'importItemDetails','class'=>'form-control viewItemDetails','disabled'=>'disabled',"rows"=>"1"]) !!}
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
                <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End import Item Area-->


        <!-- Start Delete Item Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Item Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this item?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Item Area-->

        <!-- Start History Item Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Item History Here</h4>
                    </div>
                    <div class="modal-body">

                        <!-- Start History Table -->
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p class="item-info">Item Name : Lub Filter LF-670 | US$- 120 | Tk - 1200 | List Update : 10-10-2018</p>
                                <div class="transaction_list">

                                </div>
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Note</th>
                                        <th>Rate</th>
                                        <th>Customer</th>
                                        <th>Vendor</th>
                                        <th>Sale</th>
                                        <th>Import</th>
                                        <th>Currency</th>
                                        <th>Stock</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End History Item Area-->
    </div>



@endsection
@section('script')
    <script type="text/javascript">
        var sub_cate_select_id = 0;
        var delete_id = 0;

        //get details for product import by vendor
        function get_item_import(item_id) {

            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_item_details')}}',
                data: {'_token':'{{csrf_token()}}','item_id':item_id},
                success: function(result){
                    if (result.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<result.message.length;i++)
                        {
                            row += '<span class="each-error">'+result.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        // $("#viewItemCategory").val(result.message.category.name);

                        $("#import_item_id").val(result.message.id);
                        $("#importItemSerial").val(result.message.serial_no);
                        $("#importItemName").val(result.message.name);


                        $("#importItemDetails").val(result.message.product_details);

                        $('#importItemCategory').val(result.message.category.id).trigger('change');
                        $('#importItemBrand').val(result.message.brand.id).trigger('change');
                        $('#importItemSubCate').val(result.message.sub_category.id).trigger('change');


                    }
                    $(".status").html(row);
                    sub_cate_select_id = result.message.sub_category.id
                    $('#viewItemSubCate').val(result.message.sub_category.id).trigger('change');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Data Parse Error</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }

            });
        }

        //call when click delete button
        function get_item_delete(item_id){
            delete_id = item_id;
        }
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.delete_item')}}',
                data: {'_token':'{{csrf_token()}}','item_id':delete_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Item delete successfully done</div>';
                        $(".item_id_"+delete_id).hide(2000);
                        $(".alert").hide();
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Item delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
        /*get subcategory by category id*/
        $('#itemCategory').change(function () {
            var category_id = this.value;
            $('#itemSubCategory option').each(function () {
                this.remove();
            });

            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_sub_category')}}',
                data: {'_token':'{{csrf_token()}}','category_id':category_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        for (var i=0; i<data.html.length; i++){

                            var newOption = new Option(data.html[i].text,data.html[i].id, false, false);
                            $('#itemSubCategory').append(newOption).trigger('change');
                        }

                        /**/


                        // $(".change_sub_cate").html(data.html);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Subcategory Parse Error</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
        /*get item details view*/
        function get_item_view(item_id) {

            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_item_details')}}',
                data: {'_token':'{{csrf_token()}}','item_id':item_id},
                success: function(result){
                    if (result.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<result.message.length;i++)
                        {
                            row += '<span class="each-error">'+result.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        $("h5.viewSerialNumber").text(result.message.serial_no);
                        $("h5.viewItemCategory").text(result.message.category.name);
                        $("h5.viewItemRate").text(result.message.item_tk_rate);
                        $("h5.viewItemBrand").text(result.message.brand.name);
                        $("h5.viewItemName").text(result.message.name);
                        $("h5.viewItemSubCate").text(result.message.sub_category.name);
                        $("h5.viewItemDollar").text(result.message.item_dollar_rate);
                        $("h5.viewItemDetails").text(result.message.product_details);

                        // $("#viewItemCategory").val(result.message.category.name);
                        $("#edit_serial_no").val(result.message.serial_no);
                        $("#viewItemRate").val(result.message.item_tk_rate);
                        $("#edit_item_id").val(item_id);
                        $("#viewItemName").val(result.message.name);
                        $("#viewItemCurrency").val(result.transaction.currency);
                        $("#productLastImport").val(result.transaction.last_import);
                        //  $("#viewItemSubCate").val(result.message.sub_category.name);
                        $("#viewItemDollar").val(result.message.item_dollar_rate);
                        $("#viewItemDetails").val(result.message.product_details);

                        $('#viewItemCategory').val(result.message.category.id).trigger('change');
                        $('#viewItemBrand').val(result.message.brand.id).trigger('change');

                        $('#viewItemSubCate option').each(function () {
                            this.remove();
                        });

                        for (var i=0; i<result.message.category.sub_category.length; i++){

                            // Set the value, creating a new option if necessary
                            if ($('#viewItemSubCate').find("option[value='" + result.message.category.sub_category[i].id + "']").length) {
                                $('#viewItemSubCate').val(result.message.category.sub_category[i].id).trigger('change');
                            } else {
                                // Create a DOM Option and pre-select by default
                                var newOption = new Option(result.message.category.sub_category[i].name, result.message.category.sub_category[i].id, true, true);
                                // Append it to the select
                                $('#viewItemSubCate').append(newOption).trigger('change');
                            }
                        }
                    }
                    $(".status").html(row);
                    sub_cate_select_id = result.message.sub_category.id
                    $('#viewItemSubCate').val(result.message.sub_category.id).trigger('change');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Data Parse Error</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }

            });
        }
        /*get subcategory by category id on product update*/
        $('select.viewItemCategory').change(function () {
            var category_id = this.value;
            $('#viewItemSubCate option').each(function () {
                this.remove();
            });
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_sub_category')}}',
                data: {'_token':'{{csrf_token()}}','category_id':category_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        for (var i=0; i<data.html.length; i++){
                            // Set the value, creating a new option if necessary
                            if ($('#viewItemSubCate').find("option[value='" + data.html[i].id + "']").length) {
                                $('#viewItemSubCate').val(data.html[i].id).trigger('change');
                            } else {
                                // Create a DOM Option and pre-select by default
                                var newOption = new Option(data.html[i].text, data.html[i].id, false, false);
                                // Append it to the select
                                $('#viewItemSubCate').append(newOption).trigger('change');
                            }
                        }
                    }
                    $('#viewItemSubCate').val(sub_cate_select_id).trigger('change');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Subcategory Parse Error</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });


    </script>
    <script>
        $("#index_product").DataTable({
            "bDestroy": true,
            'processing' : true,
            'ajax':{
                url: '{{route('admin.inventory.get_product_streaming')}}',
                data: {'_token':'{{csrf_token()}}'},
                type: "POST",
            },

        });
        //get the transaction details by proouct id
        function get_item_transaction(product_id) {
            set_item_info(product_id);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.transaction.get_product_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','product_id':product_id},
                    type: "POST",
                },

            });

        }
        //set product info data to top transaction
        function set_item_info(product_id) {
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_item_info')}}',
                data: {'_token':'{{csrf_token()}}','item_id':product_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                        $(".status").html(row);
                    }else{
                        var fromDate = new Date(data.message.updated_at);
                        var date = new Date(fromDate).toDateString();
                        var info_item = "Item Name : "+data.message.name+" | US$- "+data.message.item_dollar_rate+" | Tk - "+data.message.item_tk_rate+" | Last Update : "+date+" | Stock : "+data.message.stock;
                        $('.item-info').text(info_item);

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Item details parse failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        }
    </script>
@endsection
