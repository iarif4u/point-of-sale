<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Item Category")

@section('header_left')
        Item Category List
        <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Item Category List</li>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-newcategory"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Category </a>  </h3>
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-newcategorycsv"> <i class="fa fa-cloud-upload" aria-hidden="true"></i> New Category CSV File uploader </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->

                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Category ID</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($category_list->count()>0)
                            @foreach($category_list as $category)
                                <tr class="category_{{$category->id}}">
                                    <td class="categoryIdView">{{$category->id}}</td>
                                    <td class="categoryNameView">{{$category->name}}</td>
                                    <td>
                                        <a href="#" class="btn-edit" title="Edit Information"  data-toggle="modal" data-target="#modal-newcategoryedit" > <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="#" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-newcategorydetect"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>New Category Name</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>
        <!--  Start New Category Name Area -->
        <div class="modal fade" id="modal-newcategory">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Category Name Add</h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.inventory.get_category_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('categoryName', 'Category Name') !!}
                                            {!! Form::text('categoryName', null,['placeholder'=>"Enter The Category Name",'id'=>'categoryName','class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--  End New Category Name Area -->
        <!--  Start New Category CSV File Uplode Area -->
        <div class="modal fade" id="modal-newcategorycsv">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Category File</h4>
                    </div>
                    {!! Form::open(['route' => 'admin.excel.category_sheet','autocomplete'=>'off','files' => true]) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">

                                    <div class="form-group">
                                        {!! Form::label('file', 'Upload CSV File For Category Name') !!}
                                        {!! Form::file('file', null,['placeholder'=>"Enter The Category Name",'id'=>'categoryName','class'=>'form-control']) !!}
                                        <p class="help-block">Browsers your Category List .CSV File </p>
                                    </div>

                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>.CSV File Formate Downlode Here</label>
                                        <a href="{{route('admin.excel.category_sheet')}}" class="btn btn-success"> <i class="fa fa-cloud-download" aria-hidden="true"></i> Download CSV File </a>
                                        <p class="help-block">CSV File Formate </p>
                                    </div>
                                    <!-- /.form-group -->
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    <!-- /.form-group -->
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--  End New Category CSV File Uplode Area -->
        <!--  Start New Category Name Edit Area -->
        <div class="modal fade" id="modal-newcategoryedit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Category Name Edit</h4>
                    </div>
                    {!! Form::open(['route' => 'admin.inventory.update_category','autocomplete'=>'off']) !!}
                    {{ Form::hidden('category_id', 0, array('id' => 'edit_category_id')) }}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('categoryName', 'Category Name Edit') !!}
                                            {!! Form::text('categoryName', null,['placeholder'=>"Category Name Edit",'id'=>'editCategoryName','class'=>'form-control']) !!}
                                            </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--  End New Category Name Area -->

        <!-- Start Delete Item Area-->
        <div class="modal modal-danger fade" id="modal-newcategorydetect">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Category Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Category?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Item Area-->
    </div>
@endsection


@section('script')
    <script type="text/javascript">
        var category_id = "";
        var name = "";
        /*get customer data*/
        function get_category_details(current){
            category_id = $(current).parent().parent().find(".categoryIdView").text();
            name = $(current).parent().parent().find(".categoryNameView").text();
        }
        /*call when category update/edit*/
        $('.btn-edit').click(function(){
            get_category_details(this);
            $("#edit_category_id").val(category_id);
            $("#editCategoryName").val(name);
        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_category_details(this);
        });
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.delete_category')}}',
                data: {'_token':'{{csrf_token()}}','category_id':category_id},
                success: function(data){

                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Category delete successfully done</div>';
                        $('.category_'+category_id).hide('slow');
                        $(".alert").hide();
                    }
                    $('#modal-newcategorydetect').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Category delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-newcategorydetect').modal('hide');
                    $(".status").html(row);
                }
            });
        });
    </script>
@endsection