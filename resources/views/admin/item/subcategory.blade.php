<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Item Category")

@section('header_left')
    <h1>
        Item Sub Category List
        <small>Control panel</small>
    </h1>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{route('admin.inventory.get_category_list')}}"> Item Category List</a></li>
    <li class="active"> Item Sub Category List</li>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-newcategory"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Sub Category </a>  </h3>
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-newcategorycsv"> <i class="fa fa-cloud-upload" aria-hidden="true"></i> New Sub Category CSV File uploader </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->

                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Sub Category Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($subcateList->count()>0)
                            @foreach($subcateList as $subcate)
                                @if($subcate->category)
                                    <tr class="sub_category_{{$subcate->id}}">
                                        <td class="category_name">{{$subcate->category->name}}</td>
                                        <td class="sub_category_name">{{$subcate->name}}</td>
                                        <td>
                                            <a href="#" onclick="edit_subcategory(this,{{$subcate->id}})" class="btn-edit" title="Edit Information"  data-toggle="modal" data-target="#modal-editsubcategory" > <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a href="#" onclick="delete_subcategory({{$subcate->id}})" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-subcategorydetect"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>New Category Name</th>
                            <th>Sub Category Name</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>
        <!-- Start New Category List Area -->
        <div class="modal fade" id="modal-newcategory">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add New Sub Category</h4>
                    </div>
                    {!! Form::open(['route' => 'admin.inventory.get_sub_category_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('category_id', 'Main Category') !!}
                                            {!! Form::select('category_id',$cateList, null,
                                            ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'itemCategory']) !!}

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('subCategoryName', 'Sub Category Name') !!}
                                            {!! Form::text('subCategoryName', null,['placeholder'=>"Category Name",'id'=>'subCategoryName','class'=>'form-control']) !!}

                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End New Category List Area -->
        <!-- Start CSV Category List Area -->
        <div class="modal fade" id="modal-newcategorycsv">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Category File</h4>
                    </div>
                    {!! Form::open(['route' => 'admin.excel.sub_category_sheet','autocomplete'=>'off','files' => true]) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('file', 'Upload CSV File For Sub-Category Name') !!}
                                    {!! Form::file('file', null,['placeholder'=>"Enter The Sub-Category Name",'id'=>'sub_categoryName','class'=>'form-control']) !!}
                                    <p class="help-block">Browsers your Category List .CSV File </p>
                                </div>
                                    <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>.CSV File Formate Downlode Here</label>
                                    <a href="{{route('admin.excel.sub_category_sheet')}}" class="btn btn-success"> <i class="fa fa-cloud-download" aria-hidden="true"></i> Download CSV File </a>
                                    <p class="help-block">CSV File Formate </p>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <div class="modal-footer">
                            {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    <!-- /.form-group -->
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End CSV Category List Area -->
        <!-- Start Sub Category List Edit Area -->
        <div class="modal fade" id="modal-editsubcategory">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Sub Category Edit</h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.inventory.update_sub_category','autocomplete'=>'off']) !!}
                    {{ Form::hidden('subCategory_id', 0, array('id' => 'edit_subCategory_id')) }}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="ItemCategory">Main Category</label>
                                            {!! Form::select('category_id',$cateList, null,
                                            ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'editItemCategory']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="SubCategoryName">Sub Category Name</label>
                                            {!! Form::text('subCategoryName', null,['placeholder'=>"Sub-Category Name Edit",'id'=>'editSubCategoryName','class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End New Category List Area -->

        <!-- Start Delete Sub Category Area-->
        <div class="modal modal-danger fade" id="modal-subcategorydetect">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Sub Category Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Sub Category?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Sub Category Area-->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var category_name = "";
        var sub_category_name = "";
        var category_id = "";
        var sub_category_id = "";
        /*get customer data*/
        function get_category_details(current){
            category_name = $(current).parent().parent().find(".category_name").text();
            sub_category_name = $(current).parent().parent().find(".sub_category_name").text();
            category_id = $("#editItemCategory option").filter(function() {
                return $(this).html() == category_name;
            }).val();
        }
        /*call when category update/edit*/
        function edit_subcategory(context,sub_cate_id) {
            get_category_details(context);
            $('#editItemCategory').val(category_id).trigger('change');
            $("#edit_subCategory_id").val(sub_cate_id);
            $("#editSubCategoryName").val(sub_category_name);
        }
        //call when click delete button
        function delete_subcategory(subcate_id) {
            sub_category_id = subcate_id;
        }
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.delete_category')}}',
                data: {'_token':'{{csrf_token()}}','category_id':sub_category_id},
                success: function(data){

                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Sub-Category delete successfully done</div>';
                        $('.sub_category_'+sub_category_id).hide('slow');
                        $(".alert").hide();
                    }
                    $('#modal-subcategorydetect').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Sub-Category delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-subcategorydetect').modal('hide');
                    $(".status").html(row);
                }
            });
        });
    </script>
@endsection