<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Customer Registration")

@section('header_left')
    Brand List<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Brand List</li>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-addBrandarea"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Brand </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Brand ID</th>
                            <th>Brand Name</th>
                            <th>B.Contact Person</th>
                            <th>B.C. Mobile</th>
                            <th>B.C. Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($brandList->count()>0)
                            @foreach($brandList as $brand)
                                <tr class="data_id_{{$brand->id}}">
                                    <td class="data_id">{{$brand->id}}</td>
                                    <td class="data_name">{{$brand->name}}</td>
                                    <td class="data_contact">{{$brand->contact_person}}</td>
                                    <td class="data_phone">{{$brand->phone}}</td>
                                    <td class="data_address">{{$brand->address}}</td>
                                    <td>
                                        <a href="#" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                        <a href="#" class="btn-edit" title="Edit Information" data-toggle="modal" data-target="#modal-edititemarea"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="#" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-delectitemarea"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        <a onclick="get_brand_transaction({{$brand->id}})" href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Brand ID</th>
                            <th>Brand Name</th>
                            <th>B.Contact Person</th>
                            <th>B.C. Mobile</th>
                            <th>B.C. Address</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>
        <!-- start Add Brand Area-->
        <div class="modal fade" id="modal-addBrandarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Brand Information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.brand.get_brand_view','autocomplete'=>'off']) !!}

                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Brand Name') !!}
                                            {!! Form::text('name', null,['placeholder'=>"Enter The Brand Name",'id'=>'name','class'=>'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('phone', 'Brand Contact Mobile Number') !!}
                                            {!! Form::text('phone', null,['placeholder'=>"Enter Brand Contact Person Mobile Number",'id'=>'phone','class'=>'form-control']) !!}
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {!! Form::label('brandContactPerson', 'Brand Contact Person Name') !!}
                                            {!! Form::text('brandContactPerson', null,['placeholder'=>"Enter Brand Contact Person Name",'id'=>'brandContactPerson','class'=>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('brandAddress', 'Brand Address') !!}
                                            {!! Form::textarea('brandAddress', null,['placeholder'=>"Enter The Full Brand Address Details",'id'=>'brandAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                        <div class="modal-footer">
                            {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                    <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Add Brand Area-->



        <!-- start View Brand Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Brand Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- form start -->
                            <form role="form">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Brand Name</label>
                                            <input type="text" class="form-control" id="data_name" disabled="disabled"  placeholder="Enter The Brand Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="BCMobile">Brand Contact Mobile Number</label>
                                            <input type="text" class="form-control" id="data_phone" disabled="disabled"  placeholder="Enter Brand Contact Person Mobile Number">
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="BContactPerson">Brand Contact Person Name</label>
                                            <input type="text" class="form-control" id="data_contact" disabled="disabled"  placeholder="Enter Brand Contact Person Name">
                                        </div>

                                        <div class="form-group">
                                            <label for="BrandAddress">Brand Address</label>
                                            <textarea id="data_address" class="form-control" rows="1" disabled="disabled"  placeholder="Enter The Full Brand Address Details"></textarea>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                            </form>
                            <!-- / form -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Brand Area-->

        <!-- start Edit Brand Area-->
        <div class="modal fade" id="modal-edititemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Brand information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.brand.update_brand','autocomplete'=>'off']) !!}
                    {{ Form::hidden('brand_id', 0, array('id' => 'edit_data_id')) }}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Brand Name') !!}
                                            {!! Form::text('name', null,['placeholder'=>"Enter The Brand Name",'id'=>'edit_data_name','class'=>'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('phone', 'Brand Mobile') !!}
                                            {!! Form::text('phone', null,['placeholder'=>"Enter Brand Contact Person Mobile Number",'id'=>'edit_data_phone','class'=>'form-control']) !!}
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {!! Form::label('brandContactPerson', 'Brand Contact Person') !!}
                                            {!! Form::text('brandContactPerson', null,['placeholder'=>"Enter Brand Contact Person Name",'id'=>'edit_data_contact','class'=>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('brandAddress', 'Brand Address') !!}
                                            {!! Form::textarea('brandAddress', null,['placeholder'=>"Enter The Full Brand Address Details",'id'=>'edit_data_address','class'=>'form-control',"rows"=>"1"]) !!}

                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Edit Brand information Area-->

        <!-- Start Brand information Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Brand information Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Brand?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Brand information Area-->

        <!-- Start Brand History Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Brand History Here</h4>
                    </div>
                    <div class="modal-body">

                        <!-- Start History Table -->
                        <div class="box">
                            <b class="text-yellow"> Brand Name : <span class="text-green brand_name"> Minhaj </span></b>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Transaction Date</th>
                                        <th>Product Name</th>
                                        <th>Buy/Sell</th>
                                        <th>Product Price</th>
                                        <th>Dollar Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Brand History Area-->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var data_id = "";
        var data_name = "";
        var data_contact = "";
        var data_phone = "";
        var data_address = "";
        /*call when get details*/
        function get_data_details(current){
            data_id = $(current).parent().parent().find(".data_id ").text();
            data_name = $(current).parent().parent().find(".data_name").text();
            data_phone = $(current).parent().parent().find(".data_phone").text();
            data_contact = $(current).parent().parent().find(".data_contact").text();
            data_address = $(current).parent().parent().find(".data_address").text();
        }

        /*call when show details*/
        $('.btn-view').click(function(){
            get_data_details(this);
            $("#data_name").attr("placeholder", data_name);
            $("#data_phone").attr("placeholder", data_phone);
            $("#data_contact").attr("placeholder", data_contact);
            $("#data_address").attr("placeholder", data_address);
        });

        /*call when customer update/edit*/
        $('.btn-edit').click(function(){
            get_data_details(this);
            $("#edit_data_id").val(data_id);
            $("#edit_data_name").val(data_name);
            $("#edit_data_contact").val(data_contact);
            $("#edit_data_phone ").val(data_phone);
            $("#edit_data_address").val(data_address);

        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_data_details(this);
        });
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.brand.delete_brand')}}',
                data: {'_token':'{{csrf_token()}}','brand_id':data_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Brand delete successfully done</div>';
                        $('.data_id_'+data_id).slideUp(1000);
                        $(".alert").hide();
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Brand delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
        //get the brand details by brand id
        function get_brand_transaction(brand_id) {
            var brand_name = $(".data_id_"+brand_id).find('.data_name').text();
            $("span.brand_name").text(brand_name);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.transaction.get_brand_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','brand_id':brand_id},
                    type: "POST",
                },

            });

        }
    </script>
@endsection