<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- select2 -->
<script src="{{asset('assets/js/select2.full.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<!-- Timepicker -->
<script src="{{asset('assets/js/bootstrap-timepicker.min.js')}}"></script>
<!-- Datepicker -->
<script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/js/adminlte.min.js')}}"></script>
<!-- DC custom -->
<script src="{{asset('assets/js/custom.js')}}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->