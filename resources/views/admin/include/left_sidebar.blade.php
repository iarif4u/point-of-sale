@php
    function has_access($route){
        $roleData = App\UserRole::where(['user_id'=>auth()->user()->getAuthIdentifier()])->select('role')->get();
        foreach ($roleData as $role){
             if(strpos(parse_url(route($route),PHP_URL_PATH), $role->role) !== false){
                return true;
            }
        }
        return false;
    }

@endphp
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        {{--<div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Head Admin</p>
                <!-- Status -->
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>--}}

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>->
        <!-- Optionally, you can add icons to the links -->
            <li @if(URL::current()==route('home')) class="active" @endif>
                <a href="{{route('home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            @if(has_access('admin.pos.get_pos_view'))
                <li @if(URL::current()==route('admin.pos.get_pos_view')) class="active" @endif>
                    <a href="{{route('admin.pos.get_pos_view')}}">
                        <i class="fa fa-cart-arrow-down" aria-hidden="true"></i> <span>POS</span>

                    </a>
                </li>
            @endif
            @if(has_access('admin.inventory.get_item_list'))
            <li @if(strpos(URL::current(), route('admin.inventory.get_item_list')) !== false)  class="active treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-suitcase" aria-hidden="true"></i> <span>Inventory</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul  class="treeview-menu">
                    <li @if(URL::current()==route('admin.inventory.get_item_list'))  class="active" @endif>
                        <a href="{{route('admin.inventory.get_item_list')}}"><i class="fa fa-circle-o"></i> Item List </a>
                    </li>
                    <li @if(URL::current()==route('admin.inventory.get_category_list'))  class="active" @endif>
                        <a href="{{route('admin.inventory.get_category_list')}}"><i class="fa fa-circle-o"></i> Category List </a>
                    </li>
                    <li @if(URL::current()==route('admin.inventory.get_sub_category_list'))  class="active" @endif>
                        <a href="{{route('admin.inventory.get_sub_category_list')}}"><i class="fa fa-circle-o"></i> Sub-Category List </a>
                    </li>
                </ul>
            </li>
            @endif
            @if(has_access('admin.pos.get_sold_view'))
            <li @if(URL::current()==route('admin.pos.get_sold_view')) class="active" @endif>
                <a href="{{route('admin.pos.get_sold_view')}}">
                    <i class="fa fa-list" aria-hidden="true"></i> <span>Sold List</span>

                </a>
            </li>
            @endif

            @if(has_access('admin.warehouse.get_warehouse_list'))
                <li @if(strpos(URL::current(), route('admin.warehouse.get_warehouse_list')) !== false)  class="active
                treeview" @else class="treeview" @endif>
                    <a href="javascript:void(0);">
                        <i class="fa fa-bank" aria-hidden="true"></i> <span>Warehouse</span>
                        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li  @if(URL::current()==route('admin.warehouse.get_warehouse_list'))  class="active" @endif>
                            <a href="{{route('admin.warehouse.get_warehouse_list')}}"><i class="fa fa-circle-o"></i> Warehouse List </a>
                        </li>
                        <li  @if(URL::current()==route('admin.warehouse.warehouse_transfer'))  class="active" @endif>
                            <a href="{{route('admin.warehouse.warehouse_transfer')}}"><i class="fa fa-circle-o"></i> Warehouse Transfer </a>
                        </li>
                        <li  @if(URL::current()==route('admin.warehouse.warehouse_transfer_log'))  class="active"
                                @endif>
                            <a href="{{route('admin.warehouse.warehouse_transfer_log')}}"><i class="fa
                            fa-circle-o"></i> Warehouse Transfer Log</a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(has_access('admin.brand.get_brand_view'))
                <li @if(URL::current()==route('admin.brand.get_brand_view')) class="active" @endif>
                    <a href="{{route('admin.brand.get_brand_view')}}">
                        <i class="fa fa-codepen" aria-hidden="true"></i> <span>Brand</span>
                    </a>
                </li>
            @endif
            @if(has_access('admin.vendor.get_vendor_list'))
                <li @if(strpos(URL::current(), route('admin.vendor.get_vendor_list')) !== false)  class="active
                treeview" @else class="treeview" @endif>
                    <a href="javascript:void(0);">
                        <i class="fa fa-user" aria-hidden="true"></i> <span>Vendor</span>
                        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(URL::current()==route('admin.vendor.get_vendor_list'))  class="active" @endif>
                            <a href="{{route('admin.vendor.get_vendor_list')}}"><i class="fa fa-circle-o"></i> Vendors List </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(has_access('admin.customer.get_customer_list'))
                <li @if(strpos(URL::current(), route('admin.customer.get_customer_list')) !== false)  class="active
                treeview" @else class="treeview" @endif>
                    <a href="javascript:void(0);">
                        <i class="fa fa-users" aria-hidden="true"></i> <span>Customers</span>
                        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(URL::current()==route('admin.customer.get_customer_list'))  class="active" @endif>
                            <a href="{{route('admin.customer.get_customer_list')}}"><i class="fa fa-circle-o"></i> Customers List</a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(has_access('admin.quotation.get_quotation_view'))
                <li @if(strpos(URL::current(), route('admin.quotation.get_quotation_view')) !== false)  class="active
                treeview" @else class="treeview" @endif>
                    <a href="javascript:void(0);">
                        <i class="fa fa-dollar" aria-hidden="true"></i> <span>Quotation</span>
                        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(URL::current()==route('admin.quotation.make_quotation'))  class="active" @endif>
                            <a href="{{route('admin.quotation.make_quotation')}}"><i class="fa fa-circle-o"></i> New Quotation </a></li>
                        <li @if(URL::current()==route('admin.quotation.get_quotation_view'))  class="active" @endif>
                            <a href="{{route('admin.quotation.get_quotation_view')}}"><i class="fa fa-circle-o"></i>Quotation List </a></li>
                    </ul>
                </li>
            @endif
            @if(has_access('admin.user.new_user'))
            <li @if(strpos(URL::current(), route('admin.user.new_user')) !== false)  class="active
            treeview" @else class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-sliders" aria-hidden="true"></i> <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==route('admin.user.new_user'))  class="active" @endif>
                        <a href="{{route('admin.user.new_user')}}"><i class="fa fa-circle-o"></i> New User </a></li>
                    <li @if(URL::current()==route('admin.user.user'))  class="active" @endif>
                        <a href="{{route('admin.user.user')}}"><i class="fa fa-circle-o"></i>User List </a></li>
                    <li @if(URL::current()==route('admin.user.pdf_setting'))  class="active" @endif>
                        <a href="{{route('admin.user.pdf_setting')}}"><i class="fa fa-circle-o"></i>PDF
                            Setting</a></li>
                </ul>
            </li>
            @endif
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>