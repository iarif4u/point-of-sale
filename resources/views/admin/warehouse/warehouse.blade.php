@extends('admin.layouts.master')

@section('title'," DifferentCoder || Warehouse")

@section('header_left')
    Warehouse List<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Warehouse List</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-addWarehousearea"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Warehouse </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Warehouse ID</th>
                            <th>Warehouse Name</th>
                            <th>Warehouse Mobile</th>
                            <th>Warehouse Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($warehouse_list->count()>0)
                            @foreach($warehouse_list as $warehouse)
                            <tr class="warehouse_{{$warehouse->id}}">
                                <td class="warehouse_id">{{$warehouse->id}}</td>
                                <td class="warehouse_name">{{$warehouse->name}}</td>
                                <td class="warehouse_phone">{{$warehouse->phone}}</td>
                                <td class="warehouse_address">{{$warehouse->address}}</td>
                                <td>
                                    <a href="#" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                    <a href="#" class="btn-edit" title="Edit Information" data-toggle="modal" data-target="#modal-edititemarea"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="#" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-delectitemarea"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a onclick="get_warehouse_transaction('{{$warehouse->id}}')" href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Warehouse ID</th>
                            <th>Warehouse Name</th>
                            <th>Warehouse Mobile</th>
                            <th>Warehouse Address</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>

        </div>
        <!-- start Add Warehouse Area-->
        <div class="modal fade" id="modal-addWarehousearea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Warehouse get_warehouse_list </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.warehouse.get_warehouse_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('warehouseName', 'Warehouse Name') !!}
                                            {!! Form::text('warehouseName', null,['placeholder'=>"Enter The Warehouse Name",'id'=>'warehouseName','class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('warehouseMobile', 'Warehouse Mobile') !!}
                                            {!! Form::text('warehouseMobile', null,['placeholder'=>"Enter The Warehouse Mobile",'id'=>'warehouseMobile','class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('warehouseAddress', 'Warehouse Address') !!}
                                            {!! Form::text('warehouseAddress', null,['placeholder'=>"Enter The Warehouse Address Details",'id'=>'warehouseAddress','class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Add Warehouse Area-->
        <!-- start View Warehouse Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Warehouse Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="WarehouseName">Warehouse Name</label>
                                            <div>
                                                <h5 class="warehouse-view" id="WarehouseNameView">Warehouse Name</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="WarehouseMobile">Warehouse Mobile</label>
                                            <div>
                                                <h5 class="warehouse-view" id="WarehouseMobileView">Warehouse Mobile</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="WarehouseAddress">Warehouse Address</label>
                                            <h5 class="warehouse-view" id="WarehouseAddressView">Warehouse Address</h5>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Warehouse Area-->

        <!-- start Edit Warehouse Area-->
        <div class="modal fade" id="modal-edititemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Warehouse information </h4>
                    </div>
                    {!! Form::open(['route' => 'admin.warehouse.update_warehouse','autocomplete'=>'off']) !!}
                    {{ Form::hidden('warehouse_id', 0, array('id' => 'edit_warehouse_id')) }}
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('warehouseName', 'Warehouse Name') !!}
                                {!! Form::text('warehouseName', null,['placeholder'=>"Enter The Warehouse Name",'id'=>'editWarehouseName','class'=>'form-control']) !!}
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('warehouseMobile', 'Warehouse Mobile') !!}
                                {!! Form::text('warehouseMobile', null,['placeholder'=>"Enter The Warehouse Mobile",'id'=>'editWarehouseMobile','class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('warehouseAddress', 'Warehouse Address') !!}
                                {!! Form::text('warehouseAddress', null,['placeholder'=>"Enter The Warehouse Address Details",'id'=>'editWarehouseAddress','class'=>'form-control']) !!}
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Edit Warehouse information Area-->

        <!-- Start Warehouse information Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Warehouse information Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Warehouse?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Warehouse information Area-->

        <!-- Start Warehouse History Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Warehouse History Here</h4>
                    </div>
                    <div class="modal-body">

                        <!-- Start History Table -->
                        <div class="box">
                            <b class="text-yellow">Warehouse Name : <span class="text-green warehouse_name"> Minhaj </span></b>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Transaction Date</th>
                                        <th>Product Name</th>
                                        <th>Buy/Sell</th>
                                        <th>Product Price</th>
                                        <th>Dollar Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Warehouse History Area-->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var warehouse_id = "";
        var name = "";
        var warehouse_phone = "";
        var warehouse_address = "";
        /*get customer data*/
        function get_warehouse_details(current){
            warehouse_id = $(current).parent().parent().find(".warehouse_id").text();
            name = $(current).parent().parent().find(".warehouse_name").text();
            warehouse_phone = $(current).parent().parent().find(".warehouse_phone").text();
            warehouse_address = $(current).parent().parent().find(".warehouse_address").text();
        }

        /*call when warehouse details*/
        $('.btn-view').click(function(){
            get_warehouse_details(this);
            $("#WarehouseNameView").text(name);
            $("#WarehouseMobileView").text(warehouse_phone);
            $("#WarehouseAddressView").text(warehouse_address);
        });

        /*call when warehouse update/edit*/
        $('.btn-edit').click(function(){
            get_warehouse_details(this);
            $("#edit_warehouse_id").val(warehouse_id);
            $("#editWarehouseName").val(name);
            $("#editWarehouseMobile").val(warehouse_phone);
            $("#editWarehouseAddress").val(warehouse_address);

        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_warehouse_details(this);
        });

        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.warehouse.delete_warehouse')}}',
                data: {'_token':'{{csrf_token()}}','warehouse_id':warehouse_id},
                success: function(data){

                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Warehouse delete successfully done</div>';
                        $('.warehouse_'+warehouse_id).hide('slow');
                        $(".alert").hide();
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Warehouse delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
        //get the warehouse details by brand id
        function get_warehouse_transaction(warehouse_id) {
            var warehouse_name  =$(".warehouse_"+warehouse_id).find('.warehouse_name').text();
            $("span.warehouse_name").text(warehouse_name);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.transaction.get_warehouse_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','warehouse_id':warehouse_id},
                    type: "POST",
                },

            });

        }
    </script>
@endsection

