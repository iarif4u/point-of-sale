@extends('admin.layouts.master')

@section('title'," DifferentCoder || Warehouse Transfer")

@section('header_left')
    Warehouse Transfer log<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Warehouse Transfer Log</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" href="{{route('admin.warehouse.warehouse_transfer')}}">
                    <i class="fa fa-th-list" aria-hidden="true"></i> Make
                    New Warehouse Transfer</a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <div class="table">
                    <table class="table" id="Table_id">
                        <thead>
                        <tr>
                            <th>From Warehouse</th>
                            <th>To Warehouse</th>
                            <th>Product Serial No</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($logList)&&$logList->count()>0)
                            @foreach($logList as $log)
                                @if($log->product_data)
                                    <tr>
                                        <td>{{$log->warehouse_from->name}}</td>
                                        <td>{{$log->warehouse_to->name}}</td>
                                        <td>{{$log->product_data->serial_no}}</td>
                                        <td>{{$log->product_data->name}}</td>
                                        <td>{{$log->quantity}}</td>
                                        <td>{{date('d-m-Y',strtotime($log->created_at))}}</td>
                                        <td>{{date('h:i A',strtotime($log->created_at))}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <!-- /.box-body -->

            </div>

        </div>

    </div>

@endsection

@section('script')
    <script>
        $("#Table_id").dataTable();
    </script>
@endsection

