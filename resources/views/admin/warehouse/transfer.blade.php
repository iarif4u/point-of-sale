@extends('admin.layouts.master')

@section('title'," DifferentCoder || Warehouse Transfer")

@section('header_left')
    Warehouse Transfer<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Warehouse Transfer</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" href="{{route('admin.warehouse.get_warehouse_list')}}">
                    <i class="fa fa-th-list" aria-hidden="true"></i> Add
                    New Warehouse </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'admin.warehouse.warehouse_transfer','autocomplete'=>'off']) !!}
                    <table class="table-responsive-md table table-responsive tab-content">
                        <thead>
                            <tr>
                                <th style="width: 18%">From Warehouse</th>
                                <th style="width: 18%">To Warehouse</th>
                                <th style="width: 28%">Product Serial / Name</th>
                                <th>In Stock</th>
                                <th style="width: 18%">Transfer Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    {!! Form::select('from_warehouse',$warehouse,  null, ['style'=>"width: 100%;",'class'
                                 => 'form-control select2 select_warehouse']) !!}
                                </td>
                                <td>
                                    {!! Form::select('to_warehouse',$warehouse,  null, ['style'=>"width: 100%;",'class'
                                 => 'form-control select2']) !!}
                                </td>
                                <td>
                                    {!! Form::select('product_id',$product_serial,  null, ['style'=>"width: 100%;",
                                    'class'
                                 => 'form-control select2 product_id']) !!}
                                </td>

                                <td>
                                    <span class="stock">0</span>
                                </td>
                                <td>
                                    {!! Form::text('product_quantity', null,['placeholder'=>"Enter Product Quantity",
                                    'class'=>'form-control product_quantity']) !!}

                                </td>
                                <td>
                                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <div class="table">
                    <table class="table" id="Table_id">
                        <thead>
                            <tr>
                                <th>Warehouse</th>
                                <th>Serial No</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($stock_info)&&$stock_info->count()>0)
                            @foreach($stock_info as $stock)
                                @if($stock->product)
                                <tr>
                                    <td class="pd_serial_{{$stock->product->id}}
                                            wh_no_{{$stock->warehouse->id}}">{{$stock->warehouse->name}}</td>
                                    <td class="pd_serial_{{$stock->product->id}}">{{$stock->product->serial_no}}</td>
                                    <td class="pd_name_{{$stock->product->id}}">{{$stock->product->name}}</td>
                                    <td class="{{$stock->product->id}}{{$stock->warehouse->id}}">{{$stock->product_qty}}</td>
                                </tr>
                                @endif
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>

                <!-- /.box-body -->

            </div>

        </div>

    </div>

@endsection

@section('script')
    <script>
        $('#Table_id').DataTable();
        //get product serial info
        $(".product_id").change(function () {
            var product_id = this.value;
            var warehouse_id = $(".select_warehouse").val();
            var qty = $("."+warehouse_id+product_id).text();
            if (warehouse_id > 0) {
                set_product_quantity(product_id,warehouse_id);
            }

        });
        $(".select_warehouse").change(function () {
            var warehouse_id = this.value;
            var product_id = $(".product_id").val();
            if (product_id > 0) {
                set_product_quantity(product_id,warehouse_id);
            }
        });

        //set warehouse product quantity
        function set_product_quantity(product_id, warehouse_id) {
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.warehouse.warehouse_product_qty')}}',
                data: {'_token':'{{csrf_token()}}','product_id':product_id,'warehouse_id':warehouse_id},
                success: function(data){
                    $(".stock").text(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Product quantity parsing fail</span><br/>';
                    row += '</div>';
                    $(".status").html(row);
                }
            });
        }

        $(".product_quantity").keyup(function () {
            var tr_qty = $(this).val();
            var qty =  $(".stock").text();
            if (!$.isNumeric(tr_qty)){
                $(this).val(null);
            }
        });
    </script>
@endsection

