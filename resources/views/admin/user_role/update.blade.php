<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */

?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Customer Registration")

@section('header_left')
    Update User<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Update User</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-user-plus" aria-hidden="true"></i> Add New User  </h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                {!! Form::open(['route' => 'admin.user.user_update','autocomplete'=>'off']) !!}
                {{ Form::hidden('user_id', $user->id, array('id' => 'edit_user_id')) }}
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('name', 'User Name') !!}
                        {!! Form::text('name', (isset($user))?$user->name:null,['placeholder'=>"Enter The User Name",'id'=>'userName','class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'User Phone Number') !!}
                        {!! Form::text('phone', (isset($user))?$user->phone:null,['placeholder'=>"Enter The User Phone Number",'id'=>'userPhone','class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', 'User Email') !!}
                        {!! Form::text('email', (isset($user))?$user->email:null,['placeholder'=>"Enter The User Email",'id'=>'userEmail','class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'User Password') !!}
                        {!! Form::password('password', ['placeholder'=>"Old Password",'id'=>'userPassword','class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <h4 class="text text-bold">
                        Access List
                    </h4>
                    <div class="table table-responsive">
                        <table class="table-hover col-md-12 table-condensed">
                            <tr>
                                <td>
                                    {!! Form::label('pos', 'POS') !!}
                                </td>
                                <td>

                                    {!! Form::checkbox('role[]', 'pos',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'pos'])->first())?true:false) !!}
                                </td>
                                <td>
                                    {!! Form::label('inventory', 'Inventory') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'inventory',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'inventory'])->first())?true:false)  !!}
                                </td>

                                <td>
                                    {!! Form::label('quotation', 'Quotation') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'quotation',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'quotation'])->first())?true:false)  !!}
                                </td>
                                <td>
                                    {!! Form::label('setting', 'Setting') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'setting',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'setting'])->first())?true:false)  !!}
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    {!! Form::label('warehouse', 'Warehouse') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'warehouse',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'warehouse'])->first())?true:false)  !!}
                                </td>
                                <td>
                                    {!! Form::label('brand', 'Brand') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'brand',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'brand'])->first())?true:false)  !!}
                                </td>
                                <td>
                                    {!! Form::label('vendor', 'Vendor') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'vendor',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'vendor'])->first())?true:false) !!}
                                </td>
                                <td>
                                    {!! Form::label('customer', 'Customer') !!}
                                </td>
                                <td>
                                    {!! Form::checkbox('role[]', 'customer',(\App\UserRole::where(['user_id'=>$user->id,'role'=>'customer'])->first())?true:false) !!}
                                </td>
                            </tr>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        </div>
                        <div class="col-md-1">
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            <!-- / form -->
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
        </div>
    </div>

@endsection

