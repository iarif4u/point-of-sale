<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || User List")

@section('header_left')
    User List<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> User List</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a class="btn btn-success" href="{{route('admin.user.new_user')}}">
                    <i class="fa fa-th-list" aria-hidden="true"></i> Add New User
                </a>
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                {!! Form::open(['route' => 'admin.user.user','id'=>"user_update_form",'autocomplete'=>'off']) !!}
                {{ Form::hidden('user_id', 0, array('id' => 'edit_user_id')) }}
                {!! Form::close() !!}
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($user_list->count()>0)
                            @foreach($user_list as $user)
                                <tr class="customer_{{$user->id}}">
                                    <td class="customer_id">{{$user->id}}</td>
                                    <td class="customer_name">{{$user->name}}</td>
                                    <td class="customer_phone">{{$user->phone}}</td>
                                    <td class="customer_phone">{{$user->email}}</td>
                                    <td>
                                        @if($user->role->count()>0)
                                            @foreach($user->role as $role)

                                                {{ucfirst($role->role)}}
                                            <br>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn-edit" title="Edit Information" data-toggle="modal" data-target="#modal-edititemarea">
                                            <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="jacascript:void(0);" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-delectitemarea">
                                            <i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>

                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
        </div>

        <!-- Start Customer information Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">User Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure delete this Customer?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Customer information Area-->

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var customer_id = "";
        var name = "";
        var customer_phone = ""
        /*get customer data*/
        function get_customer_details(current){
            customer_id = $(current).parent().parent().find(".customer_id ").text();
            name = $(current).parent().parent().find(".customer_name").text();
            customer_phone = $(current).parent().parent().find(".customer_phone").text();
        }
        //call when click delete button
        $('.btn-detect').click(function(){
            get_customer_details(this);

        });
        $(".btn-edit").click(function(){
            get_customer_details(this);
            $("#edit_user_id").val(customer_id);
            $("#user_update_form").submit();

        });

        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.user.delete_user')}}',
                data: {'_token':'{{csrf_token()}}','user_id':customer_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        $(".alert").hide();
                        var row = '<div class="alert alert-success">User delete successfully done</div>';
                        $('.customer_'+customer_id).slideUp(1000);
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Customer delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });

    </script>
@endsection