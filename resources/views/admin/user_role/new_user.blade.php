<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */

?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Customer Registration")

@section('header_left')
    New User<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> New User</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-user-plus" aria-hidden="true"></i> Add New User  </h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                {!! Form::open(['route' => 'admin.user.new_user','autocomplete'=>'off']) !!}
                    <div class="col-md-6">
                        <div class="form-group">
                        {!! Form::label('name', 'User Name') !!}
                        {!! Form::text('name', null,['placeholder'=>"Enter The User Name",'id'=>'userName','class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone', 'User Phone Number') !!}
                            {!! Form::text('phone', null,['placeholder'=>"Enter The User Phone Number",'id'=>'userPhone','class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('email', 'User Email') !!}
                            {!! Form::text('email', null,['placeholder'=>"Enter The User Email",'id'=>'userEmail','class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'User Password') !!}
                            {!! Form::password('password', ['placeholder'=>"Enter The User Password",'id'=>'userPassword','class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text text-bold">
                            Access List
                        </h4>
                        <div class="table table-responsive">
                            <table class="table-hover col-md-12 table-condensed">
                                <tr>
                                    <td>
                                        {!! Form::label('pos', 'POS') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'pos') !!}
                                    </td>
                                    <td>
                                        {!! Form::label('inventory', 'Inventory') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'inventory') !!}
                                    </td>

                                    <td>
                                        {!! Form::label('quotation', 'Quotation') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'quotation',null) !!}
                                    </td>
                                    <td>
                                        {!! Form::label('setting', 'Setting') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'setting',null) !!}
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        {!! Form::label('warehouse', 'Warehouse') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'warehouse',null) !!}
                                    </td>
                                    <td>
                                        {!! Form::label('brand', 'Brand') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'brand',null) !!}
                                    </td>
                                    <td>
                                        {!! Form::label('vendor', 'Vendor') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'vendor',null) !!}
                                    </td>
                                    <td>
                                        {!! Form::label('customer', 'Customer') !!}
                                    </td>
                                    <td>
                                        {!! Form::checkbox('role[]', 'customer',null) !!}
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                            </div>
                            <div class="col-md-1">
                                {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- / form -->
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
        </div>
    </div>

@endsection