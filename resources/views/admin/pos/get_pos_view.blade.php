<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || POS System")

@section('style')
    <style type="text/css">
        .pos-product-tabel tr.success {
            font-size: 12px;
        }
        .pos-product-tabel tr td {
            text-align: left;
            font-size: 12px;
        }
        i.fa.fa-trash-o.ac-hover:hover {
            color: red;
        }
        .pos-product-tabel tr td input.qty-input {
            width: 35px;
        }
        .box-body.dc-table-style {
            font-size: 11px!important;
        }
        #example1{
            font-size: 13px;
        }
    </style>
@endsection

@section('header_left')
    POS <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">POS</li>
@endsection

@section('content')

    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-6">
        <div class="box box-widget">

            <div class="box-footer">
                <!-- start Add Customer Area-->
                <div class="modal fade" id="modal-addcustomerarea">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Add Customer Information </h4>
                            </div>
                            <!-- form start -->
                            {!! Form::open(['route' => 'admin.customer.get_customer_list','autocomplete'=>'off']) !!}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('customerName', 'Customer Name') !!}
                                                {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",'id'=>'CustomerName','class'=>'form-control']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('phone', 'Customer Mobile') !!}
                                                {!! Form::text('phone', null,['placeholder'=>"Enter Customer Mobile",'id'=>'CustomerMobile','class'=>'form-control']) !!}
                                            </div>

                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                {!! Form::label('CustomerCompany', 'Customer Company Name') !!}
                                                {!! Form::text('customerCompany', null,['placeholder'=>"Enter Customer Company",'id'=>'CustomerCompany','class'=>'form-control']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('CustomerAddress', 'Customer Address') !!}
                                                {!! Form::textarea('customerAddress', null,['placeholder'=>"Enter The Full Customer Address Details",'id'=>'CustomerAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                                {!! Form::submit('Add Customer',['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                        <!-- / form -->
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="row">
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.pos.get_pos_view','autocomplete'=>'off','id'=>"pos_form"]) !!}
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Register Customer List</label>
                                {!! Form::select('customer_id',$customerList,  Session::get('customer_id') ,
                                         ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'customer_id']) !!}

                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>New Customer Add</label>
                            <a class="btn btn-block btn-social btn-bitbucket"  data-toggle="modal" data-target="#modal-addcustomerarea">
                                <i class="fa fa-bitbucket"></i> Add New
                            </a>
                        </div>

                        <!-- End Add Customer Area-->

                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::select('payment_method',['0'=>"Select One",'bKash'=>"bKash","Cash"=>"Cash","Bank"=>"Bank"], null,
                                         ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'payment_method']) !!}
                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input name="invoice_no" type="text" class="form-control" id="invoice_no" placeholder="Invoice No">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input name="ref_note" type="text" class="form-control" id="note" placeholder="Reference Note">
                        </div>
                    </div>

                        <div class="pos-product-tabel">
                            <div class="col-md-12">

                                <table class="table table-bordered table-striped">
                                    <tbody class="product_list">
                                    <tr class="success">
                                        <th>Serial</th>
                                        <th>Product</th>
                                        <th style="width: 70px">Price</th>
                                        <th style="width: 45px">Qty</th>
                                        <th style="width: 65px">Total</th>
                                        <th style="width: 10px">Del</th>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table">
                                    <tr class="info">
                                        <th>Total Item : </th>
                                        <th class="total_qty">0 </th>
                                        <th style="width: 80px">G.Total : </th>
                                        <th style="width: 80px" class="total_sum">0 tk</th>
                                    </tr>
                                </table>
                                <a class="btn btn-info btn-block notification-btn"  onclick="confirm_notification(this)">
                                    <i class="fa fa-dollar"></i> Payment
                                </a>
                                <a  data-toggle="modal" data-target="#modal-payment" href="javascript:void(0);" class="invisible open_payment ">Open Modal</a>
                            </div>
                        </div>
                    <!-- start Add Customer Area-->
                    <div class="modal fade" id="modal-payment">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Make payment</h4>
                                </div>
                                <!-- form start -->

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="box-body">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('total_product_price', 'Total Payable Amount') !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('customer_pay', 'Total Pay') !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('customer_due', 'Total Due') !!}
                                                </div>

                                            </div>
                                            <!-- /.col -->
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <div class="total_product_price">0</div>
                                                    {!! Form::hidden('total_product_price', null,['id'=>'total_product_price','class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::text('customer_pay', null,['placeholder'=>"Enter Customer Paid Amount",'id'=>'customer_pay','class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    <div class="total_due">0</div>
                                                    {!! Form::hidden('customer_due', null,['id'=>'customer_due','class'=>'form-control']) !!}

                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <div class="modal-footer">
                                    {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                                </div>

                            <!-- / form -->
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                {!! Form::close() !!}
                <!-- / form -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-widget">

            <div class="box-footer product-tabel-data">
                <div class="box-body dc-table-style">
                    {{--<table id="product-table" class="table table-bordered table-striped">
                        <thead>
                        <tr class="info">
                            <th class="hide">Stock Id</th>
                            <th>Serial No</th>
                            <th>Product</th>
                            <th>Warehouse</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Add</th>
                        </tr>
                        </thead>
                        <tbody>
                       --}}{{-- @if(isset($stockList))
                            @foreach($stockList as $stock)
                                @if($stock->product)
                                    <tr class="product_{{$stock->id}}">
                                        <td class="product_serial">{{$stock->product->name}}</td>
                                        <td class="product_name">{{$stock->product->name}}</td>
                                        <td class="product_wh">{{$stock->warehouse->name}}</td>
                                        <td class="product_qty">{{$stock->product_qty}}</td>
                                        <td class="product_price">৳ {{$stock->product->item_tk_rate}}</td>
                                        <td class="ware_house hidden hide">
                                            {{$stock->warehouse->name}}
                                            <span class="warehouse_id invisible">{{$stock->warehouse->id}}</span>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" onclick="add_product_to_chart('{{$stock->product->id}}',this,'{{$stock->id}}')" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-cart-plus" aria-hidden="true"></i> </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif--}}{{--
                        </tbody>
                    </table>--}}
                    {{$dataTable->table()}}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    {{$dataTable->scripts()}}
    <script>
        $(".sidebar-toggle").click();
    </script>
    {{--<script type="text/javascript">
        $(function() {
            $('#product-table').DataTable({
                serverSide: true,
                processing:true,
                ajax: '{{route('admin.pos.stock_products')}}',
                columns: [
                    {data: 'id'},
                    {data: 'product.serial_no'},
                    {data: 'product.name'},
                    {data: 'warehouse.name',"render": function(data,type,full,meta){
                            return full.warehouse.name;
                        }},
                    {data: 'product_qty'},
                    {data: 'product.item_tk_rate'},
                    {data: 'id',name:"id","render": function(data,type,full,meta){
                            return `<a href="javascript:void(0);" onclick="add_product_to_chart('${full.product.id}',this,'${full.id}')" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-cart-plus" aria-hidden="true"></i> </a>`;
                        }
                    }
                ],
                "columnDefs": [  {
                    "targets": 0,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`hide`);
                    }
                },{
                    "targets": 1,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`product_serial`);
                    }
                }, {
                    "targets": 2,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`product_name`);
                    }
                }, {
                    "targets": 3,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`product_wh ware_house`);
                    }
                }, {
                    "targets": 4,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`product_qty`);
                    }
                }, {
                    "targets": 5,
                    "createdCell": function (td, cellData, rowData, row, col) {
                        $(td).addClass(`product_price`);
                    }
                } ],
                'createdRow': function( row, data, dataIndex ) {
                    $(row).addClass(`product_${data.id}`);
                }
            });
        });
    </script>--}}
    <script type="text/javascript">
        var product_serial = "";
        var product_name = "";
        var product_qty = "";
        var product_price = "";
        var ware_house = "";
        //var product_list = "";


        /*get product data*/
        function get_product_details(current){
            product_serial = $(current).parent().parent().find(".product_serial").text();
            product_name = $(current).parent().parent().find(".product_name").text();
            product_qty = $(current).parent().parent().find(".product_qty").text();
            product_price = $(current).parent().parent().find(".product_price").text();
            ware_house = $(current).parent().parent().find(".warehouse_id").text();
        }
        function add_product_to_chart(product_id,context,stock_id) {
            get_product_details(context);
            product_price = product_price.replace(/[^0-9]/gi, '');
            var price_input = '<input style="width:50px;padding: 2px;height: 25px;" type="text" name="product_price[]" value="'+product_price+'" onkeyup="calculate_total()" class="form-control chart_product_price">';
            var row = '<tr><td class="single_serial">'+product_serial+'</td><td class="single_name">'+product_name+'</td><td class="current_product_price">'+price_input+'</td><td>\n' +
                '<input name="product_id[]" type="hidden" value="'+product_id+'" class="qty-input" placeholder="Qty">\n' +
                '<input name="warehouse_id[]" type="hidden" value="'+ware_house+'" class="warehouse-input">\n' +
                '<input name="product_qty[]" onkeyup="claculate_qty(this)" type="text" class="single_qty qty-input product_qty" placeholder="Qty">\n' +
                '</td><td class="sub_total">0 tk</td><td><i onclick="back_product_to_list('+product_id+',this,'+stock_id+')" class="fa fa-trash-o ac-hover"></i></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
        }

        //return product to productlist from chart
        function back_product_to_list(product_id,context,stock_id) {
            $(".product_"+stock_id).show();
            $(context).parent().parent().remove();

        }
        //calculate quantity and price of product
        function claculate_qty(context) {
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(qty*price);
            calculate_total();
        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;

            $(".chart_product_price").each(function() {
                var val = $.trim( $(this).val() );
                var qty_chart = $(this).parent().parent().find('.product_qty').val();
                var qty_price = (qty_chart>0)?qty_chart*val :0 ;
                $(this).parent().parent().find('.sub_total').text(qty_price);
                sum+=qty_price;
                total_qty=parseInt(qty_chart)+parseInt(total_qty);
            });

            $('.total_sum').text(sum);
            $('.total_qty').text(total_qty);
            $('.total_product_price').text(sum);
            $('#total_product_price').val(sum);
        }

        //remove charecter from number
        $("#customer_pay").keydown(function () {
            var paid = this.value;
            var paid = paid.replace(/[^0-9\.]/g, '');
            $("#customer_pay").val(paid);
        });
        //run when insert customer pay
        $("#customer_pay").keyup(function () {
            var total = $('#total_product_price').val();
            var paid = this.value;
            var due = total-paid;
            $(".total_due").text(due);
            $("#customer_due").val(due);

        });
        //run for validation data
        function confirm_notification(context) {
            var customer_error = "<p class='alert alert-danger customer'>Customer not selsectd</p>"
            var product_error = "<p class='alert alert-danger product'>Product not added to list</p>"
            var ref_error = "<p class='alert alert-danger ref_error'>Reference note can't be blank</p>"
            var payment_method_error = "<p class='alert alert-danger ref_error'>Payment method not selected</p>"
            var customer_id = $("#customer_id").val();
            var ref_note = $("#note").val();
            var payment_method = $("#payment_method").val();
            ref_note = $.trim(ref_note);
            var error = false;
            if (customer_id==0){
                $(".status").html(customer_error);
                error = true;
                return;
            }else{
                $(".customer").remove();
                error = false;
            }
            if (payment_method==0){
                $(".status").html(payment_method_error);
                error = true;
                return;
            }

            else{
                $(".ref_error").remove();
                error = false;
            }
            if ( $(".current_product_price").length ){
                $(".single_qty").each(function(){
                    if(!this.value>0){
                        var product_name =  $(this).parent().parent().find(".single_name").text();
                        var product_qty_error = "<p class='alert alert-danger product_qty_error'>"+product_name+" quantity not found</p>";
                        $(".status").html(product_qty_error);
                        error = true;
                        return;
                    }
                });
            }else{
                error = true;
                $(".status").html(product_error);
                return;
            }
            if (!error){
                $(".product_qty_error").remove();
                $(".open_payment").click();
            }
        }

        //call when form submit
        $("#pos_form").submit(function (e) {
            if(!$('#modal-payment').is(':visible')){
                e.preventDefault();
                $(".notification-btn").click();
            }
        });

    </script>
@endsection
