<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Invoice ")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/print.css')}}">
    <style>
        .calculation,td {
            padding: 5px !important;
        }
    </style>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12 text-center challan-button">
                <button>BILL</button>
            </div>
        </div>
        <div class="row challan-header-top">
            <div class="col-xs-1 challan-logo">
                <img src="{{asset('assets/img/logo.png')}}" alt="" />
            </div>
            <div class="col-xs-6 challan-title">
                <h2>NABI & BROTHERS</h2>
                    <h3>Nabico international</h3>
            </div>
            <div class="col-xs-5 challan-eminfo">
                <table class="table">
                    <tr>
                        <th>Phone</th>
                        <td>:</td>
                        <td>031-2521487, 031-712160, 031-723543</td>
                    </tr>
                    <tr>
                        <th>fax</th>
                        <td>:</td>
                        <td>88-031-723921</td>
                    </tr>
                    <tr>
                        <th>Mobile</th>
                        <td>:</td>
                        <td>01711-721065, 01711-748508, 01815-505056, 01817-204538</td>
                    </tr>
                    <tr>
                        <th>E-Mail</th>
                        <td>:</td>
                        <td>nabico@gmail.com, upal4444@gmail.com</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 challan-subtitle">
                <h6>specially deals : spare parts of marine,machineres,auto,heavy vehicle,gas & diesel generator and other accessories.</h6>
                <h5>importer & stockist</h5>
            </div>
        </div>

        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>Admin, Inc.</strong><br>
                    Nabi & Brothers 390, Dhaka Trunk Road Kadamtali,<br>
                    Chittagong-4100,bangladesh<br>
                    Phone : 031-2521487,712160<br>
                    Mobile: 01711-721065, 01815-505056<br>
                    Email: nabico@gmail.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>@if($sells_item){{$sells_item->customer->name}}@endif</strong><br>
                    @if($sells_item) {{$sells_item->customer->address}} @endif
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice No : </b> @if(isset($sells_item))@if($sells_item->invoice_no) {{$sells_item->invoice_no}} @else{{date('Ymdh',strtotime($sells_item->created_at)).$sells_item->id}}@endif @endif<br>
                <b>Payment Date:</b> @if(isset($sells_item)){{date('d-m-Y',strtotime($sells_item->created_at))}}@endif<br>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped challan-table">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Serial</th>
                        <th>Product Name</th>
                        <th>Brand</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count =1;
                        $product_qty = 0;
                        $total_price =0;
                    @endphp
                    @if(isset($sells_item))

                        @foreach($sells_item->transaction as $transaction)
                            @php
                                $total_price = $total_price+$transaction->product_rate*$transaction->product_qty;
                                $product_qty = $product_qty+$transaction->product_qty;
                            @endphp
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$transaction->product->serial_no}}</td>
                                <td>{{$transaction->product->name}}</td>
                                <td>{{$transaction->product->brand->name}}</td>
                                <td>{{$transaction->product_qty}}</td>
                                <td>{{$transaction->product_rate}}</td>
                                <td>৳ {{$transaction->product_rate*$transaction->product_qty}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        @if(isset($sells_item->return))
        @if($sells_item->return->count()>0)
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-left text-light-blue">Return Item</h4>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Product Serial</th>
                            <th>Product Name</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $customer_get_return = 0;
                            $return_total_price =0;
                            $return_product_qty =0;
                        @endphp
                        @foreach($sells_item->return as $return)
                            @php
                                $customer_get_return+=$return->customer_get_return;
                            @endphp
                            @foreach($return->transaction as $return_transaction)
                                @php
                                    $return_product_qty = $return_product_qty+$return_transaction->product_qty;
                                    $return_subtotal = $return_transaction->product_rate*$return_transaction->product_qty;
                                    $return_total_price +=$return_subtotal;
                                @endphp
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{$return_transaction->product->serial_no}}</td>
                                    <td>{{$return_transaction->product->name}}</td>
                                    <td>{{$return_transaction->product->brand->name}}</td>
                                    <td>{{$return_transaction->product_qty}}</td>
                                    <td>{{$return_transaction->product_rate}}</td>
                                    <td>৳ {{$return_subtotal}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        @endif
        <div class="row challan-sum-area">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods: <b>@if(isset($sells_item)){{$sells_item->payment_method}}@endif</b></p>
                @if(isset($sells_item->ref_note))
                    <div class="row">
                        <div class="col-md-4">
                            <p class="lead">Reference note:</p>
                        </div>
                        <div class="col-md-8">
                            <p style="margin-top: -5px;" class="text-muted well well-sm no-shadow">
                                {{$sells_item->ref_note}}
                            </p>
                        </div>
                    </div>
                @endif
            </div>
            <!-- /.col -->
            <div class="col-xs-2"></div>
            <div class="col-xs-3">
                @php
                    $calculation_flag = true;
                @endphp
                <div class="table-responsive">
                    @if(isset($return_total_price))
                        <table class="table">

                            <tr>
                                <th>Grand Total:</th>
                                <td>@if($total_price){{$total_price}}@endif</td>
                            </tr>
                            @if(isset($return_total_price))
                            <tr>
                                <th>Return Product Amount:</th>
                                <td>@if($return_total_price){{$return_total_price}}@endif</td>
                            </tr>

                            <tr>
                                <th>Get Return Amount</th>
                                <td>{{$customer_get_return}}</td>
                            </tr>
                            <tr>
                                <th>Total Payable:</th>
                                <td>
                                    @php
                                        $customer_get_from_return = $return_total_price-$customer_get_return;
                                        $payable =$total_price-$customer_get_from_return;
                                    @endphp
                                    {{$payable}}
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <th>Total paid:</th>
                                <td>@if($sells_item){{$sells_item->paid_tk}}@endif</td>
                            </tr>
                            @if($payable>$sells_item->paid_tk)
                            <tr>
                                <th>Total due:</th>
                                <td>{{$payable-$sells_item->paid_tk}}</td>
                            </tr>
                            @else
                            <tr>
                                <th>Customer Return:</th>
                                @php
                                    if ($sells_item->paid_tk-$payable==0){
                                        $calculation_flag = false;
                                    }
                                @endphp
                                <td><span class="threshold">{{$sells_item->paid_tk-$payable}}</span></td>
                            </tr>
                            @endif
                        </table>
                    @else
                        <table class="table">
                            <tr>
                                <th>Total Item:</th>
                                <td>@if($product_qty){{$product_qty}}@endif</td>
                            </tr>
                            <tr>
                                <th>Grand Total:</th>
                                <td>৳ @if($product_qty){{$total_price}}@endif</td>
                            </tr>

                            <tr>

                                <th>Total paid:</th>

                                <td>৳ @if($sells_item){{$sells_item->paid_tk}}@endif</td>

                            </tr>

                            <tr>

                                <th>Total due:</th>
                                @php
                                    if ($total_price-$sells_item->paid_tk==0){
                                        $calculation_flag = false;
                                    }
                                @endphp
                                <td>৳ <span class="threshold">{{$total_price-$sells_item->paid_tk}}</span></td>

                            </tr>

                        </table>

                    @endif
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row challan-signture-area">
            <!-- accepted payments column -->
            <div class="col-xs-6 text-left">
                <p>Buyer's signature</p>
            </div>
            <div class="col-xs-6 text-right">
                <p>For: Nabi & brothers nabico international</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 challan-footer">
                <table class="table">
                    <tr>
                        <th>CTG  Head Office</th>
                        <td>:</td>
                        <td>390, Dhaka Trunk Road (Dastagir Super Marker), Kadamtali, Chittagong, Bangladesh                             </td>
                    </tr>

                    <tr>
                        <th>CTG  Branch           </th>
                        <td>:</td>
                        <td>699, SK.  Mujib Road (1st Floor), Chittagong. Phone : 031-718863</td>
                    </tr>

                    <tr>
                        <th>Dhaka Head Office </th>
                        <td>:</td>
                        <td>City Machinery Market  96 / 97  Nobabpur  Road, Dhaka. Phone : 025716473, 025716474                                        </td>
                    </tr>
                    <tr>
                        <th>Mobile no
                        </th>
                        <td>:</td>
                        <td> 01817204538, 01674550978</td>
                    </tr>
                    <tr>
                        <th>WEBSITE</th>
                        <td>:</td>
                        <td>www.nabicointernational.com</td>
                    </tr>
                </table>
            </div>
        </div>
        {{--Make payment modal start--}}
        @if($calculation_flag)
        <div id="make-payment" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Make Payment</h4>
                    </div>
                    @if(isset($return_total_price)&&($sells_item->paid_tk-$payable)>0)
                        {!! Form::open(['route' => 'admin.pos.make_new_return','autocomplete'=>'off','class'=>'payment_form']) !!}
                    @else
                        {!! Form::open(['route' => 'admin.pos.make_new_payment','autocomplete'=>'off','class'=>'payment_form']) !!}
                    @endif

                    {{ Form::hidden('sell_id', $sells_item->id, array('id' => 'edit_sell_id')) }}
                    {{ Form::hidden('customer_id', $sells_item->customer->id, array('id' => 'edit_customer_id')) }}
                    <div class="modal-body table table-responsive">
                        <table class="table table-responsive table-bordered table-active table-hover">
                            <tr>
                                <td><h4>Customer Name:</h4></td>
                                <td style="width: 65%;"> <h4><strong>@if($sells_item){{$sells_item->customer->name}}@endif</strong></h4></td>
                            </tr>
                            <tr>
                                @if(isset($return_total_price)&&($sells_item->paid_tk-$payable)>0)
                                    <td><h4>Paid to Customer:</h4></td>
                                    <td style="width: 65%;"> <h4><strong>@if($sells_item){{$sells_item->paid_tk-$payable}}@endif</strong></h4></td>
                                @else
                                    <td><h4>Due:</h4></td>
                                    <td style="width: 65%;"> <h4><strong>@if($sells_item){{$total_price-$sells_item->paid_tk}}@endif</strong></h4></td>
                                @endif
                            </tr>
                            <tr>
                                <td><h4>Payment Amount:</h4></td>
                                <td>
                                    {!! Form::text('payment_amount', null,['placeholder'=>"Enter Payment Amount",'id'=>'customer_pay_amount','class'=>'form-control customer_pay_amount']) !!}
                                    @if(isset($return_total_price)&&($sells_item->paid_tk-$payable)>0)
                                        <div class="text text-danger alert-amount hidden text-bold">
                                            Return amount can't be grater than customer get amout or null
                                        </div>
                                    @else
                                        <div class="text text-danger alert-amount hidden text-bold">
                                            Payment amount can't be grater than due or null
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><h4>Payment Method:</h4></td>
                                <td>
                                    {!! Form::select('payment_method',['0'=>"Select One",'bKash'=>"bKash","Cash"=>"Cash","Bank"=>"Bank"], null,['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'payment_method']) !!}
                                </td>
                            </tr>
                            <tr>
                                <td><h4>Reference:</h4></td>
                                <td>
                                    {!! Form::text('reference', null,['placeholder'=>"Enter Payment Reference",'id'=>'customer_pay','class'=>'form-control']) !!}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        @endif
        {{--Make payment modal End--}}
        <!-- Start Return Item Area-->
        <div class="modal fade" id="modal-return">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Return Sell Product</h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.return.get_return_history','autocomplete'=>'off']) !!}
                    {{ Form::hidden('sell_id', (isset($sells_item))?$sells_item->id:0, array('id' => 'sell_id')) }}
                    {{ Form::hidden('customer_id', (isset($sells_item))?$sells_item->customer_id : 0, array('id' => 'customer_id')) }}
                    <div class="modal-body table col-md-12">
                        <div class="calculation_result alert alert-danger hidden">Product return quantity must be less than sell quantity</div>
                        <p class="text-light-blue">Customer Name: <span>@if(isset($sells_item)){{$sells_item->customer->name}}@endif</span></p>
                        <table class="text-left table table-responsive table-hover table-content table-bordered table-active">
                            <thead>
                                <th>Name</th>
                                <th>Brand</th>
                                <th style="width: 18%;">Warehouse</th>
                                <th>Rate</th>
                                <th>Quantity</th>
                                <th>Return Quantity</th>
                                <th>Price</th>
                                <th>Return Price</th>
                                <th>Remove</th>
                            </thead>
                            <tbody class="return-list">

                            @if(isset($sells_item))
                                @php
                                    $total_price=0;
                                    $product_qty=0;
                                @endphp
                                @foreach($sells_item->transaction as $transaction)
                                    <tr>
                                        <td>
                                    @php
                                        $total_price = $total_price+$transaction->product_rate*$transaction->product_qty;
                                        $product_qty = $product_qty+$transaction->product_qty;
                                    @endphp
                                {{ Form::hidden('product_id[]', $transaction->product->id, array('class' => 'product_id')) }}
                                {{ Form::hidden('warehouse_id[]', $transaction->warehouse->id, array('class' => 'warehouse_id')) }}
                                {{ Form::hidden('transaction_id[]', $transaction->id, array('class' => 'transaction_id')) }}
                                {{ Form::hidden('sell_qty[]', $transaction->product_qty, array('class' => 'product_qty')) }}

                                        {{$transaction->product->name}}</td>
                                        <td>{{$transaction->product->brand->name}}</td>
                                        <td>{{$transaction->warehouse->name}}</td>
                                        <td class="product_rate">{{$transaction->product_rate}}</td>
                                        <td class="product_qty">{{$transaction->product_qty}}</td>
                                        <td>
                                            {!! Form::text('return_qty[]', null,['placeholder'=>"Return Qty",'class'=>'form-control return_qty']) !!}
                                        </td>
                                        <td>৳ {{$transaction->product_rate*$transaction->product_qty}}</td>
                                        <td class="each_return_price">0</td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-danger remove-return">X</a>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tr>
                                <td colspan="4" class="text-right text-bold">Total : </td>
                                <td>{{$product_qty}} </td>
                                <td class="total_return_qty">0</td>
                                <td>{{$total_price}} </td>
                                <td class="return_price">0</td>
                            </tr>
                        </table>
                        <div class="col-md-8 col-md-offset-3">
                            <div style="margin-right: -8%;" class="text-right">
                                @if(isset($sells_item))
                                <table class="pull-right calculation">
                                    <tr>
                                        <td>Total Paid :</td>
                                        <td><span class="paid_tk">{{$sells_item->paid_tk}}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Total Due :</td>
                                        <td class="due">{{$sells_item->total_tk-$sells_item->paid_tk}}</td>
                                    </tr>
                                    <tr>
                                        <td>Customer Total Return Price :</td>
                                        <td class="customer_return_price">0</td>
                                    </tr>
                                    <tr>
                                        <td>Customer Get Return Price :</td>
                                        <td class="">
                                            <input type="text" name="customer_get_return_tk" id="" class="form-control">
                                        </td>
                                    </tr>
                                </table>
                                    @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Return Item Area-->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            @if(isset($sells_item))
            <div class="col-xs-12">

                <a href="{{route('admin.pos.get_invoice_print',['id'=>$sells_item->id])}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                @if($calculation_flag)
                <a href="javascript:void(0);" class="btn btn-primary pull-right make-payment">
                    @if(isset($return_total_price)&&($sells_item->paid_tk-$payable)>0)
                        Return Money
                    @else
                        Payment
                    @endif
                </a>
                @endif
                <a href="{{route('admin.pos.get_invoice_download',['id'=>$sells_item->id])}}" class="btn btn-success pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                </a>

                <button onclick="open_return_modal()" type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-reply"></i> Return
                </button>

                @endif
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('script')
    <script type="text/javascript">
        function open_return_modal(){
            $('#modal-return').modal().toggle();
        }
        $(".return_qty").keyup(function () {
            var paid = this.value;
            var paid = paid.replace(/[^0-9\.]/g, '');
            $(this).val(paid);
            calculation();
        });
        function calculation() {
            var total_price =0;
            var total_qty =0;
            $(".return_qty").each(function () {
                var qty = parseInt($.trim(this.value));
                var error = false;
                if (qty>0){
                    var rate = parseInt($.trim($(this).parent().parent().find(".product_rate").text()));
                    var sell_qty = parseInt($.trim($(this).parent().parent().find(".product_qty").text()));
                    var due = parseInt($.trim($(".due").text()));
                    if (qty>sell_qty){
                        error =true;
                        show_error();
                        $(this).val(null);
                    }else{
                        var products_price = rate*qty;
                        total_price+=products_price;
                        total_qty+=qty;
                        var customer_get_return = total_price-due;
                        $(".return_price").text(total_price);
                        $(this).parent().parent().find('.each_return_price').text(products_price);

                        $(".total_return_qty").text(total_qty);
                        $(".customer_return_price").text(customer_get_return);
                    }
                }
            });

        }
        function show_error() {
            $('.calculation_result').removeClass('hidden');
            $('.calculation_result').fadeIn();
            setTimeout(function() {
                $('.calculation_result').fadeOut().delay(2000);
            },2000);
        }
        //remove product item from return
        $(".remove-return").click(function () {
            $(this).parent().parent().remove();
        });
        //make payment
        $(".make-payment").click(function () {
            $('#make-payment').modal().toggle();
        });
        $(".payment_form").submit(function (event) {
            var threshold = parseInt($(".threshold").text());
            var amount = parseInt($(".customer_pay_amount").val());
            if (amount>threshold||amount==0||!amount) {
                event.preventDefault();
                $(".alert-amount").removeClass('hidden');
            }


        });
    </script>
@endsection
