<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Sold Item")

@section('header_left')
        Sold List
        <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Sold List</li>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" href="{{route('admin.pos.get_pos_view')}}"> <i class="fa fa-th-list" aria-hidden="true"></i> New Selll </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->

                <div class="box-body dc-table-style">
                   {{-- <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Sells Id</th>
                            <th>Customer Name</th>
                            <th>Total Amount</th>
                            <th>Total Paid</th>
                            <th>Due</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(isset($sells_item))
                            @foreach($sells_item as $sells)
                                <tr class="category_{{$sells->id}}">
                                    <td class="categoryIdView">{{date('Ymdh',strtotime($sells->created_at)).$sells->id}}</td>
                                    <td class="categoryIdView">@if($sells->customer){{$sells->customer->name}}@else Unknow or Customer deleted @endif</td>
                                    <td class="categoryIdView">{{$sells->total_tk}}</td>
                                    <td class="categoryIdView">{{$sells->paid_tk}}</td>
                                    <td class="categoryIdView">{{$sells->total_tk-$sells->paid_tk}}</td>
                                    <td class="categoryNameView">{{date('d-m-Y',strtotime($sells->created_at))}}</td>
                                    <td>
                                        <a href="{{route('admin.pos.get_pos_details',['id'=>$sells->id])}}" class="btn-view" title="View Information" > <i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="{{route('admin.pos.get_invoice_download',['id'=>$sells->id])}}" class="btn-history btn" title="Download PDF"><i class="fa fa-file-pdf-o" style="font-size: 18px" aria-hidden="true"></i></a>

                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                        <tfoot>
                        <tr>
                            <td>Sells Id</td>
                            <td>Customer Name</td>
                            <th>Total Amount</th>
                            <th>Total Paid</th>
                            <th>Due</th>
                            <td>Date</td>
                            <td>Action</td>
                        </tr>
                        </tfoot>
                    </table>--}}
                    {{$dataTable->table()}}
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>

    </div>
@endsection


@section('script')
    {{$dataTable->scripts()}}
    <script type="text/javascript">
        var category_id = "";
        var name = "";
        /*get customer data*/
        function get_category_details(current){
            category_id = $(current).parent().parent().find(".categoryIdView").text();
            name = $(current).parent().parent().find(".categoryNameView").text();
        }
        /*call when category update/edit*/
        $('.btn-edit').click(function(){
            get_category_details(this);
            $("#edit_category_id").val(category_id);
            $("#editCategoryName").val(name);
        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_category_details(this);
        });
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.delete_category')}}',
                data: {'_token':'{{csrf_token()}}','category_id':category_id},
                success: function(data){

                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Category delete successfully done</div>';
                        $('.category_'+category_id).hide('slow');
                    }
                    $('#modal-newcategorydetect').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Category delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-newcategorydetect').modal('hide');
                    $(".status").html(row);
                }
            });
        });
    </script>
@endsection
