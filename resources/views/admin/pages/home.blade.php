<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 5:57 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," NAB || DifferentCoder")

@section('header_left')
    Dashboard
    <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="box box-default">

        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    {{--<table id="index_product" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 115px">Serial</th>
                            <th>Name</th>
                            <th>Sub Category</th>
                            <th>Details</th>
                            <th>Warehouse</th>
                            <th>BDTk </th>
                            <th>Dollar</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Serial</th>
                            <th>Name</th>
                            <th>Sub Category</th>
                            <th>Details</th>
                            <th>Warehouse</th>
                            <th>BDTk </th>
                            <th>Dollar</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>--}}
                    {{$dataTable->table()}}
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->

        </div>



        <!-- start View Item Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Item </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- box boyd start -->
                            <div class="box-body view-box">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="SerialNumber">Serial Number</label>
                                        <h5 class="viewSerialNumber item_display"></h5>

                                    </div>
                                    <div class="form-group">
                                        <label for="ItemCategory">Item Category</label>
                                        <h5 class="viewItemCategory item_display"></h5>
                                    </div>
                                  {{--  <div class="form-group">
                                        <label for="ItemRate">Item Rate</label>
                                        <h5 class="viewItemRate item_display"></h5>
                                    </div>--}}
                                    <div class="form-group">
                                        <label for="Brand Name">Brand Name</label>
                                        <h5 class="viewItemBrand  item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productStcok">Product Stock </label>
                                        <h5 class="viewItemStock item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productStcok">Currency </label>
                                        <h5 class="viewItemCurrency item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productStcok">Warehouse</label>
                                        <h5 class="viewItemWarehouse item_display"></h5>
                                    </div>

                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="ItemName">Item Name</label>
                                        <h5 class="viewItemName item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="Item Sub-Category">Item Sub-Category</label>
                                        <h5 class="viewItemSubCate item_display"></h5>
                                    </div>

                                    {{--<div class="form-group">
                                        <label for="ItemRate">Item Dollar Rate</label>
                                        <h5 class="viewItemDollar item_display"></h5>
                                    </div>--}}

                                    <div class="form-group">
                                        <label for="ProductDetails">Product Details </label>
                                        <h5 class="viewItemDetails item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productImport">Last Import</label>
                                        <h5 class="viewImport item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productImport">Dollar Rate</label>
                                        <h5 class="viewItemDollarRate item_display"></h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="productImport">Vendor</label>
                                        <h5 class="viewItemVendor item_display"></h5>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer view-box">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Item Area-->

        <!-- Start History Item Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Item History Here</h4>
                    </div>
                    <div class="modal-body">

                        <!-- Start History Table -->
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p class="item-info">Item Name : Lub Filter LF-670 | US$- 120 | Tk - 1200 | List Update : 10-10-2018</p>
                                <div class="transaction_list">

                                </div>
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Note</th>
                                        <th>Rate</th>
                                        <th>Customer</th>
                                        <th>Vendor</th>
                                        <th>Sale</th>
                                        <th>Import</th>
                                        <th>Currency</th>
                                        <th>Stock</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End History Item Area-->
    </div>
    <!-- /.row -->
@endsection

@section('script')
    {{$dataTable->scripts()}}
    <script>

        /*$("#index_product").DataTable({
            "bDestroy": true,
            'processing' : true,
            'ajax':{
                url: '{{route('admin.get_home_products')}}',
                data: {'_token':'{{csrf_token()}}'},
                type: "POST",
            },

        });*/
        //get the transaction details by proouct id
        function get_item_transaction(product_id,warehouse_id) {
            set_item_info(product_id);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.get_product_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','product_id':product_id,'warehouse_id':warehouse_id},
                    type: "POST",
                },

            });

        }
        //set product info data to top transaction
        function set_item_info(product_id) {
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_item_info')}}',
                data: {'_token':'{{csrf_token()}}','item_id':product_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                        $(".status").html(row);
                    }else{
                        var fromDate = new Date(data.message.updated_at);
                        var date = new Date(fromDate).toDateString();
                        var info_item = "Item Name : "+data.message.name+" | US$- "+data.message.item_dollar_rate+" | Tk - "+data.message.item_tk_rate+" | Last Update : "+date+" | Stock : "+data.message.stock;
                        $('.item-info').text(info_item);

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Item details parse failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        }

        /*get item details view*/
        function get_item_view(item_id) {

            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.inventory.get_item_details')}}',
                data: {'_token':'{{csrf_token()}}','item_id':item_id},
                success: function(result){
                    if (result.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<result.message.length;i++)
                        {
                            row += '<span class="each-error">'+result.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        $("h5.viewSerialNumber").text(result.message.serial_no);
                        $("h5.viewItemCategory").text(result.message.category.name);
                        /*$("h5.viewItemRate").text(result.message.item_tk_rate);*/
                        $("h5.viewItemBrand").text(result.message.brand.name);
                        $("h5.viewItemName").text(result.message.name);
                        $("h5.viewItemSubCate").text(result.message.sub_category.name);
                      /*  $("h5.viewItemDollar").text(result.message.item_dollar_rate);*/
                        $("h5.viewItemDetails").text(result.message.product_details);
                        $("h5.viewItemStock").text(result.message.stock);
                        $("h5.viewImport").text(result.transaction.last_import);
                        $("h5.viewItemCurrency").text(result.transaction.currency);
                        $("h5.viewItemDollarRate").text(result.transaction.dollar);
                        $("h5.viewItemWarehouse").text(result.transaction.warehouse);
                        $("h5.viewItemVendor").text(result.transaction.vendor);

                        // $("#viewItemCategory").val(result.message.category.name);
                        $("#edit_serial_no").val(result.message.serial_no);
                        $("#viewItemRate").val(result.message.item_tk_rate);
                        $("#edit_item_id").val(item_id);
                        $("#viewItemName").val(result.message.name);
                        //  $("#viewItemSubCate").val(result.message.sub_category.name);
                        $("#viewItemDollar").val(result.message.item_dollar_rate);
                        $("#viewItemDetails").val(result.message.product_details);

                        $('#viewItemCategory').val(result.message.category.id).trigger('change');
                        $('#viewItemBrand').val(result.message.brand.id).trigger('change');

                        $('#viewItemSubCate option').each(function () {
                            this.remove();
                        });

                        for (var i=0; i<result.message.category.sub_category.length; i++){

                            // Set the value, creating a new option if necessary
                            if ($('#viewItemSubCate').find("option[value='" + result.message.category.sub_category[i].id + "']").length) {
                                $('#viewItemSubCate').val(result.message.category.sub_category[i].id).trigger('change');
                            } else {
                                // Create a DOM Option and pre-select by default
                                var newOption = new Option(result.message.category.sub_category[i].name, result.message.category.sub_category[i].id, true, true);
                                // Append it to the select
                                $('#viewItemSubCate').append(newOption).trigger('change');
                            }
                        }
                    }
                    $(".status").html(row);
                    sub_cate_select_id = result.message.sub_category.id
                    $('#viewItemSubCate').val(result.message.sub_category.id).trigger('change');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Data Parse Error</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }

            });
        }
    </script>
@endsection
