<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Vendor Registration")

@section('header_left')
    <h1>Vendor List<small>Control panel</small></h1>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Vendor List</li>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-addVendorarea"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Vendor </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Vendor ID</th>
                            <th>Vendor Name</th>
                            <th>Vendor Mobile</th>
                            <th>Vendor Company</th>
                            <th>Vendor Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($vendor_list->count()>0)
                            @foreach($vendor_list as $vendor)
                        <tr class="vendor_{{$vendor->id}}">
                            <td class="vendor_id">{{$vendor->id}}</td>
                            <td class="vendor_name">{{$vendor->name}}</td>
                            <td class="vendor_phone">{{$vendor->phone}}</td>
                            <td class="vendor_company">{{$vendor->company}}</td>
                            <td class="vendor_address">{{$vendor->address}}</td>
                            <td>
                                <a href="#" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                <a href="#" class="btn-edit" title="Edit Information" data-toggle="modal" data-target="#modal-edititemarea"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="#" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-delectitemarea"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <a href="javascript:void(0);" onclick="get_vendor_transaction('{{$vendor->id}}')" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Vendor ID</th>
                            <th>Vendor Name</th>
                            <th>Vendor Mobile</th>
                            <th>Vendor Company</th>
                            <th>Vendor Address</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            
        </div>
        <!-- start Add Vendor Area-->
        <div class="modal fade" id="modal-addVendorarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Vendor Information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.vendor.get_vendor_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('vendorName', 'Vendor Name') !!}
                                            {!! Form::text('vendorName', null,['placeholder'=>"Enter The Vendor Name",'id'=>'vendorName','class'=>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('phone', 'Vendor Mobile') !!}
                                            {!! Form::text('phone', null,['placeholder'=>"Enter The Vendor Mobile",'id'=>'phone','class'=>'form-control']) !!}
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {!! Form::label('vendorCompany', 'Vendor Company Name') !!}
                                            {!! Form::text('vendorCompany', null,['placeholder'=>"Enter The Vendor Company",'id'=>'vendorCompany','class'=>'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('vendorAddress', 'Vendor Address') !!}
                                            {!! Form::textarea('vendorAddress', null,['placeholder'=>"Enter The Full Vendor Address Details",'id'=>'vendorAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->



                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Add Vendor Area-->



        <!-- start View Vendor Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Vendor Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="VendorName">Vendor Name</label>
                                            <div>
                                                <h5 class="vendor-view" id="vendorNameView">Vendor Name</h5>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Vendor Mobile</label>
                                            <div>
                                                <h5 class="vendor-view" id="phoneView">Vendor Mobile</h5>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="VendorCompany">Vendor Company Name</label>
                                            <div>
                                                <h5 class="vendor-view" id="vendorCompanyView">Vendor Company</h5>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="VendorAddress">Vendor Address</label>
                                            <div>
                                                <h5 class="vendor-view" id="vendorAddressView">Vendor Address</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Vendor Area-->

        <!-- start Edit Vendor Area-->
        <div class="modal fade" id="modal-edititemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Vendor information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.vendor.update_vendor','autocomplete'=>'off']) !!}
                    {{ Form::hidden('vendor_id', 0, array('id' => 'edit_vendor_id')) }}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('vendorName', 'Vendor Name') !!}
                                            {!! Form::text('vendorName', null,['placeholder'=>"Enter The Vendor Name",'id'=>'editVendorName','class'=>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('phone', 'Vendor Mobile') !!}
                                            {!! Form::text('phone', null,['placeholder'=>"Enter Vendor Mobile",'id'=>'editphone','class'=>'form-control']) !!}

                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {!! Form::label('vendorCompany', 'Vendor Company Name') !!}
                                            {!! Form::text('vendorCompany', null,['placeholder'=>"Enter Vendor Company",'id'=>'editVendorCompany','class'=>'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('vendorAddress', 'Vendor Address') !!}
                                            {!! Form::textarea('vendorAddress', null,['placeholder'=>"Enter The Full Vendor Address Details",'id'=>'editVendorAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->


                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Edit Vendor information Area-->

        <!-- Start Vendor information Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Vendor information Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Vendor?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Vendor information Area-->

        <!-- Start Vendor History Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Vendor History Here</h4>
                    </div>
                    <div class="modal-body">

                        <!-- Start History Table -->
                        <div class="box">
                            <b class="text-yellow"> Vendor Name : <span class="text-green vendor-name"> Minhaj </span></b>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Product Buy Date</th>
                                        <th>Product Name</th>
                                        <th>Proudct Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Vendor History Area-->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var vendor_id = "";
        var name = "";
        var vendor_phone = "";
        var vendor_company = "";
        var vendor_address = "";
        /*get vendor data*/
        function get_vendor_details(current){
            vendor_id = $(current).parent().parent().find(".vendor_id ").text();
            name = $(current).parent().parent().find(".vendor_name").text();
            vendor_phone = $(current).parent().parent().find(".vendor_phone").text();
            vendor_company = $(current).parent().parent().find(".vendor_company").text();
            vendor_address = $(current).parent().parent().find(".vendor_address").text();
        }

        /*call when vendor details*/
        $('.btn-view').click(function(){
            get_vendor_details(this);
            $("#vendorNameView").text(name);
            $("#vendorCompanyView").text( vendor_company);
            $("#phoneView").text( vendor_phone);
            $("#vendorAddressView").text(vendor_address);
        });

        /*call when customer update/edit*/
        $('.btn-edit').click(function(){
            get_vendor_details(this);
            $("#edit_vendor_id").val(vendor_id);
            $("#editVendorName").val(name);
            $("#editphone").val(vendor_phone);
            $("#editVendorCompany").val(vendor_company);
            $("#editVendorAddress").val(vendor_address);

        });
        //call when click delete button
        $('.btn-detect').click(function(){
            get_vendor_details(this);
        });

        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.vendor.delete_vendor')}}',
                data: {'_token':'{{csrf_token()}}','vendor_id':vendor_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Vendor delete successfully done</div>';
                        $('.vendor_'+vendor_id).hide('slow');
                        $(".alert").hide();
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Vendor delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
        //get the vendor details by vendor id
        function get_vendor_transaction(vendor_id) {
            var vendor = $('.vendor_'+vendor_id).find('.vendor_name').text();
            $("span.vendor-name").text(vendor);
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.transaction.get_vendor_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','vendor_id':vendor_id},
                    type: "POST",
                },

            });

        }
    </script>
@endsection