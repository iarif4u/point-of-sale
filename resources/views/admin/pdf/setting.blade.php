<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 5:57 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," NAB || DifferentCoder")

@section('header_left')
    Dashboard
    <small>PDF Settings</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-gear"></i> Setting</li>
    <li class="active"><i class="fa fa-file-pdf-o"></i> PDF</li>
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="box box-default">

        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    {!! Form::open(['route' => 'admin.user.pdf_setting','autocomplete'=>'off','files' => true]) !!}
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                {!! Form::label('header', 'PDF Header Image') !!}
                                {!! Form::file('header',['class'=>'pdf-header-img']) !!}
                            </div>
                        </div>
                        <div class="col-md-8">
                            @php
                                if ($pdf){
                                    $path = 'pdf_img/'.$pdf->pdf_header;
                                }else{
                                    $path = null;
                                }
                            @endphp
                            <img src="{{asset($path)}}" alt="PDF header image file" class="image
                            pdf-header-src
                            image-container
                            img-bordered img-responsive">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                {!! Form::label('footer', 'PDF Footer Image') !!}
                                {!! Form::file('footer',['class'=>'pdf-footer-img']) !!}
                            </div>
                        </div>
                        <div class="col-md-8">
                            @php
                                if ($pdf){
                                    $path = 'pdf_img/'.$pdf->pdf_footer;
                                }else{
                                    $path = null;
                                }
                            @endphp
                            <img src="{{asset($path)}}" alt="PDF footer image file" class="image
                            image-container
                            img-bordered img-responsive pdf-footer-src">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.row -->
@endsection


@section('script')
    <script type="text/javascript">
        function readURL(input,img_link) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $(img_link).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            }
        }
        $(".pdf-header-img").change(function(){
            readURL(this,'.pdf-header-src');
        });

        $(".pdf-footer-img").change(function(){
            readURL(this,'.pdf-footer-src');
        });
    </script>
@endsection
@section('style')
    <style>
        .image{
            height: 200px;
        }
    </style>
@endsection