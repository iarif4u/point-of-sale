<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Quotation ")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/print.css')}}">
    <style>
        .calculation,td {
            padding: 5px !important;
        }
    </style>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->
    <section id="invoice" class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12 text-center challan-button">
                <button>CHALLAN</button>
            </div>
        </div>
        <div class="row challan-header-top">
            <div class="col-xs-1 challan-logo">
                <img src="{{asset('assets/img/logo.png')}}" alt="" />
            </div>
            <div class="col-xs-6 challan-title">
                <h2>NABI & BROTHERS</h2>
                <h3>Nabico international</h3>
            </div>
            <div class="col-xs-5 challan-eminfo">
                <table class="table">
                    <tr>
                        <th>Phone</th>
                        <td>:</td>
                        <td>031-2521487, 031-712160, 031-723543</td>
                    </tr>
                    <tr>
                        <th>fax</th>
                        <td>:</td>
                        <td>88-031-723921</td>
                    </tr>
                    <tr>
                        <th>Mobile</th>
                        <td>:</td>
                        <td>01711-721065, 01711-748508, 01815-505056, 01817-204538</td>
                    </tr>
                    <tr>
                        <th>E-Mail</th>
                        <td>:</td>
                        <td>nabico@gmail.com, upal4444@gmail.com</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 challan-subtitle">
                <h6>specially deals : spare parts of marine,machineres,auto,heavy vehicle,gas & diesel generator and other accessories.</h6>
                <h5>importer & stockist</h5>
            </div>
        </div>

        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>Admin, Inc.</strong><br>
                    Nabi & Brothers 390, Dhaka Trunk Road Kadamtali,
                    Chittagong-4100,bangladesh
                    Phone : 031-2521487,712160
                    Mobile: 01711-721065, 01815-505056
                    Email: nabico@gmail.com 
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>@if($quotation->customer){{$quotation->customer->name}}@endif</strong><br>
                    @if($quotation->customer) {{$quotation->customer->address}} @endif
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Quotation No : </b> @if(isset($quotation)){{$quotation->id}}@endif<br>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped challan-table">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Product Serial</th>
                        <th>Product Name</th>
                        <th>Brand</th>
                        <th>Quantity</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $count =1;
                        $product_qty = 0;

                    @endphp
                    @if(isset($quotation))

                        @foreach($quotation->transaction as $transaction)
                            @php
                                $product_qty = $product_qty+$transaction->product_qty;
                            @endphp
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$transaction->product->serial_no}}</td>
                                <td>{{$transaction->product->name}}</td>
                                <td>{{$transaction->product->brand->name}}</td>
                                <td>{{$transaction->product_qty}}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row challan-sum-area">
            <!-- accepted payments column -->
            <div class="col-xs-6">

                <div class="row">
                    <div class="col-md-4">
                        <p class="lead">Reference note:</p>
                    </div>
                    <div class="col-md-8">
                        <p style="margin-top: -5px;" class="text-muted well well-sm no-shadow">
                            {{$quotation->ref_note}}
                        </p>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-3"></div>
            <div class="col-xs-3">

                <div style="margin-left: -55px;" class="table-responsive">
                    <table class="table" style="">
                        <tr>
                            <th style="width: 99px;">Total Item:</th>
                            <td>{{$product_qty}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row challan-signture-area">
            <!-- accepted payments column -->
            <div class="col-xs-6 text-left">
                <p>Buyer's signature</p>
            </div>
            <div class="col-xs-6 text-right">
                <p>For: Nabi & brothers nabico international</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 challan-footer">
                <table class="table">
                    <tr>
                        <th>CTG  Head Office</th>
                        <td>:</td>
                        <td>390, Dhaka Trunk Road (Dastagir Super Marker), Kadamtali, Chittagong, Bangladesh                             </td>
                    </tr>

                    <tr>
                        <th>CTG  Branch           </th>
                        <td>:</td>
                        <td>699, SK.  Mujib Road (1st Floor), Chittagong. Phone : 031-718863</td>
                    </tr>

                    <tr>
                        <th>Dhaka Head Office </th>
                        <td>:</td>
                        <td>City Machinery Market  96 / 97  Nobabpur  Road, Dhaka. Phone : 025716473, 025716474                                        </td>
                    </tr>
                    <tr>
                        <th>Mobile no
                        </th>
                        <td>:</td>
                        <td> 01817204538, 01674550978</td>
                    </tr>
                    <tr>
                        <th>WEBSITE</th>
                        <td>:</td>
                        <td>www.nabicointernational.com</td>
                    </tr>
                </table>
            </div>
        </div>


        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{route('admin.quotation.get_quotation_print',['id'=>$quotation->id])}}" class="btn btn-default"><i class="fa fa-print"></i> Print</a>

                <a href="{{route('admin.quotation.get_quotation_pdf_download',['id'=>$quotation->id])}}" class="btn btn-success pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                </a>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection