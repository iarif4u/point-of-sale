<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title',"DifferentCoder || Make Quotation ")

@section('style')
    <style type="text/css">
        .pos-product-tabel tr.success {
            font-size: 12px;
        }
        .pos-product-tabel tr td {
            text-align: left;
            font-size: 12px;
        }
        i.fa.fa-trash-o.ac-hover:hover {
            color: red;
        }
        .pos-product-tabel tr td input.qty-input {
            width: 35px;
        }
        .box-body.dc-table-style {
            font-size: 11px!important;
        }
        #example1{
            font-size: 13px;
        }
    </style>
@endsection

@section('header_left')
    Quotation <small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Quotation</li>
@endsection

@section('content')

    <!-- SELECT2 EXAMPLE -->
    <div class="col-md-6">
        <div class="box box-widget">

            <div class="box-footer">
                <!-- start Add Customer Area-->
                <div class="modal fade" id="modal-addcustomerarea">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Add Customer Information </h4>
                            </div>
                            <!-- form start -->
                            {!! Form::open(['route' => 'admin.customer.get_customer_list','autocomplete'=>'off']) !!}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('customerName', 'Customer Name') !!}
                                                {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",'id'=>'CustomerName','class'=>'form-control']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('phone', 'Customer Mobile') !!}
                                                {!! Form::text('phone', null,['placeholder'=>"Enter Customer Mobile",'id'=>'CustomerMobile','class'=>'form-control']) !!}
                                            </div>

                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                {!! Form::label('CustomerCompany', 'Customer Company Name') !!}
                                                {!! Form::text('customerCompany', null,['placeholder'=>"Enter Customer Company",'id'=>'CustomerCompany','class'=>'form-control']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('CustomerAddress', 'Customer Address') !!}
                                                {!! Form::textarea('customerAddress', null,['placeholder'=>"Enter The Full Customer Address Details",'id'=>'CustomerAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                                {!! Form::submit('Add Customer',['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                        <!-- / form -->
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="row">
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.quotation.make_quotation','id'=>'quotation','autocomplete'=>'off']) !!}
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Registerd Customer List</label>
                            {!! Form::select('customer_id',(isset($customerList))?$customerList:[0=>"Select One"],  Session::get('customer_id') ,
                                     ['style'=>"width: 100%;",'class' => 'form-control select2','id' => 'customer_id']) !!}

                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>New Customer Add</label>
                        <a class="btn btn-block btn-social btn-bitbucket"  data-toggle="modal" data-target="#modal-addcustomerarea">
                            <i class="fa fa-bitbucket"></i> Add New
                        </a>
                    </div>

                    <!-- End Add Customer Area-->

                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="ref_note" type="text" class="form-control" id="note" placeholder="Reference Note">
                        </div>
                    </div>

                    <div class="pos-product-tabel">
                        <div class="col-md-12">

                            <table class="table table-bordered table-striped">
                                <tbody class="product_list">
                                <tr class="success">
                                    <th>Product Serial</th>
                                    <th>Product </th>
                                    <th style="width: 35px">Qty</th>
                                    <th style="width: 10px">Del</th>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table">
                                <tr class="info">

                                    <th style="width: 80px" class="total_sum hide  hidden">0 tk</th>
                                    <th>Total Item : </th>
                                    <th class="total_qty">0 </th>
                                    <input type="hidden" name="total_product_price" id="total_product_price">
                                </tr>
                            </table>
                            {!! Form::submit('Submit',['class'=>'btn btn-primary btn-block']) !!}

                        </div>
                    </div>
                    <!-- /.modal -->
                {!! Form::close() !!}
                <!-- / form -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-widget">

            <div class="box-footer product-tabel-data">
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Product Serial</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th>Add</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($productList))
                            @foreach($productList as $product)
                                <tr class="product_{{$product->id}}">
                                    <td class="product_price">{{$product->serial_no}}</td>
                                    <td class="product_name">{{$product->name}}</td>
                                    <td class="product_qty">{{$product->stock}}</td>


                                    <td>
                                        <a href="#" onclick="add_product_to_chart('{{$product->id}}',this,'{{$product->id}}')" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-cart-plus" aria-hidden="true"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript">
        var product_name = "";
        var product_qty = "";
        var product_price = "";
        //var product_list = "";


        /*get product data*/
        function get_product_details(current){
            product_name = $(current).parent().parent().find(".product_name").text();
            product_qty = $(current).parent().parent().find(".product_qty").text();
            product_price = $(current).parent().parent().find(".product_price").text();

        }
        function add_product_to_chart(product_id,context,stock_id) {
            get_product_details(context);
            var row = '<tr><td class="single_name">'+product_price+'</td><td class="current_product_price">'+product_name+'</td><td>\n' +
                '<input name="product_price[]" type="hidden" value="'+product_price+'" class="qty-input" placeholder="Qty">\n' +
                '<input name="product_id[]" type="hidden" value="'+product_id+'" class="qty-input" placeholder="Qty">\n' +
                '<input name="product_qty[]" onkeyup="calculate_qty(this)" type="text" class="single_qty qty-input product_qty" placeholder="Qty">\n' +
                '</td><td><i onclick="back_product_to_list('+product_id+',this,'+stock_id+')" class="fa fa-trash-o ac-hover"></i></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
        }

        //return product to productlist from chart
        function back_product_to_list(product_id,context,stock_id) {
            $(".product_"+stock_id).show();
            $(context).parent().parent().remove();

        }
        //calculate quantity and price of product
        function calculate_qty(context) {
            
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(qty*price);
            calculate_total();
        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;

            $(".product_qty").each(function() {
                var val = $.trim( $(this).val() );
                if ( val ) {
                    val = parseFloat( val.replace( /^\$/, "" ) );
                    total_qty += !isNaN( val ) ? val : 0;
                }
            });

            $(".sub_total").each(function() {
                var val = $.trim( $(this).text() );

                if ( val ) {
                    val = parseFloat( val.replace( /^\$/, "" ) );

                    sum += !isNaN( val ) ? val : 0;
                }
            });

            $('.total_sum').text(sum);
            $('.total_qty').text(total_qty);
            $('.total_product_price').text(sum);

            $('#total_product_price').val(sum);
        }

        //remove charecter from number
        $("#customer_pay").keydown(function () {
            var paid = this.value;
            var paid = paid.replace(/[^0-9\.]/g, '');
            $("#customer_pay").val(paid);
        });
        //run when insert customer pay
        $("#customer_pay").keyup(function () {
            var total = $('#total_product_price').val();
            var paid = this.value;
            var due = total-paid;
            $(".total_due").text(due);
            $("#customer_due").val(due);

        });
        //run for validation data
        $("#quotation").submit(function (e) {
            context = $(this);
            var customer_error = "<p class='alert alert-danger customer'>Customer not selsectd</p>"
            var product_error = "<p class='alert alert-danger product'>Product not added to list</p>"
            var ref_error = "<p class='alert alert-danger ref_error'>Reference note can't be blank</p>"

            var customer_id = $("#customer_id").val();
            var ref_note = $("#note").val();
            ref_note = $.trim(ref_note);
            var error = false;
            if (customer_id==0){

                e.preventDefault();
                $(".status").html(customer_error);
                error = true;
                return;
            }else{
                $(".customer").remove();
            }
            if(ref_note.length==0){
                e.preventDefault();
                $(".status").html(ref_error);
                error = true;
                return;
            }
            else{
                $(".ref_error").remove();
            }
            if ( $(".current_product_price").length ){
                $(".single_qty").each(function(){
                    if(!this.value>0){
                        var product_name =  $(this).parent().parent().find(".single_name").text();
                        var product_qty_error = "<p class='alert alert-danger product_qty_error'>"+product_name+" quantity not found</p>";
                        $(".status").html(product_qty_error);
                        error = true;
                        e.preventDefault();
                        return;
                    }
                });
            }else{
                e.preventDefault();
                error = true;
                $(".status").html(product_error);
                return;
            }
            if (!error){
                $(".product_qty_error").remove();
            }

        });
    </script>
@endsection