<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Quotation List")
@section('style')
    <style>
        i.fa{
            font-size: 20px;
        }
    </style>
@endsection
@section('header_left')
    Quotation List<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Quotation List</li>
@endsection

@section('content')
    <!-- SELECT2 EXAMPLE -->

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a href="{{route('admin.quotation.make_quotation')}}" class="btn btn-success"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Quotation </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 65px;">Challan ID</th>
                            <th style="width: 65px;">Date</th>
                            <th style="width: 65px;">Time</th>
                            <th>Customer Name</th>
                            <th>Customer Mobile</th>
                            <th class="text-center" colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($quotationList))
                            @foreach($quotationList as $quotation)
                        <tr>
                            <td>{{$quotation->id}}</td>
                            <td>{{date('d-m-Y',strtotime($quotation->created_at))}}</td>
                            <td>{{date('h:i A',strtotime($quotation->created_at))}}</td>
                            <td>@if($quotation->customer){{$quotation->customer->name}}@else Unknown @endif</td>
                            <td>@if($quotation->customer){{$quotation->customer->phone}}@else Unknown @endif</td>

                            <td class="text-center">
                                <a href="{{route('admin.quotation.get_quotation_details',['id'=>$quotation->id])}}" class="btn-history" title="View Details"><i class="fa fa-eye" aria-hidden="true"></i></a>


                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.quotation.get_quotation_pdf_download',['id'=>$quotation->id])}}" class="btn-history" title="Download PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Challan ID</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Customer Name</th>
                            <th>Customer Mobile</th>
                            <th class="text-center" colspan="2">Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
            <!-- /.row -->
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var data_id = "";
        var data_name = "";
        var data_contact = "";
        var data_phone = "";
        var data_address = "";
        /*call when get details*/
        function get_data_details(current){
            data_id = $(current).parent().parent().find(".data_id ").text();
            data_name = $(current).parent().parent().find(".data_name").text();
            data_phone = $(current).parent().parent().find(".data_phone").text();
            data_contact = $(current).parent().parent().find(".data_contact").text();
            data_address = $(current).parent().parent().find(".data_address").text();
        }

        /*call when show details*/
        $('.btn-view').click(function(){
            get_data_details(this);
            $("#data_name").attr("placeholder", data_name);
            $("#data_phone").attr("placeholder", data_phone);
            $("#data_contact").attr("placeholder", data_contact);
            $("#data_address").attr("placeholder", data_address);
        });

        /*call when customer update/edit*/
        $('.btn-edit').click(function(){
            get_data_details(this);
            $("#edit_data_id").val(data_id);
            $("#edit_data_name").val(data_name);
            $("#edit_data_contact").val(data_contact);
            $("#edit_data_phone ").val(data_phone);
            $("#edit_data_address").val(data_address);

        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_data_details(this);
        });
        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.brand.delete_brand')}}',
                data: {'_token':'{{csrf_token()}}','brand_id':data_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        var row = '<div class="alert alert-success">Brand delete successfully done</div>';
                        $('.data_id_'+data_id).slideUp(1000);
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Brand delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });
    </script>
@endsection