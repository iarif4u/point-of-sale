<?php
/**
 * User: Md. Arif
 * Date: 6/2/2018
 * Time: 1:18 PM
 */
?>
@extends('admin.layouts.master')

@section('title'," DifferentCoder || Customer Registration")

@section('header_left')
    Customer List<small>Control panel</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Customer List</li>
@endsection


@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><a class="btn btn-success" data-toggle="modal" data-target="#modal-addcustomerarea"> <i class="fa fa-th-list" aria-hidden="true"></i> Add New Customer </a>  </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <!-- Start Invite Staff List controls -->
                <div class="box-body dc-table-style">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Customer Mobile</th>
                            <th>Customer Company</th>
                            <th>Customer Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($customerList->count()>0)
                            @foreach($customerList as $customer)
                        <tr class="customer_{{$customer->id}}">
                            <td class="customer_id">{{$customer->id}}</td>
                            <td class="customer_name">{{$customer->name}}</td>
                            <td class="customer_phone">{{$customer->phone}}</td>
                            <td class="customer_company">{{$customer->company}}</td>
                            <td class="customer_address">{{$customer->address}}</td>
                            <td>
                                <a href="#" class="btn-view" title="View Information" data-toggle="modal" data-target="#modal-viewitemarea"><i class="fa fa-eye" aria-hidden="true"></i> </a>
                                <a href="#" class="btn-edit" title="Edit Information" data-toggle="modal" data-target="#modal-edititemarea"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="#" class="btn-detect" title="Detect Information" data-toggle="modal" data-target="#modal-delectitemarea"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <a onclick="get_customer_transaction({{$customer->id}})" href="javascript:void(0);" class="btn-history" title="History Information" data-toggle="modal" data-target="#modal-historyitemarea"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Customer Mobile</th>
                            <th>Customer Company</th>
                            <th>Customer Address</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /End Invite Staff List controls -->
            </div>
        </div>
        <!-- start Add Customer Area-->
        <div class="modal fade" id="modal-addcustomerarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Customer Information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.customer.get_customer_list','autocomplete'=>'off']) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('customerName', 'Customer Name') !!}
                                        {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",'id'=>'CustomerName','class'=>'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('phone', 'Customer Mobile') !!}
                                        {!! Form::text('phone', null,['placeholder'=>"Enter Customer Mobile",'id'=>'phone','class'=>'form-control']) !!}
                                    </div>

                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        {!! Form::label('CustomerCompany', 'Customer Company Name') !!}
                                        {!! Form::text('customerCompany', null,['placeholder'=>"Enter Customer Company",'id'=>'CustomerCompany','class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('CustomerAddress', 'Customer Address') !!}
                                        {!! Form::textarea('customerAddress', null,['placeholder'=>"Enter The Full Customer Address Details",'id'=>'CustomerAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <!-- / form -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Add Customer Area-->



        <!-- start View Customer Area-->
        <div class="modal fade" id="modal-viewitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">View Customer Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="CustomerName">Customer Name</label>
                                            <div>
                                                <h5 class="customer-view" id="CustomerNameView">Customer Name</h5>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Customer Mobile</label>
                                            <div>
                                                <h5 class="customer-view" id="phoneView">Customer Mobile</h5>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="CustomerCompany">Customer Company Name</label>
                                            <h5 class="customer-view" id="CustomerCompanyView">Customer Company</h5>
                                        </div>
                                        <div class="form-group">
                                            <label for="CustomerAddress">Customer Address</label>
                                            <h5 class="customer-view" id="CustomerAddressView">Customer Company</h5>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                            <!-- / form -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End View Customer Area-->

        <!-- start Edit Customer Area-->
        <div class="modal fade" id="modal-edititemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Customer information </h4>
                    </div>
                    <!-- form start -->
                    {!! Form::open(['route' => 'admin.customer.update_customer','autocomplete'=>'off']) !!}
                    {{ Form::hidden('customer_id', 0, array('id' => 'edit_customer_id')) }}
                    <div class="modal-body">
                        <div class="row">
                                <div class="box-body">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('customerName', 'Customer Name') !!}
                                            {!! Form::text('customerName', null,['placeholder'=>"Enter The Customer Name",'id'=>'editCustomerName','class'=>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('phone', 'Customer Mobile') !!}
                                            {!! Form::text('phone', null,['placeholder'=>"Enter Customer Mobile",'id'=>'editphone','class'=>'form-control']) !!}
                                        </div>

                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            {!! Form::label('customerCompany', 'Customer Company Name') !!}
                                            {!! Form::text('customerCompany', null,['placeholder'=>"Enter Customer Company",'id'=>'editCustomerCompany','class'=>'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('customerAddress', 'Customer Address') !!}
                                            {!! Form::textarea('customerAddress', null,['placeholder'=>"Enter The Full Customer Address Details",'id'=>'editCustomerAddress','class'=>'form-control',"rows"=>"1"]) !!}
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->

                            <!-- / form -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="modal-footer">
                        {!! Form::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Edit Customer information Area-->

        <!-- Start Customer information Area-->
        <div class="modal modal-danger fade" id="modal-delectitemarea">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Customer information Delete Now</h4>
                    </div>
                    <div class="modal-body">
                        <p>are you sure delete this Customer?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline delete-confirm">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- End Delete Customer information Area-->

        <!-- Start Customer History Area-->
        <div class="modal fade" id="modal-historyitemarea">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Customer History Here</h4>
                    </div>
                    <div class="modal-body">
                        <!-- Start History Table -->
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="Table_id" class="table table-bordered transaction-table">
                                    <thead>
                                    <tr>
                                        <th>Sale ID</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Product Name</th>
                                        <th>Product Price</th>
                                        <th>Type</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.End History Table -->

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Customer History Area-->
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var customer_id = "";
        var name = "";
        var customer_phone = "";
        var customer_company = "";
        var customer_address = "";
        /*get customer data*/
        function get_customer_details(current){
            customer_id = $(current).parent().parent().find(".customer_id ").text();
            name = $(current).parent().parent().find(".customer_name").text();
            customer_phone = $(current).parent().parent().find(".customer_phone").text();
            customer_company = $(current).parent().parent().find(".customer_company").text();
            customer_address = $(current).parent().parent().find(".customer_address").text();
        }
        /*call when customer update/edit*/
        $('.btn-edit').click(function(){
            get_customer_details(this);
            $("#edit_customer_id").val(customer_id);
            $("#editCustomerName").val(name);
            $("#editphone").val(customer_phone);
            $("#editCustomerCompany").val(customer_company);
            $("#editCustomerAddress").val(customer_address);

        });
        /*call when customer details*/
        $('.btn-view').click(function(){
            get_customer_details(this);
            $("#CustomerNameView").text(name);
            $("#CustomerCompanyView").text( customer_company);
            $("#phoneView").text( customer_phone);
            $("#CustomerAddressView").text(customer_address);
        });

        //call when click delete button
        $('.btn-detect').click(function(){
            get_customer_details(this);
        });

        //call when confirm delete
        $('.delete-confirm').click(function(){
            $.ajax({
                type: "POST",
                dataType:'JSON',
                url: '{{route('admin.customer.delete_customer')}}',
                data: {'_token':'{{csrf_token()}}','customer_id':customer_id},
                success: function(data){
                    if (data.error=="true"){
                        var row = '<div class="alert alert-danger">';
                        for(var i=0;i<data.message.length;i++)
                        {
                            row += '<span class="each-error">'+data.message[i]+'</span><br/>';
                        }
                        row += '</div>';
                    }else{
                        $(".alert").hide();
                        var row = '<div class="alert alert-success">Customer delete successfully done</div>';
                        $('.customer_'+customer_id).slideUp(1000);
                    }
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var row = '<div class="alert alert-danger">';
                    row += '<span class="each-error">Customer delete failed</span><br/>';
                    row += '</div>';
                    $('#modal-delectitemarea').modal('hide');
                    $(".status").html(row);
                }
            });
        });

        //get the transaction details by proouct id
        function get_customer_transaction(customer_id) {
            $('#Table_id').DataTable({
                "bDestroy": true,
                'processing' : true,
                'ajax':{
                    url: '{{route('admin.transaction.get_customer_transaction')}}',
                    data: {'_token':'{{csrf_token()}}','customer_id':customer_id},
                    type: "POST",
                },

            });

        }
    </script>
@endsection