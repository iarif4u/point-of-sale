<!DOCTYPE html>
<html>
<head>
    @include('admin.include.header')
    <title> @yield('title') </title>
    @yield('style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        {{--include the top menu bar--}}
        @include('admin.include.top_sidebar')
        {{--include the left side bar--}}
        @include('admin.include.left_sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <h1>
                    @yield('header_left')
                </h1>
                <ol class="breadcrumb">
                    @yield('header_right')
                </ol>
            </section>

            <!-- Main content -->
            <section class="content container-fluid">
                <div class="status"></div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <span class="each-error">{{ $error }} </span><br/>
                        @endforeach
                    </div>

                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <!-- Small boxes (Stat box) -->
                    @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        {{--include the footer--}}
        @include('admin.include.footer')
    </div>
<!-- ./wrapper -->
    {{--link the js plugin--}}
    @include('admin.include.javascript_bar')
    @yield('script')
    <script>
        var documentTitle = document.title + " || ";

        (function titleMarquee() {
            document.title = documentTitle = documentTitle.substring(1) + documentTitle.substring(0,1);
            setTimeout(titleMarquee, 200);
        })();
    </script>
</body>
</html>