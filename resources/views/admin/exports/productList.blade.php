<?php
/**
 * User: Md. Arif
 * Date: 6/8/2018
 * Time: 8:34 PM
 */
?>
        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product List Export by Admin</title>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Item serial</th>
            <th>Name</th>
            <th>Category</th>
            <th>Sub-category</th>
            <th>Brand</th>
            <th>Warehouse</th>
            <th>Item rate TK</th>
            <th>Item dollar rate</th>
            <th>Item quantity</th>
            <th>Product details</th>
        </tr>
        </thead>
        <tbody>
        @if($stocks->count()>0)
            @foreach($stocks as $stock)
                <tr>
                    <th>{{$stock->product->serial_no}}</th>
                    <th>{{$stock->product->name}}</th>
                    <th>{{$stock->product->category->name}}</th>
                    <th>{{$stock->product->sub_category->name}}</th>
                    <th>{{$stock->product->brand->name}}</th>
                    <th>{{$stock->warehouse->name}}</th>
                    <th>{{$stock->product->item_tk_rate}}</th>
                    <th>{{$stock->product->item_dollar_rate}}</th>
                    <th>{{$stock->product_qty}}</th>
                    <th>{{$stock->product->product_details}}</th>

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</body>
</html>
