<?php
/**
 * User: Md. Arif
 * Date: 6/8/2018
 * Time: 8:34 PM
 */
?>
        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Staff List Export by Admin</title>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number</th>
            <th>E-mail</th>
            <th>UserName</th>
            <th>Type</th>
            <th>Position</th>

        </tr>
        </thead>
        <tbody>
        @if($staffList->count()>0)
            @foreach($staffList as $staff)
                <tr>
                    <td>{{$staff->name}}</td>
                    <td>{{$staff->phone}}</td>
                    <td>{{$staff->email}}</td>
                    <td>{{$staff->username}}</td>
                    <td>{{$staff->get_staff->type}}</td>
                    <td>{{$staff->get_staff->position}}</td>

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</body>
</html>
