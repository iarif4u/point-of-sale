<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


/*Routes Start for Admin*/
Route::group(['middleware' => ['auth'], 'as'=>'admin.'], function(){
    Route::post('/home/product', 'HomeController@get_home_products')->name('get_home_products');
    Route::post('home/transaction', 'HomeController@get_product_transaction')->name('get_product_transaction');

    //route start for pos user access
    Route::group(['prefix' => 'pos'], function() {
        Route::group(['middleware' => 'auth', 'as'=>'pos.'], function() {
            Route::get('/', 'SellController@get_pos_view')->name('get_pos_view');
            Route::get('/stock/products', 'SellController@get_stock_products')->name('stock_products');
            Route::get('/sold', 'SellController@get_sold_view')->name('get_sold_view');
            Route::get('/sold/details/{id}', 'SellController@get_pos_details')->name('get_pos_details');
            Route::post('/', 'SellController@make_new_sell');
            Route::post('/payment', 'SellController@make_new_payment')->name('make_new_payment');
            Route::post('/return', 'SellController@make_new_return')->name('make_new_return');
            Route::get('/download/{id}', 'SellController@get_invoice_download')->name('get_invoice_download');
            Route::get('/print/{id}', 'SellController@get_invoice_print')->name('get_invoice_print');
        });


        //route start for return  user access
        /*Those Routes for return product*/
        Route::group(['prefix' => 'return', 'as'=>'return.'], function() {
            Route::get('/details/{return_id}', 'ReturnController@get_return_details')->name('get_return_details');
            Route::post('/', 'ReturnController@get_return_product');
            Route::get('/', 'ReturnController@get_return_history')->name('get_return_history');
        });
        //route end for return  user access

        /*those route for transaction*/
        Route::group(['prefix' => 'transaction', 'as'=>'transaction.'], function() {
            Route::post('import', 'TransactionController@import_item')->name('import_item');
            Route::post('details/product', 'TransactionController@get_product_transaction')->name('get_product_transaction');
            Route::post('details/customer', 'TransactionController@get_customer_transaction')->name('get_customer_transaction');
            Route::post('details/brand', 'TransactionController@get_brand_transaction')->name('get_brand_transaction');
            Route::post('details/vendor', 'TransactionController@get_vendor_transaction')->name('get_vendor_transaction');
            Route::post('details/warehouse', 'TransactionController@get_warehouse_transaction')->name('get_warehouse_transaction');

        });
        /*transaction route end*/
    });
    //route end for pos user access

    //route start for inventory  user access
    /*Those Routes for Inventory item*/
    Route::group(['prefix' => 'inventory'], function() {
        Route::group(['as'=>'inventory.'], function() {
            Route::get('/', 'ItemController@get_item_list')->name('get_item_list');
            Route::get('/excel', 'ItemController@download_excel_file')->name('excel');
            Route::post('/product', 'ItemController@get_product_streaming')->name('get_product_streaming');

            Route::post('/item/details', 'ItemController@get_item_details')->name('get_item_details');
            Route::post('/item/update', 'ItemController@update_item')->name('update_item');
            Route::post('/item/delete', 'ItemController@delete_item')->name('delete_item');
            Route::post('/item/info', 'ItemController@get_item_info')->name('get_item_info');

            Route::post('/get_sub_category', 'ItemController@get_sub_category')->name('get_sub_category');
            Route::post('/', 'ItemController@make_new_item');
            Route::get('/category', 'ItemController@get_category_list')->name('get_category_list');
            Route::post('/category', 'ItemController@make_category');
            Route::post('/category/update', 'ItemController@update_category')->name('update_category');
            Route::post('/category/delete', 'ItemController@delete_category')->name('delete_category');
            Route::get('/sub-category', 'ItemController@get_sub_category_list')->name('get_sub_category_list');
            Route::post('/sub-category', 'ItemController@insert_sub_category');
            Route::post('/sub-category/update', 'ItemController@update_sub_category')->name('update_sub_category');

        });
        //those route for excel operations
        Route::group(['prefix' => 'excel', 'as'=>'excel.'], function() {
            Route::get('/category', 'ExcelController@get_category_sheet')->name('category_sheet');
            Route::post('/category', 'ExcelController@import_category_sheet');
            Route::get('/sub-category', 'ExcelController@get_sub_category_sheet')->name('sub_category_sheet');
            Route::post('/sub-category', 'ExcelController@import_sub_category_sheet');

            Route::post('/product', 'ExcelController@import_product_sheet')->name('product_sheet');
        });

    });
    //route end for inventory  user access

    //route start for quotation  user access
    /*Those Routes for Quotation*/
    Route::group(['prefix' => 'quotation', 'as'=>'quotation.'], function() {
        Route::get('/', 'QuotationController@get_quotation_view')->name('get_quotation_view');
        Route::get('/view/{id}', 'QuotationController@get_quotation_details')->name('get_quotation_details');
        Route::get('/download/{id}', 'QuotationController@get_quotation_pdf_download')->name('get_quotation_pdf_download');
        Route::get('/puserrint/{id}', 'QuotationController@get_quotation_print')->name('get_quotation_print');

        Route::get('/make', 'QuotationController@get_new_quotation_view')->name('make_quotation');

        Route::post('/make', 'QuotationController@make_quotation');
    });
    //route end for quotation  user access

    //route start for setting  user access
    //new user with role
    Route::group(['prefix' => 'setting', 'as'=>'user.'], function() {
        Route::get('/', 'UserRoleController@get_user_view')->name('new_user');
        Route::get('/list', 'UserRoleController@get_user_list')->name('user');
        Route::post('/list', 'UserRoleController@update_user_view')->name('user');
        Route::post('/update', 'UserRoleController@update_user_info')->name('user_update');
        Route::post('/details', 'UserRoleController@get_user_view');
        Route::post('/', 'UserRoleController@set_new_user');
        Route::post('/delete', 'UserRoleController@delete_user')->name('delete_user');
        Route::get('/pdf', 'PDFController@get_pdf_setting_view')->name('pdf_setting');
        Route::post('/pdf', 'PDFController@pdf_setting');
    });
    //route end for setting  user access

    //route start for vendor  user access
    /*Those Routes for Vendor*/
    Route::group(['prefix' => 'vendor', 'as'=>'vendor.'], function() {

        Route::get('/', 'VendorController@get_vendor_list')->name('get_vendor_list');
        Route::post('/', 'VendorController@make_vendor');
        Route::post('/update', 'VendorController@update_vendor')->name('update_vendor');
        Route::post('/delete', 'VendorController@delete_vendor')->name('delete_vendor');

    });
    //route end for vendor  user access

    //route start for customer  user access
    /*Those Routes for Customer*/
    Route::group(['prefix' => 'customer', 'as'=>'customer.'], function() {
        Route::get('/', 'CustomerController@get_customer_list')->name('get_customer_list');
        Route::post('/', 'CustomerController@make_customer');
        Route::post('/update', 'CustomerController@update_customer')->name('update_customer');
        Route::post('/delete', 'CustomerController@delete_customer')->name('delete_customer');
    });
    //route end for customer user access

    //route start for warehouse  user access
    /*Those Routes for Warehouse*/
    Route::group(['prefix' => 'warehouse', 'as'=>'warehouse.'], function() {
        Route::get('/', 'WarehouseController@get_warehouse_list')->name('get_warehouse_list');
        Route::get('/transfer', 'WarehouseController@warehouse_transfer')->name('warehouse_transfer');
        Route::get('/transfer/log', 'WarehouseController@warehouse_transfer_log')->name('warehouse_transfer_log');
        Route::post('/transfer', 'WarehouseController@transfer_warehouse_stock');
        Route::post('/qty', 'WarehouseController@warehouse_product_qty')->name('warehouse_product_qty');
        Route::post('/', 'WarehouseController@make_warehouse');
        Route::post('/update', 'WarehouseController@update_warehouse')->name('update_warehouse');
        Route::post('/delete', 'WarehouseController@delete_warehouse')->name('delete_warehouse');
    });
    //route endfor for warehouse  user access

    //route start for brand  user access
    /*Those Routes for Brand*/
    Route::group(['prefix' => 'brand', 'as'=>'brand.'], function() {
        Route::get('/', 'BrandController@get_brand_view')->name('get_brand_view');
        Route::post('/', 'BrandController@make_brand');
        Route::post('/update', 'BrandController@update_brand')->name('update_brand');
        Route::post('/delete', 'BrandController@delete_brand')->name('delete_brand');
    });
    //route end for brand  user access


});
/*Routes End for Admin*/
